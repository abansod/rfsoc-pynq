-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
entity rfsoc_byp_stub is
  port (
    m_axis_udp_tready : in std_logic_vector( 1-1 downto 0 );
    aresetn : in std_logic_vector( 1-1 downto 0 );
    s_axis_0_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_0_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_0_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_0_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_0_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_0_tvalid : in std_logic_vector( 1-1 downto 0 );
    irig_trig_in : in std_logic_vector( 1-1 downto 0 );
    s_axis_1_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_1_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_1_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_1_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_1_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_1_tvalid : in std_logic_vector( 1-1 downto 0 );
    s_axis_2_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_2_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_2_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_2_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_2_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_2_tvalid : in std_logic_vector( 1-1 downto 0 );
    s_axis_3_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_3_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_3_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_3_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_3_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_3_tvalid : in std_logic_vector( 1-1 downto 0 );
    irig_comp_in : in std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tdata : in std_logic_vector( 512-1 downto 0 );
    s_axis_snap_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_snap_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_snap_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_snap_tvalid : in std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tready : in std_logic_vector( 1-1 downto 0 );
    s_axis_udp_tdata : in std_logic_vector( 512-1 downto 0 );
    s_axis_udp_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_udp_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_udp_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_udp_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_udp_tvalid : in std_logic_vector( 1-1 downto 0 );
    clk : in std_logic;
    rfsoc_byp_aresetn : in std_logic;
    rfsoc_byp_s_axi_awaddr : in std_logic_vector( 11-1 downto 0 );
    rfsoc_byp_s_axi_awvalid : in std_logic;
    rfsoc_byp_s_axi_wdata : in std_logic_vector( 32-1 downto 0 );
    rfsoc_byp_s_axi_wstrb : in std_logic_vector( 4-1 downto 0 );
    rfsoc_byp_s_axi_wvalid : in std_logic;
    rfsoc_byp_s_axi_bready : in std_logic;
    rfsoc_byp_s_axi_araddr : in std_logic_vector( 11-1 downto 0 );
    rfsoc_byp_s_axi_arvalid : in std_logic;
    rfsoc_byp_s_axi_rready : in std_logic;
    m_axis_udp_tdata : out std_logic_vector( 512-1 downto 0 );
    m_axis_udp_tid : out std_logic_vector( 8-1 downto 0 );
    m_axis_udp_tkeep : out std_logic_vector( 64-1 downto 0 );
    m_axis_udp_tlast : out std_logic_vector( 1-1 downto 0 );
    m_axis_udp_tuser : out std_logic_vector( 32-1 downto 0 );
    m_axis_udp_tvalid : out std_logic_vector( 1-1 downto 0 );
    s_axis_0_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_1_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_2_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_3_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tready : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tdata : out std_logic_vector( 512-1 downto 0 );
    m_axis_snap_tid : out std_logic_vector( 16-1 downto 0 );
    m_axis_snap_tkeep : out std_logic_vector( 64-1 downto 0 );
    m_axis_snap_tlast : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tvalid : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tuser : out std_logic_vector( 32-1 downto 0 );
    s_axis_udp_tready : out std_logic_vector( 1-1 downto 0 );
    rfsoc_byp_s_axi_awready : out std_logic;
    rfsoc_byp_s_axi_wready : out std_logic;
    rfsoc_byp_s_axi_bresp : out std_logic_vector( 2-1 downto 0 );
    rfsoc_byp_s_axi_bvalid : out std_logic;
    rfsoc_byp_s_axi_arready : out std_logic;
    rfsoc_byp_s_axi_rdata : out std_logic_vector( 32-1 downto 0 );
    rfsoc_byp_s_axi_rresp : out std_logic_vector( 2-1 downto 0 );
    rfsoc_byp_s_axi_rvalid : out std_logic
  );
end rfsoc_byp_stub;
architecture structural of rfsoc_byp_stub is 
begin
  sysgen_dut : entity xil_defaultlib.rfsoc_byp 
  port map (
    m_axis_udp_tready => m_axis_udp_tready,
    aresetn => aresetn,
    s_axis_0_tdata => s_axis_0_tdata,
    s_axis_0_tid => s_axis_0_tid,
    s_axis_0_tkeep => s_axis_0_tkeep,
    s_axis_0_tlast => s_axis_0_tlast,
    s_axis_0_tuser => s_axis_0_tuser,
    s_axis_0_tvalid => s_axis_0_tvalid,
    irig_trig_in => irig_trig_in,
    s_axis_1_tdata => s_axis_1_tdata,
    s_axis_1_tid => s_axis_1_tid,
    s_axis_1_tkeep => s_axis_1_tkeep,
    s_axis_1_tlast => s_axis_1_tlast,
    s_axis_1_tuser => s_axis_1_tuser,
    s_axis_1_tvalid => s_axis_1_tvalid,
    s_axis_2_tdata => s_axis_2_tdata,
    s_axis_2_tid => s_axis_2_tid,
    s_axis_2_tkeep => s_axis_2_tkeep,
    s_axis_2_tlast => s_axis_2_tlast,
    s_axis_2_tuser => s_axis_2_tuser,
    s_axis_2_tvalid => s_axis_2_tvalid,
    s_axis_3_tdata => s_axis_3_tdata,
    s_axis_3_tid => s_axis_3_tid,
    s_axis_3_tkeep => s_axis_3_tkeep,
    s_axis_3_tlast => s_axis_3_tlast,
    s_axis_3_tuser => s_axis_3_tuser,
    s_axis_3_tvalid => s_axis_3_tvalid,
    irig_comp_in => irig_comp_in,
    s_axis_snap_tdata => s_axis_snap_tdata,
    s_axis_snap_tid => s_axis_snap_tid,
    s_axis_snap_tkeep => s_axis_snap_tkeep,
    s_axis_snap_tlast => s_axis_snap_tlast,
    s_axis_snap_tuser => s_axis_snap_tuser,
    s_axis_snap_tvalid => s_axis_snap_tvalid,
    m_axis_snap_tready => m_axis_snap_tready,
    s_axis_udp_tdata => s_axis_udp_tdata,
    s_axis_udp_tid => s_axis_udp_tid,
    s_axis_udp_tkeep => s_axis_udp_tkeep,
    s_axis_udp_tlast => s_axis_udp_tlast,
    s_axis_udp_tuser => s_axis_udp_tuser,
    s_axis_udp_tvalid => s_axis_udp_tvalid,
    clk => clk,
    rfsoc_byp_aresetn => rfsoc_byp_aresetn,
    rfsoc_byp_s_axi_awaddr => rfsoc_byp_s_axi_awaddr,
    rfsoc_byp_s_axi_awvalid => rfsoc_byp_s_axi_awvalid,
    rfsoc_byp_s_axi_wdata => rfsoc_byp_s_axi_wdata,
    rfsoc_byp_s_axi_wstrb => rfsoc_byp_s_axi_wstrb,
    rfsoc_byp_s_axi_wvalid => rfsoc_byp_s_axi_wvalid,
    rfsoc_byp_s_axi_bready => rfsoc_byp_s_axi_bready,
    rfsoc_byp_s_axi_araddr => rfsoc_byp_s_axi_araddr,
    rfsoc_byp_s_axi_arvalid => rfsoc_byp_s_axi_arvalid,
    rfsoc_byp_s_axi_rready => rfsoc_byp_s_axi_rready,
    m_axis_udp_tdata => m_axis_udp_tdata,
    m_axis_udp_tid => m_axis_udp_tid,
    m_axis_udp_tkeep => m_axis_udp_tkeep,
    m_axis_udp_tlast => m_axis_udp_tlast,
    m_axis_udp_tuser => m_axis_udp_tuser,
    m_axis_udp_tvalid => m_axis_udp_tvalid,
    s_axis_0_tready => s_axis_0_tready,
    s_axis_1_tready => s_axis_1_tready,
    s_axis_2_tready => s_axis_2_tready,
    s_axis_3_tready => s_axis_3_tready,
    s_axis_snap_tready => s_axis_snap_tready,
    m_axis_snap_tdata => m_axis_snap_tdata,
    m_axis_snap_tid => m_axis_snap_tid,
    m_axis_snap_tkeep => m_axis_snap_tkeep,
    m_axis_snap_tlast => m_axis_snap_tlast,
    m_axis_snap_tvalid => m_axis_snap_tvalid,
    m_axis_snap_tuser => m_axis_snap_tuser,
    s_axis_udp_tready => s_axis_udp_tready,
    rfsoc_byp_s_axi_awready => rfsoc_byp_s_axi_awready,
    rfsoc_byp_s_axi_wready => rfsoc_byp_s_axi_wready,
    rfsoc_byp_s_axi_bresp => rfsoc_byp_s_axi_bresp,
    rfsoc_byp_s_axi_bvalid => rfsoc_byp_s_axi_bvalid,
    rfsoc_byp_s_axi_arready => rfsoc_byp_s_axi_arready,
    rfsoc_byp_s_axi_rdata => rfsoc_byp_s_axi_rdata,
    rfsoc_byp_s_axi_rresp => rfsoc_byp_s_axi_rresp,
    rfsoc_byp_s_axi_rvalid => rfsoc_byp_s_axi_rvalid
  );
end structural;
