--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Mon Sep 23 09:08:52 2024
--Host        : fpgdev running 64-bit Ubuntu 18.04.6 LTS
--Command     : generate_target rfsoc_byp_bd_wrapper.bd
--Design      : rfsoc_byp_bd_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rfsoc_byp_bd_wrapper is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC;
    CLK_IN1_D_clk_p : in STD_LOGIC;
    aresetn : in STD_LOGIC_VECTOR ( 0 to 0 );
    irig_comp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    irig_trig_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_snap_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    m_axis_snap_tid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_snap_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_snap_tlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_snap_tready : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_snap_tuser : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_snap_tvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_udp_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    m_axis_udp_tid : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_udp_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_udp_tlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_udp_tready : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_udp_tuser : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_udp_tvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset_rtl : in STD_LOGIC;
    s_axis_0_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_0_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_0_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_0_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_0_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_0_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_0_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_1_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_1_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_1_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_1_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_1_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_1_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_1_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_2_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_2_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_2_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_2_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_2_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_2_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_2_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_3_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_3_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_3_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_3_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_3_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_3_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_3_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_snap_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s_axis_snap_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_snap_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_snap_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_snap_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_snap_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_snap_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_udp_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s_axis_udp_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_udp_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_udp_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_udp_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_udp_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_udp_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end rfsoc_byp_bd_wrapper;

architecture STRUCTURE of rfsoc_byp_bd_wrapper is
  component rfsoc_byp_bd is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC;
    CLK_IN1_D_clk_p : in STD_LOGIC;
    reset_rtl : in STD_LOGIC;
    aresetn : in STD_LOGIC_VECTOR ( 0 to 0 );
    irig_comp_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    irig_trig_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_snap_tid : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_snap_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    m_axis_snap_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_snap_tlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_snap_tuser : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_snap_tvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_snap_tready : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_udp_tid : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_udp_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    m_axis_udp_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_udp_tlast : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_udp_tuser : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axis_udp_tvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
    m_axis_udp_tready : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_0_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_0_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_0_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_0_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_0_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_0_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_0_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_1_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_1_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_1_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_1_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_1_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_1_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_1_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_2_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_2_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_2_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_2_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_2_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_2_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_2_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_3_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_3_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    s_axis_3_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_3_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_3_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_3_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_3_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_snap_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_snap_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s_axis_snap_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_snap_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_snap_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_snap_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_snap_tready : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_udp_tid : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_udp_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s_axis_udp_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_udp_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_udp_tuser : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_udp_tvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axis_udp_tready : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component rfsoc_byp_bd;
begin
rfsoc_byp_bd_i: component rfsoc_byp_bd
     port map (
      CLK_IN1_D_clk_n => CLK_IN1_D_clk_n,
      CLK_IN1_D_clk_p => CLK_IN1_D_clk_p,
      aresetn(0) => aresetn(0),
      irig_comp_in(0) => irig_comp_in(0),
      irig_trig_in(0) => irig_trig_in(0),
      m_axis_snap_tdata(511 downto 0) => m_axis_snap_tdata(511 downto 0),
      m_axis_snap_tid(15 downto 0) => m_axis_snap_tid(15 downto 0),
      m_axis_snap_tkeep(63 downto 0) => m_axis_snap_tkeep(63 downto 0),
      m_axis_snap_tlast(0) => m_axis_snap_tlast(0),
      m_axis_snap_tready(0) => m_axis_snap_tready(0),
      m_axis_snap_tuser(31 downto 0) => m_axis_snap_tuser(31 downto 0),
      m_axis_snap_tvalid(0) => m_axis_snap_tvalid(0),
      m_axis_udp_tdata(511 downto 0) => m_axis_udp_tdata(511 downto 0),
      m_axis_udp_tid(7 downto 0) => m_axis_udp_tid(7 downto 0),
      m_axis_udp_tkeep(63 downto 0) => m_axis_udp_tkeep(63 downto 0),
      m_axis_udp_tlast(0) => m_axis_udp_tlast(0),
      m_axis_udp_tready(0) => m_axis_udp_tready(0),
      m_axis_udp_tuser(31 downto 0) => m_axis_udp_tuser(31 downto 0),
      m_axis_udp_tvalid(0) => m_axis_udp_tvalid(0),
      reset_rtl => reset_rtl,
      s_axis_0_tdata(127 downto 0) => s_axis_0_tdata(127 downto 0),
      s_axis_0_tid(7 downto 0) => s_axis_0_tid(7 downto 0),
      s_axis_0_tkeep(63 downto 0) => s_axis_0_tkeep(63 downto 0),
      s_axis_0_tlast(0) => s_axis_0_tlast(0),
      s_axis_0_tready(0) => s_axis_0_tready(0),
      s_axis_0_tuser(31 downto 0) => s_axis_0_tuser(31 downto 0),
      s_axis_0_tvalid(0) => s_axis_0_tvalid(0),
      s_axis_1_tdata(127 downto 0) => s_axis_1_tdata(127 downto 0),
      s_axis_1_tid(7 downto 0) => s_axis_1_tid(7 downto 0),
      s_axis_1_tkeep(63 downto 0) => s_axis_1_tkeep(63 downto 0),
      s_axis_1_tlast(0) => s_axis_1_tlast(0),
      s_axis_1_tready(0) => s_axis_1_tready(0),
      s_axis_1_tuser(31 downto 0) => s_axis_1_tuser(31 downto 0),
      s_axis_1_tvalid(0) => s_axis_1_tvalid(0),
      s_axis_2_tdata(127 downto 0) => s_axis_2_tdata(127 downto 0),
      s_axis_2_tid(7 downto 0) => s_axis_2_tid(7 downto 0),
      s_axis_2_tkeep(63 downto 0) => s_axis_2_tkeep(63 downto 0),
      s_axis_2_tlast(0) => s_axis_2_tlast(0),
      s_axis_2_tready(0) => s_axis_2_tready(0),
      s_axis_2_tuser(31 downto 0) => s_axis_2_tuser(31 downto 0),
      s_axis_2_tvalid(0) => s_axis_2_tvalid(0),
      s_axis_3_tdata(127 downto 0) => s_axis_3_tdata(127 downto 0),
      s_axis_3_tid(7 downto 0) => s_axis_3_tid(7 downto 0),
      s_axis_3_tkeep(63 downto 0) => s_axis_3_tkeep(63 downto 0),
      s_axis_3_tlast(0) => s_axis_3_tlast(0),
      s_axis_3_tready(0) => s_axis_3_tready(0),
      s_axis_3_tuser(31 downto 0) => s_axis_3_tuser(31 downto 0),
      s_axis_3_tvalid(0) => s_axis_3_tvalid(0),
      s_axis_snap_tdata(511 downto 0) => s_axis_snap_tdata(511 downto 0),
      s_axis_snap_tid(7 downto 0) => s_axis_snap_tid(7 downto 0),
      s_axis_snap_tkeep(63 downto 0) => s_axis_snap_tkeep(63 downto 0),
      s_axis_snap_tlast(0) => s_axis_snap_tlast(0),
      s_axis_snap_tready(0) => s_axis_snap_tready(0),
      s_axis_snap_tuser(31 downto 0) => s_axis_snap_tuser(31 downto 0),
      s_axis_snap_tvalid(0) => s_axis_snap_tvalid(0),
      s_axis_udp_tdata(511 downto 0) => s_axis_udp_tdata(511 downto 0),
      s_axis_udp_tid(7 downto 0) => s_axis_udp_tid(7 downto 0),
      s_axis_udp_tkeep(63 downto 0) => s_axis_udp_tkeep(63 downto 0),
      s_axis_udp_tlast(0) => s_axis_udp_tlast(0),
      s_axis_udp_tready(0) => s_axis_udp_tready(0),
      s_axis_udp_tuser(31 downto 0) => s_axis_udp_tuser(31 downto 0),
      s_axis_udp_tvalid(0) => s_axis_udp_tvalid(0)
    );
end STRUCTURE;
