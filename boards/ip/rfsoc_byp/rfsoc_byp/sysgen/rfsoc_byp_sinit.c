/**
* @file rfsoc_byp_sinit.c
*
* The implementation of the rfsoc_byp driver's static initialzation
* functionality.
*
* @note
*
* None
*
*/
#ifndef __linux__
#include "xstatus.h"
#include "xparameters.h"
#include "rfsoc_byp.h"
extern rfsoc_byp_Config rfsoc_byp_ConfigTable[];
/**
* Lookup the device configuration based on the unique device ID.  The table
* ConfigTable contains the configuration info for each device in the system.
*
* @param DeviceId is the device identifier to lookup.
*
* @return
*     - A pointer of data type rfsoc_byp_Config which
*    points to the device configuration if DeviceID is found.
*    - NULL if DeviceID is not found.
*
* @note    None.
*
*/
rfsoc_byp_Config *rfsoc_byp_LookupConfig(u16 DeviceId) {
    rfsoc_byp_Config *ConfigPtr = NULL;
    int Index;
    for (Index = 0; Index < XPAR_RFSOC_BYP_NUM_INSTANCES; Index++) {
        if (rfsoc_byp_ConfigTable[Index].DeviceId == DeviceId) {
            ConfigPtr = &rfsoc_byp_ConfigTable[Index];
            break;
        }
    }
    return ConfigPtr;
}
int rfsoc_byp_Initialize(rfsoc_byp *InstancePtr, u16 DeviceId) {
    rfsoc_byp_Config *ConfigPtr;
    Xil_AssertNonvoid(InstancePtr != NULL);
    ConfigPtr = rfsoc_byp_LookupConfig(DeviceId);
    if (ConfigPtr == NULL) {
        InstancePtr->IsReady = 0;
        return (XST_DEVICE_NOT_FOUND);
    }
    return rfsoc_byp_CfgInitialize(InstancePtr, ConfigPtr);
}
#endif
