/**
*
* @file rfsoc_byp_hw.h
*
* This header file contains identifiers and driver functions (or
* macros) that can be used to access the device.  The user should refer to the
* hardware device specification for more details of the device operation.
*/ 
#define RFSOC_BYP_TIME_IN_HI 0x104/**< time_in_hi */
#define RFSOC_BYP_X_SETUP 0xc/**< x_setup */
#define RFSOC_BYP_TX_PKT_WAIT 0x28/**< tx_pkt_wait */
#define RFSOC_BYP_TX_PKT_BYTES 0x10/**< tx_pkt_bytes */
#define RFSOC_BYP_TX_METADATA 0x8/**< tx_metadata */
#define RFSOC_BYP_IPTX_BASE 0x4/**< iptx_base */
#define RFSOC_BYP_CONTROL 0x0/**< control */
#define RFSOC_BYP_TIME_IN_LO 0x100/**< time_in_lo */
#define RFSOC_BYP_INTF_RST 0x400/**< intf_rst */
