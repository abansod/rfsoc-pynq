set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks clk_out1_base_c_clk_mmcm_200_0] \
                               -group [get_clocks {txoutclk_out[0]}] \
                               -group [get_clocks {rxoutclk_out[0]}] \ 
                               -group [get_clocks -include_generated_clocks clk_out2_base_c_clk_mmcm_200_0] \
                               -group [get_clocks -include_generated_clocks clk_out1_base_c_clk_mmcm_201_0] \
                               -group [get_clocks -include_generated_clocks {GTYE4_CHANNEL_TXOUTCLKPCS[0]}] \
                               -group [get_clocks -include_generated_clocks {GTYE4_CHANNEL_TXOUTCLKPCS[1]}] \
                               -group [get_clocks -include_generated_clocks {GTYE4_CHANNEL_TXOUTCLKPCS[2]}] \
                               -group [get_clocks -include_generated_clocks {GTYE4_CHANNEL_TXOUTCLKPCS[3]}]

