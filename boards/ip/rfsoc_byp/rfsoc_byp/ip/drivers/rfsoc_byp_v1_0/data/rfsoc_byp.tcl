proc generate {drv_handle} {
    xdefine_include_file $drv_handle "xparameters.h" "rfsoc_byp" "NUM_INSTANCES" "DEVICE_ID" "C_RFSOC_BYP_S_AXI_BASEADDR" "C_RFSOC_BYP_S_AXI_HIGHADDR" 
    xdefine_config_file $drv_handle "rfsoc_byp_g.c" "rfsoc_byp" "DEVICE_ID" "C_RFSOC_BYP_S_AXI_BASEADDR" 
    xdefine_canonical_xpars $drv_handle "xparameters.h" "rfsoc_byp" "DEVICE_ID" "C_RFSOC_BYP_S_AXI_BASEADDR" "C_RFSOC_BYP_S_AXI_HIGHADDR" 

}