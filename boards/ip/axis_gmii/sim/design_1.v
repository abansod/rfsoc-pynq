//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.1 (lin64) Build 3526262 Mon Apr 18 15:47:01 MDT 2022
//Date        : Mon Oct 14 17:28:31 2024
//Host        : fpgdev running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=4,numReposBlks=4,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=2,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (cfg_ifg_0,
    cfg_rx_enable_0,
    cfg_tx_enable_0,
    clk_0,
    clk_enable_0,
    error_bad_fcs_0,
    error_bad_frame_0,
    error_underflow_0,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_rxd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_txd,
    m_axis_ptp_ts_0,
    m_axis_ptp_ts_tag_0,
    m_axis_ptp_ts_valid_0,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast,
    m_axis_tready,
    m_axis_tuser,
    m_axis_tvalid,
    rst_0,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    s_axis_tready,
    s_axis_tuser,
    s_axis_tvalid,
    start_packet_rx,
    start_packet_tx);
  input [7:0]cfg_ifg_0;
  input cfg_rx_enable_0;
  input cfg_tx_enable_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.CLK_0 CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.CLK_0, ASSOCIATED_BUSIF s_axis:m_axis, ASSOCIATED_RESET rst_0, CLK_DOMAIN design_1_clk_0, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0" *) input clk_0;
  input clk_enable_0;
  output error_bad_fcs_0;
  output error_bad_frame_0;
  output error_underflow_0;
  input gmii_rx_dv;
  input gmii_rx_er;
  input [7:0]gmii_rxd;
  output gmii_tx_en;
  output gmii_tx_er;
  output [7:0]gmii_txd;
  output [95:0]m_axis_ptp_ts_0;
  output [15:0]m_axis_ptp_ts_tag_0;
  output m_axis_ptp_ts_valid_0;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axis, CLK_DOMAIN design_1_clk_0, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1" *) output [7:0]m_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TKEEP" *) output m_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TLAST" *) output m_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TREADY" *) input m_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TUSER" *) output [0:0]m_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis TVALID" *) output m_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.RST_0 RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.RST_0, INSERT_VIP 0, POLARITY ACTIVE_HIGH" *) input rst_0;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TDATA" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis, CLK_DOMAIN design_1_clk_0, FREQ_HZ 100000000, HAS_TKEEP 0, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.0, TDATA_NUM_BYTES 1, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 1" *) input [7:0]s_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TKEEP" *) input s_axis_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TLAST" *) input s_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TREADY" *) output s_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TUSER" *) input [0:0]s_axis_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis TVALID" *) input s_axis_tvalid;
  output start_packet_rx;
  output start_packet_tx;

  wire axis_gmii_rx_0_error_bad_fcs;
  wire axis_gmii_rx_0_error_bad_frame;
  wire [7:0]axis_gmii_rx_0_m_axis_TDATA;
  wire axis_gmii_rx_0_m_axis_TKEEP;
  wire axis_gmii_rx_0_m_axis_TLAST;
  wire axis_gmii_rx_0_m_axis_TREADY;
  wire [0:0]axis_gmii_rx_0_m_axis_TUSER;
  wire axis_gmii_rx_0_m_axis_TVALID;
  wire axis_gmii_rx_0_start_packet;
  wire axis_gmii_tx_0_error_underflow;
  wire axis_gmii_tx_0_gmii_tx_en;
  wire axis_gmii_tx_0_gmii_tx_er;
  wire [7:0]axis_gmii_tx_0_gmii_txd;
  wire [95:0]axis_gmii_tx_0_m_axis_ptp_ts;
  wire [15:0]axis_gmii_tx_0_m_axis_ptp_ts_tag;
  wire axis_gmii_tx_0_m_axis_ptp_ts_valid;
  wire axis_gmii_tx_0_start_packet;
  wire [7:0]cfg_ifg_0_1;
  wire cfg_rx_enable_0_1;
  wire cfg_tx_enable_0_1;
  wire clk_0_1;
  wire clk_enable_0_1;
  wire gmii_rx_dv_0_1;
  wire gmii_rx_er_0_1;
  wire [7:0]gmii_rxd_0_1;
  wire [0:0]mii_select_0_1;
  wire [95:0]ptp_ts_0_1;
  wire rst_0_1;
  wire [7:0]s_axis_0_1_TDATA;
  wire s_axis_0_1_TKEEP;
  wire s_axis_0_1_TLAST;
  wire s_axis_0_1_TREADY;
  wire [0:0]s_axis_0_1_TUSER;
  wire s_axis_0_1_TVALID;

  assign axis_gmii_rx_0_m_axis_TREADY = m_axis_tready;
  assign cfg_ifg_0_1 = cfg_ifg_0[7:0];
  assign cfg_rx_enable_0_1 = cfg_rx_enable_0;
  assign cfg_tx_enable_0_1 = cfg_tx_enable_0;
  assign clk_0_1 = clk_0;
  assign clk_enable_0_1 = clk_enable_0;
  assign error_bad_fcs_0 = axis_gmii_rx_0_error_bad_fcs;
  assign error_bad_frame_0 = axis_gmii_rx_0_error_bad_frame;
  assign error_underflow_0 = axis_gmii_tx_0_error_underflow;
  assign gmii_rx_dv_0_1 = gmii_rx_dv;
  assign gmii_rx_er_0_1 = gmii_rx_er;
  assign gmii_rxd_0_1 = gmii_rxd[7:0];
  assign gmii_tx_en = axis_gmii_tx_0_gmii_tx_en;
  assign gmii_tx_er = axis_gmii_tx_0_gmii_tx_er;
  assign gmii_txd[7:0] = axis_gmii_tx_0_gmii_txd;
  assign m_axis_ptp_ts_0[95:0] = axis_gmii_tx_0_m_axis_ptp_ts;
  assign m_axis_ptp_ts_tag_0[15:0] = axis_gmii_tx_0_m_axis_ptp_ts_tag;
  assign m_axis_ptp_ts_valid_0 = axis_gmii_tx_0_m_axis_ptp_ts_valid;
  assign m_axis_tdata[7:0] = axis_gmii_rx_0_m_axis_TDATA;
  assign m_axis_tkeep = axis_gmii_rx_0_m_axis_TKEEP;
  assign m_axis_tlast = axis_gmii_rx_0_m_axis_TLAST;
  assign m_axis_tuser[0] = axis_gmii_rx_0_m_axis_TUSER;
  assign m_axis_tvalid = axis_gmii_rx_0_m_axis_TVALID;
  assign rst_0_1 = rst_0;
  assign s_axis_0_1_TDATA = s_axis_tdata[7:0];
  assign s_axis_0_1_TKEEP = s_axis_tkeep;
  assign s_axis_0_1_TLAST = s_axis_tlast;
  assign s_axis_0_1_TUSER = s_axis_tuser[0];
  assign s_axis_0_1_TVALID = s_axis_tvalid;
  assign s_axis_tready = s_axis_0_1_TREADY;
  assign start_packet_rx = axis_gmii_rx_0_start_packet;
  assign start_packet_tx = axis_gmii_tx_0_start_packet;
  design_1_axis_gmii_rx_0_0 axis_gmii_rx_0
       (.cfg_rx_enable(cfg_rx_enable_0_1),
        .clk(clk_0_1),
        .clk_enable(clk_enable_0_1),
        .error_bad_fcs(axis_gmii_rx_0_error_bad_fcs),
        .error_bad_frame(axis_gmii_rx_0_error_bad_frame),
        .gmii_rx_dv(gmii_rx_dv_0_1),
        .gmii_rx_er(gmii_rx_er_0_1),
        .gmii_rxd(gmii_rxd_0_1),
        .m_axis_tdata(axis_gmii_rx_0_m_axis_TDATA),
        .m_axis_tkeep(axis_gmii_rx_0_m_axis_TKEEP),
        .m_axis_tlast(axis_gmii_rx_0_m_axis_TLAST),
        .m_axis_tready(axis_gmii_rx_0_m_axis_TREADY),
        .m_axis_tuser(axis_gmii_rx_0_m_axis_TUSER),
        .m_axis_tvalid(axis_gmii_rx_0_m_axis_TVALID),
        .mii_select(mii_select_0_1),
        .ptp_ts(ptp_ts_0_1),
        .rst(rst_0_1),
        .start_packet(axis_gmii_rx_0_start_packet));
  design_1_axis_gmii_tx_0_0 axis_gmii_tx_0
       (.cfg_ifg(cfg_ifg_0_1),
        .cfg_tx_enable(cfg_tx_enable_0_1),
        .clk(clk_0_1),
        .clk_enable(clk_enable_0_1),
        .error_underflow(axis_gmii_tx_0_error_underflow),
        .gmii_tx_en(axis_gmii_tx_0_gmii_tx_en),
        .gmii_tx_er(axis_gmii_tx_0_gmii_tx_er),
        .gmii_txd(axis_gmii_tx_0_gmii_txd),
        .m_axis_ptp_ts(axis_gmii_tx_0_m_axis_ptp_ts),
        .m_axis_ptp_ts_tag(axis_gmii_tx_0_m_axis_ptp_ts_tag),
        .m_axis_ptp_ts_valid(axis_gmii_tx_0_m_axis_ptp_ts_valid),
        .mii_select(mii_select_0_1),
        .ptp_ts(ptp_ts_0_1),
        .rst(rst_0_1),
        .s_axis_tdata(s_axis_0_1_TDATA),
        .s_axis_tkeep(s_axis_0_1_TKEEP),
        .s_axis_tlast(s_axis_0_1_TLAST),
        .s_axis_tready(s_axis_0_1_TREADY),
        .s_axis_tuser(s_axis_0_1_TUSER),
        .s_axis_tvalid(s_axis_0_1_TVALID),
        .start_packet(axis_gmii_tx_0_start_packet));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(mii_select_0_1));
  design_1_xlconstant_1_0 xlconstant_1
       (.dout(ptp_ts_0_1));
endmodule
