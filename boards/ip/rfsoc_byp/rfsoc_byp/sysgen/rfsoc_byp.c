#include "rfsoc_byp.h"
#ifndef __linux__
int rfsoc_byp_CfgInitialize(rfsoc_byp *InstancePtr, rfsoc_byp_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->rfsoc_byp_BaseAddress = ConfigPtr->rfsoc_byp_BaseAddress;

    InstancePtr->IsReady = 1;
    return XST_SUCCESS;
}
#endif
void rfsoc_byp_time_in_hi_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 260, Data);
}
u32 rfsoc_byp_time_in_hi_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 260);
    return Data;
}
void rfsoc_byp_x_setup_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 12, Data);
}
u32 rfsoc_byp_x_setup_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 12);
    return Data;
}
void rfsoc_byp_tx_pkt_wait_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 40, Data);
}
u32 rfsoc_byp_tx_pkt_wait_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 40);
    return Data;
}
void rfsoc_byp_tx_pkt_bytes_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 16, Data);
}
u32 rfsoc_byp_tx_pkt_bytes_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 16);
    return Data;
}
void rfsoc_byp_tx_metadata_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 8, Data);
}
u32 rfsoc_byp_tx_metadata_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 8);
    return Data;
}
void rfsoc_byp_iptx_base_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 4, Data);
}
u32 rfsoc_byp_iptx_base_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 4);
    return Data;
}
void rfsoc_byp_control_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 0, Data);
}
u32 rfsoc_byp_control_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 0);
    return Data;
}
void rfsoc_byp_time_in_lo_write(rfsoc_byp *InstancePtr, u32 Data) {

    Xil_AssertVoid(InstancePtr != NULL);

    rfsoc_byp_WriteReg(InstancePtr->rfsoc_byp_BaseAddress, 256, Data);
}
u32 rfsoc_byp_time_in_lo_read(rfsoc_byp *InstancePtr) {

    u32 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 256);
    return Data;
}
u8 rfsoc_byp_intf_rst_read(rfsoc_byp *InstancePtr) {

    u8 Data;
    Xil_AssertVoid(InstancePtr != NULL);

    Data = rfsoc_byp_ReadReg(InstancePtr->rfsoc_byp_BaseAddress, 1024);
    return Data;
}
