//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.1 (lin64) Build 3526262 Mon Apr 18 15:47:01 MDT 2022
//Date        : Mon Oct 14 17:28:31 2024
//Host        : fpgdev running 64-bit Ubuntu 18.04.6 LTS
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (cfg_ifg_0,
    cfg_rx_enable_0,
    cfg_tx_enable_0,
    clk_0,
    clk_enable_0,
    error_bad_fcs_0,
    error_bad_frame_0,
    error_underflow_0,
    gmii_rx_dv,
    gmii_rx_er,
    gmii_rxd,
    gmii_tx_en,
    gmii_tx_er,
    gmii_txd,
    m_axis_ptp_ts_0,
    m_axis_ptp_ts_tag_0,
    m_axis_ptp_ts_valid_0,
    m_axis_tdata,
    m_axis_tkeep,
    m_axis_tlast,
    m_axis_tready,
    m_axis_tuser,
    m_axis_tvalid,
    rst_0,
    s_axis_tdata,
    s_axis_tkeep,
    s_axis_tlast,
    s_axis_tready,
    s_axis_tuser,
    s_axis_tvalid,
    start_packet_rx,
    start_packet_tx);
  input [7:0]cfg_ifg_0;
  input cfg_rx_enable_0;
  input cfg_tx_enable_0;
  input clk_0;
  input clk_enable_0;
  output error_bad_fcs_0;
  output error_bad_frame_0;
  output error_underflow_0;
  input gmii_rx_dv;
  input gmii_rx_er;
  input [7:0]gmii_rxd;
  output gmii_tx_en;
  output gmii_tx_er;
  output [7:0]gmii_txd;
  output [95:0]m_axis_ptp_ts_0;
  output [15:0]m_axis_ptp_ts_tag_0;
  output m_axis_ptp_ts_valid_0;
  output [7:0]m_axis_tdata;
  output m_axis_tkeep;
  output m_axis_tlast;
  input m_axis_tready;
  output [0:0]m_axis_tuser;
  output m_axis_tvalid;
  input rst_0;
  input [7:0]s_axis_tdata;
  input s_axis_tkeep;
  input s_axis_tlast;
  output s_axis_tready;
  input [0:0]s_axis_tuser;
  input s_axis_tvalid;
  output start_packet_rx;
  output start_packet_tx;

  wire [7:0]cfg_ifg_0;
  wire cfg_rx_enable_0;
  wire cfg_tx_enable_0;
  wire clk_0;
  wire clk_enable_0;
  wire error_bad_fcs_0;
  wire error_bad_frame_0;
  wire error_underflow_0;
  wire gmii_rx_dv;
  wire gmii_rx_er;
  wire [7:0]gmii_rxd;
  wire gmii_tx_en;
  wire gmii_tx_er;
  wire [7:0]gmii_txd;
  wire [95:0]m_axis_ptp_ts_0;
  wire [15:0]m_axis_ptp_ts_tag_0;
  wire m_axis_ptp_ts_valid_0;
  wire [7:0]m_axis_tdata;
  wire m_axis_tkeep;
  wire m_axis_tlast;
  wire m_axis_tready;
  wire [0:0]m_axis_tuser;
  wire m_axis_tvalid;
  wire rst_0;
  wire [7:0]s_axis_tdata;
  wire s_axis_tkeep;
  wire s_axis_tlast;
  wire s_axis_tready;
  wire [0:0]s_axis_tuser;
  wire s_axis_tvalid;
  wire start_packet_rx;
  wire start_packet_tx;

  design_1 design_1_i
       (.cfg_ifg_0(cfg_ifg_0),
        .cfg_rx_enable_0(cfg_rx_enable_0),
        .cfg_tx_enable_0(cfg_tx_enable_0),
        .clk_0(clk_0),
        .clk_enable_0(clk_enable_0),
        .error_bad_fcs_0(error_bad_fcs_0),
        .error_bad_frame_0(error_bad_frame_0),
        .error_underflow_0(error_underflow_0),
        .gmii_rx_dv(gmii_rx_dv),
        .gmii_rx_er(gmii_rx_er),
        .gmii_rxd(gmii_rxd),
        .gmii_tx_en(gmii_tx_en),
        .gmii_tx_er(gmii_tx_er),
        .gmii_txd(gmii_txd),
        .m_axis_ptp_ts_0(m_axis_ptp_ts_0),
        .m_axis_ptp_ts_tag_0(m_axis_ptp_ts_tag_0),
        .m_axis_ptp_ts_valid_0(m_axis_ptp_ts_valid_0),
        .m_axis_tdata(m_axis_tdata),
        .m_axis_tkeep(m_axis_tkeep),
        .m_axis_tlast(m_axis_tlast),
        .m_axis_tready(m_axis_tready),
        .m_axis_tuser(m_axis_tuser),
        .m_axis_tvalid(m_axis_tvalid),
        .rst_0(rst_0),
        .s_axis_tdata(s_axis_tdata),
        .s_axis_tkeep(s_axis_tkeep),
        .s_axis_tlast(s_axis_tlast),
        .s_axis_tready(s_axis_tready),
        .s_axis_tuser(s_axis_tuser),
        .s_axis_tvalid(s_axis_tvalid),
        .start_packet_rx(start_packet_rx),
        .start_packet_tx(start_packet_tx));
endmodule
