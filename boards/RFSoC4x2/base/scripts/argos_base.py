#!/bin/env ipython
'''
Author: Amit Bansod.
'''
import pynq, time, struct, sys, logging, socket
#import numpy as np
#import matplotlib
#matplotlib.use('TkAgg')
#import matplotlib.pyplot as plt
#import math as m
from select import select
from argparse import ArgumentParser
from network import IpAddress as ip
import os.path 
usleep = lambda x: time.sleep(x/1000000.0)

timeout=1

inp='none'
boffile = 'argos_base.bit'
    
fpga=[]

def exit_fail():
    exit()

def exit_clean(): 
    exit()

if __name__ == '__main__':

    usage = 'argos_base.py <INDEX>  %prog [options]'
    p = ArgumentParser(description=usage)
    p.add_argument('--noprogram', dest='noprogram', action='store_true',
        help='Don\'t program the board, only read the xclbin file.')   
    p.add_argument('--firmware', dest='firmware', action='store', type=str, default="argos_base.bit",
        help='Firmware file to be programmed.')   
    p.add_argument('--monitor_qsfp28', dest='monitor_qsfp28', action='store_true',
        help='Monitor QSFP28 Registers for dropped packets.') 
    p.add_argument('--qsfp28_0_src_ip', dest='qsfp28_0_src_ip', type=str,   default='192.168.2.32', choices=['192.168.2.32','192.168.2.33','192.168.2.34','192.168.2.35','192.168.2.36','192.168.2.37','192.168.2.38'],      action='store',
        help='Source Address QSFP28_0.') 
    p.add_argument('--mcast_src_addr0', dest='mcast_src0_base', type=str,   default='225.0.0.100',      action='store',
        help='Multicast Source Address QSFP28_0 base.') 
    p.add_argument('--mcast_src_num0', dest='mcast_src0_num', type=int,       default=4,        action='store',
        help='Number of Multicast Source Addresses for QSFP28_0 .')
    p.add_argument('--mcast_src_port', dest='mcast_src_port', type=int,  default=7148,   action='store',
        help='Multicast Source Port.')
    p.add_argument('--mcast_dst_port', dest='mcast_dest_port', type=int, default=7148,  action='store',
        help='Multicast Destination Port.')
    p.add_argument('--mcast_dst_base', dest='mcast_dest_base', type=str, default='239.0.0.150',      action='store',
        help='Multicast Destination Address base.')
    p.add_argument('--mcast_dst_num', dest='mcast_dest_num', type=int,   default=4, choices=[1,2,4,8,16,32,64],       action='store',
        help='Number of Multicast Destination Addresses.')

    args = p.parse_args()
try:

    print('Connecting to active device... '),
    fpga = pynq.Device.active_device
    time.sleep(1)

    #if fpga.is_connected():
    #    print ('ok\n')
    #else:
    #    print ('ERROR connecting to device \n')
    #    exit_fail()
    
    localfile = args.firmware
    print("firmware:{}".format(localfile))
    if (localfile == "") or (not os.path.isfile(localfile)):
       print("Please provide a valid firmware file")
       exit()

    sys.stdout.flush()
    print ('------------------------')

    if not args.noprogram:
        print ('Programming FPGA...'),
        ol = pynq.Overlay(localfile)
        time.sleep(2)
        if ol.is_loaded():
           print ('ok')
        else:
           print("programming file could not be downloaded. \nExiting...")
           exit()
    else:
        print ('------------------------')
        print ('Reading FPGA Firmware file...'),
        ol = pynq.Overlay(localfile, download=False)
        time.sleep(2)
        print ('ok')

    
    fpga = pynq.Device.active_device
    ol.ip_dict.keys() #get_system_information(filename=localfile)
    btns_gpio = ol.btns_gpio
    leds_gpio = ol.leds_gpio
    rgbleds_gpio = ol.rgbleds_gpio
    sws_gpio = ol.sws_gpio
    pmod0= ol.axi_gpio_1
    pmod1= ol.axi_gpio_2

    #cmac
    krnl_cmac = ol.udp_core_100g_ip_0
    krnl_pfb  = ol.rfsoc_byp_0

    src_ip_qsfp28_0      = ip.str2ip(args.qsfp28_0_src_ip)   

    mcast_dest_base_addr = ip.str2ip(args.mcast_dest_base)

    mcast_src_ip_qsfp28_0 = ip.str2ip(args.mcast_src0_base)
    mcast_ports           = (args.mcast_src_port << 16) | args.mcast_src_port # TODO

    mcast_dest_ports      = (args.mcast_dest_port << 16) | args.mcast_dest_port
   
    krnl_pfb.write(0x0, 0) 

    print ('iConfiguring transmitter/receiver core...')
    print ('unsubscribing from old mcast groups...')
    addr = 0x0 + 0x2000
    igmp_register_0_old = krnl_cmac.read(addr)
    igmp_register_old_data_0     = igmp_register_0_old & 0xfffffff5
    krnl_cmac.write(addr,igmp_register_old_data_0) # leave mcast groups, QSFP28_0

    sys.stdout.flush()

    addr = 0x0
    krnl_cmac.write(addr,0x35069EEA) # 100G Core 0 Source MAC Lower
    addr = 0x0 + 4
    krnl_cmac.write(addr,0x0000000A) # 100G Core 0 Source MAC Upper

    addr = 0x0 + 0xC
    krnl_cmac.write(addr,0x5e00019B) # 100G Core 0 Destination MAC Lower
    addr = 0x0 + 0x10
    krnl_cmac.write(addr,0x00000100) # 100G Core 0 Destination MAC Upper

    addr = 0x0 + 0x24
    krnl_cmac.write(addr,mcast_src_ip_qsfp28_0) # mcast destination ip, Applicable when using unicast mode

    addr = 0x0 + 0x28
    krnl_cmac.write(addr,src_ip_qsfp28_0) # 100G Core 0 Source IP
    read = krnl_cmac.read(addr)
    print("QSFP28 Core 0 IP",ip.ip2str(read),"\n")
    
    addr = 0x0 + 0x2C
    krnl_cmac.write(addr,mcast_ports) # udp src and dest port, 7148

    # do not strip headers from unknown packets, QSFP28_0
    addr = 0x0 + 0x38
    read_data = krnl_cmac.read(addr)
    control_new = read_data   & 0xfffaf0ff  #0xfffaffff
    control_new = control_new | 0x00300000 # 5 => filter incoming data on destination ip, mac
    krnl_cmac.write(addr,control_new) # 100G Core 0 filter control

    # LUT Mode Setup, QSFP28_0
    addr = 0x0 + 0x48
    control_new = read_data & 0xffffffef # enable udp checksum
    control_new = read_data | 0x00000020
    krnl_cmac.write(addr,control_new) 

    print ('done\n')

    print ('Setting destination multicast addresses...'), 
    # Setting destination multicast addresses

    # QSFP28 0
    j = 0
    mcast_ip_dup_factor = int(64/args.mcast_dest_num)
    for i in range(64):

        addr = 0x0 + 0x1800 + i*4
        mac_lo_addr = 0x0 + 0x1000 + i*4
        mac_hi_addr = 0x0 + 0x1400 + i*4
        port_addr = 0x0 + 0x1C00 + i*4

        #if i > 0 and i%mcast_ip_dup_factor==0 :
        #    j = j + 1

        value = mcast_dest_base_addr + i            
        mac_lo = (0x5e000000 |  (0x00ffffff & mcast_dest_base_addr)) + i
        mac_hi = 0x00000100 # 9a01025e0001 byte-reversed => 01005e02019a

        krnl_cmac.write(mac_lo_addr,mac_lo)
        krnl_cmac.write(mac_hi_addr,mac_hi)

        krnl_cmac.write(addr,value)

        krnl_cmac.write(port_addr,mcast_dest_ports)


    print ('done\n')

    print ('Configuring IGMP Subscriptions...')
  
    # IGMP Subscription
    print ("wait before sending igmp requests...: \n")
    usleep(1000000)
    addr = 0x0 + 0x2000
    mcast_src_num_qsfp28_0 = args.mcast_src0_num

    #igmp_register_data_qsfp28_0     = (mcast_src_num_qsfp28_0 << 24) | 0x00000005
    #krnl_cmac.write(addr,igmp_register_data_qsfp28_0) # leave mcast groups, QSFP28_0

    for i in range(mcast_src_num_qsfp28_0):
        addr = 0x0 + 0x2004 + i*4
        value = mcast_src_ip_qsfp28_0 + i
        krnl_cmac.write(addr,value)
        read = krnl_cmac.read(addr)
        print("Address:",ip.ip2str(read),"\n")

    usleep(10000000)
    #subscribe back again usleep(2000000)
    # subscribe back again

    print ('done\n')

    print ('Subscribing...')
    addr = 0x0 + 0x2000
    igmp_register_data_qsfp28_0     = (mcast_src_num_qsfp28_0 << 24) | 0x0000000D
    krnl_cmac.write(addr,igmp_register_data_qsfp28_0)
    read = krnl_cmac.read(addr)
    print("IGMP Control #0",hex(read),"\n")

    # packet size 8000 bytes, TODO
    addr = 0x0 + 0x10
    krnl_pfb.write(addr,0x00001f40) #1f40
    # heap size 32000, TODO
    addr = 0x0 + 0x0C
    krnl_pfb.write(addr,0x04080804)

    addr = 0x0

    spead_ctrl = 0x00000C09

    if args.n_bits == 8:
        spead_ctrl = 0x00002C09

    krnl_pfb.write(addr,spead_ctrl)

    print ('done\n')

    print ('---------------------------')
    print ('Resetting ...')
    sys.stdout.flush()
    print ('done\n')

    usleep(5000)
    spead_ctrl = spead_ctrl & 0xfffffdff
    krnl_pfb.write(addr,spead_ctrl)
    spead_ctrl = spead_ctrl | 0x00000200
    krnl_pfb.write(addr,spead_ctrl)
    usleep(5000)
    print ('Enabling output...'),
    spead_ctrl = spead_ctrl & 0xfffffdff
    krnl_pfb.write(addr,spead_ctrl)

    time.sleep(2)

    print ("configuring the f-engine")
    spead_ctrl = spead_ctrl | 0x00004000
    krnl_pfb.write(0x0,spead_ctrl)
    print ('done\n')
   
    #firmware = krnl_pfb.read(0x500)

    #print("firmware timestamp {}".format(firmware))
   

    if args.monitor_qsfp28 or args.monitor_feng or args.plotadc :     

        while(1):

            if args.monitor_qsfp28:

                print ('Reading QSFP28 Core Registers...')
                read_data = krnl_cmac.read(0x4C)
                print("Core 0 Received UDP Packet Counter:", hex(read_data))
                read_data = krnl_cmac.read(0x50)
                print("Core 0 Received Ping Packet Counter:", hex(read_data))
                read_data = krnl_cmac.read(0x54)
                print("Core 0 Arp Counter:", hex(read_data))
                read_data = krnl_cmac.read(0x60)
                print("Core 0 Dropped MAC Address Counter:", hex(read_data))
                read_data = krnl_cmac.read(0x64)
                print("Core 0 Dropped IP Address Counter:", hex(read_data))
                read_data = krnl_cmac.read(0x68)
                print("Core 0 Received Port Packet Counter:", hex(read_data))
                
                read_data = krnl_cmac.read(0x0)
                print("Core 0 MAC:", hex(read_data))

            if args.monitor_feng:
                print ('Reading F Engine Registers...')

                print("ctrls {}".format(krnl_ctr0.register_map)) 
                #plt.show()
             
            rlist, _, _ = select([sys.stdin], [], [], timeout)

            if rlist:
               inp = sys.stdin.read(1)
            if inp == 'q':
               break
            else:
               sys.stderr.write(".")
               #plt.clf()
               continue
 
    print ('==========================')

except KeyboardInterrupt:
     exit_clean()
except Exception as inst:
     exit_fail()

exit_clean()


