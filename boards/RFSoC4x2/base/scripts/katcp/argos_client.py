#!/bin/env ipython
'''
Author: Amit Bansod.
'''
import pynq, time, struct, sys, logging, socket
from array import array
from select import select
from argparse import ArgumentParser
from network import IpAddress as ip
import gc 
import json

class RFSoCInterface:
    """
    RFSoC 4x2 Board Client for argos base overlay
    """
    
    def __init__(self, firmwarefile):
        """
        Init
        """
        self._firmwarefile = firmwarefile
        self._fpga = None
        self._client = None

    def setFirmware(self, firmwarefile):
        """
        Set the firmware-file used for the client
        """
        self._firmwarefile = firmwarefile
        self._fpga = None
        self._client = None

    async def connect(self):
        """
        Connects to a client.
        """
        self._fpga = pynq.Device.active_device
        info = self._fpga.clocks
        print(info)
    async def is_connected(self):
        """
        Returns true if connection to Alveo exists and is active
        """
        self._client = pynq.Overlay(self._firmwarefile, download=False)
        return self._client.is_loaded()

    async def program(self):
        """
        Programs FPGA with the chosen firmware
        """
        
        self._client = pynq.Overlay(self._firmwarefile, dtbo='pl_x.dtbo')

        if not self._client.is_loaded():
            raise RuntimeError(f"Error loading firmware: {self._firmwarefile}")

    async def get_fpga_clock(self):
        """Return clock speed of the FPGA"""
        clk = round(self._fpga.clocks['clock0']['frequency'])
        return clk

    async def initialize(self):
        """
        Connect to FPGA and try to read system information. Reprograms FPGA if this fails.
        """
        self._client = pynq.Overlay(self._firmwarefile, dtbo='pl_x.dtbo')

        await self.connect()
        try:
            self._client.is_loaded()
        except Exception as E:
            await self.program()

            self._client.is_loaded()
        print("init done")

    async def reset_dpc(self):
        """
        reset register for dpc
        """
        self._client.rfsoc_byp_0.write(0x0,1)


    async def start_dpc(self):
        """
        reset register for dpc
        """
        self._client.rfsoc_byp_0.write(0x0,0x2)


    async def start_1pps(self):
        """
        reset register for dpc
        """

        # write ntp time, 48 bits
        #hi
        self._client.rfsoc_byp_0.write(0x104,0x6dff)
        #lo
        self._client.rfsoc_byp_0.write(0x100,0x34853845)

        # enable pps trigger
        self._client.rfsoc_byp_0.write(0x0,0x6)

    async def dpc_start_status(self):
        """
        read dpc status
        """
        reg = self._client.rfsoc_byp_0.read(0x400)
        val = (reg & 0x2)>>1
        return val

    async def dpc_1pps_status(self):
        """
        read dpc 1pps status
        """
        reg = self._client.rfsoc_byp_0.read(0x400)
        val = (reg & 0x4)>>2
        return val

    async def configure(self,mac0=None,ip0="192.168.2.32", **kwargs):
        """
        Set mac and IP for the NIC.
        """
        print("configuring mac/ip qsfp28")
        await self.connect()
        try:
            self._client.is_loaded()

            await self.set_mac(self._client.udp_core_100g_ip_0, mac0, 0)
            read = await  self.set_ip(self._client.udp_core_100g_ip_0, ip0, 0)

            await self.configure_input_filter_control(self._client.udp_core_100g_ip_0,0) 

        except Exception as E:
            pass
        print("done")

    async def set_mac(self, kernel_wizard, mac, offset):
        """
        Set a mac address to a register with given offset (0 for mac0, 0x4000
        for mac1) using specified kernel_wizard
        """
        
        # 100G Core 0 Source MAC Lower
        addr = 0x0 + offset
        kernel_wizard.write(
            addr, 0x35069EEA)
        # 100G Core 0 Source MAC Upper
        addr = 0x0 + offset + 4
        kernel_wizard.write(
            addr, 0x0000000A)

    async def set_ip(self, kernel_wizard, ip, offset):
        """
        Set an IP address to a register with given offset (0 for ip0, 0x4000
        for ip1) using specified kernel_wizard
        """

        addr = 0x0 + 0x28 + offset
        kernel_wizard.write(addr, ip.str2ip)
        read = kernel_wizard.read(addr)
        return read


    async def configure_input_filter_control(self, kernel_wizard, offset):
        """
        Configure input filter control
        """
        
        addr = 0x0 + offset + 0x38
        read_data = kernel_wizard.read(addr)
        #control_new = read_data | 0x000f0307  # 0xfffaffff
        # 5 => filter incoming data on destination IP, mac
        #control_new = control_new | 0x00300000
        control_new  = 0x000a0007 #0x000a0007
        kernel_wizard.write(
            addr, control_new)
        read_data = kernel_wizard.read(addr)
        print("Input control",hex(read_data))

    async def LUT_mode_setup(self, kernel_wizard, offset):
        """
        Set the 100GbE ports for LUT Farm Mode
        """
        addr = 0x0 + offset + 0x48
        read_data = kernel_wizard.read(addr)
        control_new = read_data & 0xffffffef  # enable UDP checksum
        control_new = read_data | 0x00000020
        kernel_wizard.write(addr, control_new)

    async def set_destination_multicast_groups(self, kernel_wizard, number_of_groups, dest_ip, dest_port, offset):
        """
        Write registers for the destination multicast groups
        """
      
        mcast_dest_ports = (dest_port << 16) | dest_port
        for i in range(number_of_groups):
            addr = 0x0 + offset + 0x1800 + i * 4
            mac_lo_addr = 0x0 +  offset + 0x1000 + i * 4
            mac_hi_addr = 0x0 +  offset + 0x1400 + i * 4
            port_addr = 0x0 +  offset + 0x1C00 + i * 4

            value = ip.str2ip(dest_ip) + i
            mac_lo = (0x5e000000 | (0x00ffffff & ip.str2ip(dest_ip))) + i
            mac_hi = 0x00000100  # 9a01025e0001 byte-reversed => 01005e02019a

            kernel_wizard.write(mac_lo_addr, mac_lo)
            kernel_wizard.write(mac_hi_addr, mac_hi)
            kernel_wizard.write(addr, value)
            kernel_wizard.write(port_addr, mcast_dest_ports)


    async def configure_output(self, dest_ip, dest_port, number_of_groups=4, channels_per_group=1, packet_size=8000, board_id=0x08):
        """
        Configure RFSoC 100G output
        Args:
            dest_ip
            dest_port
            number_of_groups
            channels_per_group
            board_id            Board ID [0 .. 255] inserted into the SPEAD header
        """
        print("configuring output...") 
        await self.LUT_mode_setup(self._client.udp_core_100g_ip_0, 0)

        await self.set_destination_multicast_groups(self._client.udp_core_100g_ip_0, number_of_groups, dest_ip, dest_port, 0)
        print("done")
    async def read_qsfp28_registers(self):
        """
        reads various registers on qsfp interface
        """
        qsfp_data = {}

        read_data = self._client.udp_core_100g_ip_0.read(0x4C)
        qsfp_data['udp_packet_counter'] = hex(read_data)

        read_data = self._client.udp_core_100g_ip_0.read(0x50)
        qsfp_data['ping_packet_counter'] = hex(read_data)

        read_data = self._client.udp_core_100g_ip_0.read(0x54)
        qsfp_data['arp_packet_counter'] = hex(read_data)

        read_data = self._client.udp_core_100g_ip_0.read(0x60)
        qsfp_data['dropped_mac_packet_counter'] = hex(read_data)

        read_data = self._client.udp_core_100g_ip_0.read(0x64)
        qsfp_data['dropped_ip_packet_counter'] = hex(read_data)

        read_data = self._client.udp_core_100g_ip_0.read(0x68)
        qsfp_data['port_packet_counter'] = hex(read_data)
        
        return json.dumps(qsfp_data)
