#create_pblock pblock_microblaze_0
#add_cells_to_pblock [get_pblocks pblock_microblaze_0] [get_cells -quiet [list base_i/mdm_0 base_i/microblaze_0 base_i/microblaze_0_axi_intc base_i/microblaze_0_axi_periph base_i/microblaze_0_local_memory]]
#resize_pblock [get_pblocks pblock_microblaze_0] -add {SLICE_X76Y180:SLICE_X118Y355}
#resize_pblock [get_pblocks pblock_microblaze_0] -add {DSP48E2_X13Y72:DSP48E2_X22Y141}
#resize_pblock [get_pblocks pblock_microblaze_0] -add {RAMB18_X8Y72:RAMB18_X11Y141}
#resize_pblock [get_pblocks pblock_microblaze_0] -add {RAMB36_X8Y36:RAMB36_X11Y70}
#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets clk]
