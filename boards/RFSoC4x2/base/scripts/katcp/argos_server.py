import time
import json 
import os 
import asyncio
import aiokatcp
import signal
from aiokatcp import Sensor, FailReply, RequestContext
import argos_client

class ArgosServer(aiokatcp.DeviceServer):
    """
    sample katcp wrapper
    """
    VERSION = 'argos-api-1.0'
    BUILD_STATE = 'argos-1.0.1.dev0'

    def __init__(self, ip, port):
        """init the katcp server with given ip, port"""
        self.sensors = {}
        self._client = argos_client.RFSoCInterface("argos_base.bit")
        super().__init__(ip, port)
        print("katcp server init done; setting up sensors")
        self.setupSensors()
        print("done")

    async def request_dpc_reset(self, ctx: RequestContext, value: int):
        """requests reset of the dpc"""
        await self._client.reset_dpc()

    async def request_start_1pps(self, ctx: RequestContext, value: int):
        """requests to start 1pps sync process"""
        await self._client.start_1pps()
      
    def setupSensors(self):

        self._fpga_clock = Sensor(float, "fpga-clock",
                                  description="FPGA Clock estimate",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fpga_clock)
      
        self._fpga_connected = Sensor(int, "fpga-connected",
                                  description="FPGA Programmed Status",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._fpga_connected)
      
        self._dpc_start_status = Sensor(int, "fpga-dpc-start",
                                  description="FPGA DPC Status",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._dpc_start_status)
      
        self._dpc_1pps_status = Sensor(int, "fpga-1pps-status",
                                  description="FPGA DPC 1PPS Status",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._dpc_1pps_status)
      
        self._dpc_qsfp28_registers = Sensor(str, "fpga-qsfp28-registers",
                                  description="FPGA QSFP28 Stats",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._dpc_qsfp28_registers)
        self._dpc_qsfp28_filters = Sensor(str, "fpga-qsfp28-filters",
                                  description="FPGA QSFP28 Filter Control",
                                  initial_status=Sensor.Status.UNKNOWN)
        self.sensors.add(self._dpc_qsfp28_filters)


    async def fpga_configure(self):
      """configures the board for base overlay"""


      #program the overlay
      await self._client.initialize()

      await self._client.configure()

      await self._client.configure_output(dest_ip="239.2.1.150",dest_port=7148)

      

    async def update_sensors(self):
      """ update katcp sensors"""

      while True:
        print("updating sensors...")
        ### update sensors
        self._fpga_clock.set_value(await self._client.get_fpga_clock())
        await asyncio.sleep(0.1)
        self._fpga_connected.set_value(await self._client.is_connected())
        await asyncio.sleep(0.1)
        self._dpc_start_status.set_value(await self._client.dpc_start_status())
        await asyncio.sleep(0.1)
        self._dpc_1pps_status.set_value(await self._client.dpc_1pps_status())
        await asyncio.sleep(0.1)
        self._dpc_qsfp28_registers.set_value(await self._client.read_qsfp28_registers())
        #await asyncio.sleep(0.1)
        #self._dpc_qsfp28_filters.set_value(await self._client.read_qsfp28_filters())
        print("done")
        await asyncio.sleep(5)


async def argos_server():
    
    server = ArgosServer('192.168.2.199', 7147)
    await server.start()
    print("katcp server running on port 7147")
    await server.fpga_configure()
    loop = asyncio.get_event_loop()
    task = loop.create_task(server.update_sensors())
    loop.add_signal_handler(signal.SIGINT, server.halt)
    print("CTRL C to terminate")
    await server.join()

if __name__ == "__main__":
    asyncio.run(argos_server())

