#ifndef RFSOC_BYP__H
#define RFSOC_BYP__H
#ifdef __cplusplus
extern "C" {
#endif
/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "rfsoc_byp_hw.h"
/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 rfsoc_byp_BaseAddress;
} rfsoc_byp_Config;
#endif
/**
* The rfsoc_byp driver instance data. The user is required to
* allocate a variable of this type for every rfsoc_byp device in the system.
* A pointer to a variable of this type is then passed to the driver
* API functions.
*/
typedef struct {
    u32 rfsoc_byp_BaseAddress;
    u32 IsReady;
} rfsoc_byp;
/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define rfsoc_byp_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define rfsoc_byp_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define rfsoc_byp_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define rfsoc_byp_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif
/************************** Function Prototypes *****************************/
#ifndef __linux__
int rfsoc_byp_Initialize(rfsoc_byp *InstancePtr, u16 DeviceId);
rfsoc_byp_Config* rfsoc_byp_LookupConfig(u16 DeviceId);
int rfsoc_byp_CfgInitialize(rfsoc_byp *InstancePtr, rfsoc_byp_Config *ConfigPtr);
#else
int rfsoc_byp_Initialize(rfsoc_byp *InstancePtr, const char* InstanceName);
int rfsoc_byp_Release(rfsoc_byp *InstancePtr);
#endif
/**
* Write to time_in_hi gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the time_in_hi instance to operate on.
* @param	Data is value to be written to gateway time_in_hi.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_time_in_hi_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from time_in_hi gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the time_in_hi instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_time_in_hi_read(rfsoc_byp *InstancePtr);
/**
* Write to x_setup gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the x_setup instance to operate on.
* @param	Data is value to be written to gateway x_setup.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_x_setup_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from x_setup gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the x_setup instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_x_setup_read(rfsoc_byp *InstancePtr);
/**
* Write to tx_pkt_wait gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the tx_pkt_wait instance to operate on.
* @param	Data is value to be written to gateway tx_pkt_wait.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_tx_pkt_wait_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from tx_pkt_wait gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the tx_pkt_wait instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_tx_pkt_wait_read(rfsoc_byp *InstancePtr);
/**
* Write to tx_pkt_bytes gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the tx_pkt_bytes instance to operate on.
* @param	Data is value to be written to gateway tx_pkt_bytes.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_tx_pkt_bytes_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from tx_pkt_bytes gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the tx_pkt_bytes instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_tx_pkt_bytes_read(rfsoc_byp *InstancePtr);
/**
* Write to tx_metadata gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the tx_metadata instance to operate on.
* @param	Data is value to be written to gateway tx_metadata.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_tx_metadata_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from tx_metadata gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the tx_metadata instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_tx_metadata_read(rfsoc_byp *InstancePtr);
/**
* Write to iptx_base gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the iptx_base instance to operate on.
* @param	Data is value to be written to gateway iptx_base.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_iptx_base_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from iptx_base gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the iptx_base instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_iptx_base_read(rfsoc_byp *InstancePtr);
/**
* Write to control gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the control instance to operate on.
* @param	Data is value to be written to gateway control.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_control_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from control gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the control instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_control_read(rfsoc_byp *InstancePtr);
/**
* Write to time_in_lo gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the time_in_lo instance to operate on.
* @param	Data is value to be written to gateway time_in_lo.
*
* @return	None.
*
* @note    .
*
*/
void rfsoc_byp_time_in_lo_write(rfsoc_byp *InstancePtr, u32 Data);
/**
* Read from time_in_lo gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the time_in_lo instance to operate on.
*
* @return	u32
*
* @note    .
*
*/
u32 rfsoc_byp_time_in_lo_read(rfsoc_byp *InstancePtr);
/**
* Read from intf_rst gateway of rfsoc_byp. Assignments are LSB-justified.
*
* @param	InstancePtr is the intf_rst instance to operate on.
*
* @return	u8
*
* @note    .
*
*/
u8 rfsoc_byp_intf_rst_read(rfsoc_byp *InstancePtr);
#ifdef __cplusplus
}
#endif
#endif
