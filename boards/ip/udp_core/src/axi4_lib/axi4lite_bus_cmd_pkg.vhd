-- <l---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------l>
-- <h---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------h>
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi4_lib;
use axi4_lib.axi4lite_pkg.all;

package axi4lite_bus_cmd_pkg is

constant C_WR_TIMEOUT_LIMIT         : integer := 100;
constant C_RD_TIMEOUT_LIMIT         : integer := 100;

--------------------------------------------------------------------------------
-- Function Declarations:
--------------------------------------------------------------------------------
function bitmask_op(
    constant original_data          : in    std_logic_vector(c_axi4lite_data_w-1 downto 0);
    constant mask                   : in    std_logic_vector(c_axi4lite_data_w-1 downto 0);
    constant operation              : in    string := "assert"
) return std_logic_vector;

--------------------------------------------------------------------------------
-- Procedure Declarations:
--------------------------------------------------------------------------------
procedure axi4lite_initialise(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_aresetn         : in    std_logic;
    signal axi4lite_mosi            : out   t_axi4lite_mosi
);


procedure axi4lite_rd32(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_miso            : in    t_axi4lite_miso;
    signal axi4lite_mosi            : out   t_axi4lite_mosi;
    constant addr                   : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    signal data                     : out   std_logic_vector(c_axi4lite_data_w-1 downto 0)
);


procedure axi4lite_read32(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_miso            : in    t_axi4lite_miso;
    signal axi4lite_mosi            : out   t_axi4lite_mosi;
    constant addr                   : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    variable data                   : out   std_logic_vector(c_axi4lite_data_w-1 downto 0)
);


procedure axi4lite_wr32(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_miso            : in    t_axi4lite_miso;
    signal axi4lite_mosi            : out   t_axi4lite_mosi;
    variable addr                   : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    constant data                   : in    std_logic_vector(c_axi4lite_data_w-1 downto 0)
);


procedure axi4lite_abs_addr_wr32(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_miso            : in    t_axi4lite_miso;
    signal axi4lite_mosi            : out   t_axi4lite_mosi;
    constant addr                   : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    constant data                   : in    std_logic_vector(c_axi4lite_data_w-1 downto 0)
);


end package axi4lite_bus_cmd_pkg;
--------------------------------------------------------------------------------
-- Package Body:
--------------------------------------------------------------------------------
package body axi4lite_bus_cmd_pkg is
--------------------------------------------------------------------------------
-- Functions:
--------------------------------------------------------------------------------
function bitmask_op(
    constant original_data          : in    std_logic_vector(c_axi4lite_data_w-1 downto 0);
    constant mask                   : in    std_logic_vector(c_axi4lite_data_w-1 downto 0);
    constant operation              : in    string := "assert"
) return std_logic_vector is

variable masked_data                : std_logic_vector(c_axi4lite_data_w-1 downto 0);

begin
    
    if operation = "assert" then
        masked_data := original_data or mask;
    elsif operation = "deassert" then
        masked_data := original_data and (not mask);
    elsif operation = "toggle" then
        masked_data := original_data xor mask;
    else
        assert false report "Unknown AXI4Lite Bitmask Operation. Valid Operations are: assert, deassert, toggle" severity error;
    end if;
    
    return masked_data;
    
end function;


--------------------------------------------------------------------------------
-- Procedures:
--------------------------------------------------------------------------------
procedure axi4lite_initialise(
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_aresetn             : in    std_logic;
    signal axi4lite_mosi                : out   t_axi4lite_mosi
) is

constant C_WAIT_CYCLES      : integer := 3;

begin
    axi4lite_mosi.arvalid <= '0';
    axi4lite_mosi.araddr <= (others=>'0');
    axi4lite_mosi.rready <= '0';
    axi4lite_mosi.awvalid <= '0';
    axi4lite_mosi.awaddr <= (others=> '0');
    axi4lite_mosi.wdata <= (others => '0');
    axi4lite_mosi.wvalid <= '0';
    axi4lite_mosi.bready <= '1';
    axi4lite_mosi.wstrb <= (others => '1');
    
    wait until axi4lite_aresetn = not('0') and rising_edge(axi4lite_aclk);
    for i in 0 to C_WAIT_CYCLES-1 loop
        wait until rising_edge(axi4lite_aclk);
    end loop;
    
    assert false report "AXI4Lite Bus Initialised" severity note;
end procedure axi4lite_initialise;


procedure axi4lite_rd32(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_miso            : in    t_axi4lite_miso;
    signal axi4lite_mosi            : out   t_axi4lite_mosi;
    constant addr                   : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    signal data                     : out   std_logic_vector(c_axi4lite_data_w-1 downto 0)
) is

variable timeout_counter            : integer := 0;

begin
    axi4lite_mosi.arvalid <= '1';
    axi4lite_mosi.araddr <= addr;
    rdy_wait_loop : loop
        wait until rising_edge(axi4lite_aclk);
        if(timeout_counter = C_RD_TIMEOUT_LIMIT) then
            -- Don't reset the counter as we want the vld_wait_loop to also exit immediately
            assert false report "AXI4Lite Read Operation (arready) Timeout" severity warning;
            exit rdy_wait_loop;
        elsif(axi4lite_miso.arready = '1') then
            axi4lite_mosi.arvalid <= '0';
            axi4lite_mosi.rready <= '1';
            timeout_counter := 0;
            exit rdy_wait_loop;
        else
            timeout_counter := timeout_counter + 1;
        end if;
    end loop rdy_wait_loop;
    
    vld_wait_loop : loop
        wait until rising_edge(axi4lite_aclk);
        if(timeout_counter = C_RD_TIMEOUT_LIMIT) then
            assert false report "AXI4Lite Read Operation (rvalid) Timeout" severity warning;
            exit vld_wait_loop;
        elsif(axi4lite_miso.rvalid  = '1') then
            axi4lite_mosi.rready <= '0';
            data <= axi4lite_miso.rdata;
            assert false report "AXI4Lite Read Operation Complete" severity note;
            exit vld_wait_loop;
        else
            timeout_counter := timeout_counter + 1;
        end if;
    end loop vld_wait_loop;
    
    wait until rising_edge(axi4lite_aclk);
    
end procedure axi4lite_rd32;

--! axi4lite_read32 uses a variable for data instead of a signal (required for Read/Modify/Write operations)
procedure axi4lite_read32(
    signal axi4lite_aclk            : in    std_logic;
    signal axi4lite_miso            : in    t_axi4lite_miso;
    signal axi4lite_mosi            : out   t_axi4lite_mosi;
    constant addr                   : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    variable data                   : out   std_logic_vector(c_axi4lite_data_w-1 downto 0)
) is

variable timeout_counter            : integer := 0;

begin
    axi4lite_mosi.arvalid <= '1';
    axi4lite_mosi.araddr <= addr;
    rdy_wait_loop : loop
        wait until rising_edge(axi4lite_aclk);
        if(timeout_counter = C_RD_TIMEOUT_LIMIT) then
            -- Don't reset the counter as we want the vld_wait_loop to also exit immediately
            assert false report "AXI4Lite Read Operation (arready) Timeout" severity warning;
            exit rdy_wait_loop;
        elsif(axi4lite_miso.arready = '1') then
            axi4lite_mosi.arvalid <= '0';
            axi4lite_mosi.rready <= '1';
            timeout_counter := 0;
            exit rdy_wait_loop;
        else
            timeout_counter := timeout_counter + 1;
        end if;
    end loop rdy_wait_loop;
    
    vld_wait_loop : loop
        wait until rising_edge(axi4lite_aclk);
        if(timeout_counter = C_RD_TIMEOUT_LIMIT) then
            assert false report "AXI4Lite Read Operation (rvalid) Timeout" severity warning;
            exit vld_wait_loop;
        elsif(axi4lite_miso.rvalid  = '1') then
            axi4lite_mosi.rready <= '0';
            data := axi4lite_miso.rdata;
            assert false report "AXI4Lite Read Operation Complete" severity note;
            exit vld_wait_loop;
        else
            timeout_counter := timeout_counter + 1;
        end if;
    end loop vld_wait_loop;
    
    wait until rising_edge(axi4lite_aclk);
    
end procedure axi4lite_read32;


procedure axi4lite_wr32(
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    variable addr                       : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    constant data                       : in    std_logic_vector(c_axi4lite_data_w-1 downto 0)
) is

variable done                           : std_logic_vector(2 downto 0):="000";
variable timeout_counter                : integer := 0;

begin
    axi4lite_mosi.awvalid <= '1';
    axi4lite_mosi.awaddr <= addr;
    axi4lite_mosi.wdata <= data;
    axi4lite_mosi.wvalid <= '1';
    axi4lite_mosi.wstrb <= (others => '1');
    wait until rising_edge(axi4lite_aclk);
    wait_loop : loop
        if(timeout_counter = C_WR_TIMEOUT_LIMIT) then
            assert false report "AXI4Lite Write Operation Timeout" severity warning;
            exit wait_loop;
        else
            if(axi4lite_miso.awready = '1') then
                done(0) := '1';
                axi4lite_mosi.awvalid <= '0';
            end if;
            if(axi4lite_miso.wready = '1') then
                done(1) := '1';
                axi4lite_mosi.wvalid <= '0';
            end if; 
            if(axi4lite_miso.bvalid = '1') then
                done(2) := '1';
            end if; 
            wait until rising_edge(axi4lite_aclk);
            if done = "111" then
                assert false report "AXI4Lite Write Operation Complete" severity note;
                exit wait_loop;
            else
                timeout_counter := timeout_counter + 1;
            end if;
        end if;
    end loop wait_loop;
end procedure axi4lite_wr32;

--! axi4lite_abs_addr_wr32 Allows addr to be populated with X"xxxx_xxxx" Literal String (constant)
procedure axi4lite_abs_addr_wr32(
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    constant addr                       : in    std_logic_vector(c_axi4lite_addr_w-1 downto 0);
    constant data                       : in    std_logic_vector(c_axi4lite_data_w-1 downto 0)
) is

variable done                           : std_logic_vector(2 downto 0):="000";
variable timeout_counter                : integer := 0;

begin
    axi4lite_mosi.awvalid <= '1';
    axi4lite_mosi.awaddr <= addr;
    axi4lite_mosi.wdata <= data;
    axi4lite_mosi.wvalid <= '1';
    axi4lite_mosi.wstrb <= (others => '1');
    wait until rising_edge(axi4lite_aclk);
    wait_loop : loop
        if(timeout_counter = C_WR_TIMEOUT_LIMIT) then
            assert false report "AXI4Lite Write Operation Timeout" severity warning;
            exit wait_loop;
        else
            if(axi4lite_miso.awready = '1') then
                done(0) := '1';
                axi4lite_mosi.awvalid <= '0';
            end if;
            if(axi4lite_miso.wready = '1') then
                done(1) := '1';
                axi4lite_mosi.wvalid <= '0';
            end if; 
            if(axi4lite_miso.bvalid = '1') then
                done(2) := '1';
            end if; 
            wait until rising_edge(axi4lite_aclk);
            if done = "111" then
                assert false report "AXI4Lite Write Operation Complete" severity note;
                exit wait_loop;
            else
                timeout_counter := timeout_counter + 1;
            end if;
        end if;
    end loop wait_loop;
end procedure axi4lite_abs_addr_wr32;

end package body axi4lite_bus_cmd_pkg;
