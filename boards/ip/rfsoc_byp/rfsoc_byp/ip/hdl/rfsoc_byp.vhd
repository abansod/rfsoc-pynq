-- Generated from Simulink block rfsoc_byp/MB/S0
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_s0 is
  port (
    we0 : in std_logic_vector( 1-1 downto 0 );
    p0s0p1s0 : in std_logic_vector( 512-1 downto 0 );
    r_addr : in std_logic_vector( 8-1 downto 0 );
    w_addr : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout1 : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_s0;
architecture structural of rfsoc_byp_s0 is 
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_ce_net : std_logic;
  signal constant1_op_net : std_logic_vector( 512-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 1-1 downto 0 );
  signal db0_0_douta_net : std_logic_vector( 512-1 downto 0 );
  signal w_addr_x0 : std_logic_vector( 8-1 downto 0 );
  signal d0 : std_logic_vector( 512-1 downto 0 );
  signal we : std_logic_vector( 1-1 downto 0 );
  signal r_addr_x0 : std_logic_vector( 8-1 downto 0 );
begin
  dout1 <= d1_out;
  register1_q_net <= we0;
  register1_q_net_x1 <= p0s0p1s0;
  register0_q_net <= r_addr;
  register1_q_net_x0 <= w_addr;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  constant1 : entity xil_defaultlib.sysgen_constant_5c925725ec 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_41a2f15474 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  db0_0 : entity xil_defaultlib.rfsoc_byp_xltdpram 
  generic map (
    addr_width_b => 8,
    clocking_mode => "common_clock",
    data_width_b => 512,
    latency => 2,
    mem_init_file => "xpm_eb0cf1_vivado.mem",
    mem_size => 131072,
    mem_type => "block",
    read_reset_a => "0",
    read_reset_b => "0",
    width => 512,
    width_addr => 8,
    write_mode_a => "read_first",
    write_mode_b => "read_first"
  )
  port map (
    ena => "1",
    enb => "1",
    rsta => "0",
    rstb => "0",
    addra => w_addr_x0,
    dina => d0,
    wea => we,
    addrb => r_addr_x0,
    dinb => constant1_op_net,
    web => constant3_op_net,
    a_clk => a_clk_net,
    a_ce => a_ce_net,
    b_clk => a_clk_net,
    b_ce => a_ce_net,
    douta => db0_0_douta_net,
    doutb => d1_out
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => w_addr_x0
  );
  register11 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => r_addr_x0
  );
  register24 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => we
  );
  register25 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => d0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/S1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_s1 is
  port (
    we0 : in std_logic_vector( 1-1 downto 0 );
    p0s0p1s0 : in std_logic_vector( 512-1 downto 0 );
    r_addr : in std_logic_vector( 8-1 downto 0 );
    w_addr : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout1 : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_s1;
architecture structural of rfsoc_byp_s1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal constant1_op_net : std_logic_vector( 512-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 1-1 downto 0 );
  signal db0_0_douta_net : std_logic_vector( 512-1 downto 0 );
  signal w_addr_x0 : std_logic_vector( 8-1 downto 0 );
  signal d0 : std_logic_vector( 512-1 downto 0 );
  signal we : std_logic_vector( 1-1 downto 0 );
  signal r_addr_x0 : std_logic_vector( 8-1 downto 0 );
begin
  dout1 <= d1_out;
  register1_q_net <= we0;
  register1_q_net_x1 <= p0s0p1s0;
  register0_q_net <= r_addr;
  register1_q_net_x0 <= w_addr;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  constant1 : entity xil_defaultlib.sysgen_constant_5c925725ec 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_41a2f15474 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  db0_0 : entity xil_defaultlib.rfsoc_byp_xltdpram 
  generic map (
    addr_width_b => 8,
    clocking_mode => "common_clock",
    data_width_b => 512,
    latency => 2,
    mem_init_file => "xpm_eb0cf1_vivado.mem",
    mem_size => 131072,
    mem_type => "block",
    read_reset_a => "0",
    read_reset_b => "0",
    width => 512,
    width_addr => 8,
    write_mode_a => "read_first",
    write_mode_b => "read_first"
  )
  port map (
    ena => "1",
    enb => "1",
    rsta => "0",
    rstb => "0",
    addra => w_addr_x0,
    dina => d0,
    wea => we,
    addrb => r_addr_x0,
    dinb => constant1_op_net,
    web => constant3_op_net,
    a_clk => a_clk_net,
    a_ce => a_ce_net,
    b_clk => a_clk_net,
    b_ce => a_ce_net,
    douta => db0_0_douta_net,
    doutb => d1_out
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => w_addr_x0
  );
  register11 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => r_addr_x0
  );
  register24 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => we
  );
  register25 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => d0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/S2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_s2 is
  port (
    we0 : in std_logic_vector( 1-1 downto 0 );
    p0s0p1s0 : in std_logic_vector( 512-1 downto 0 );
    r_addr : in std_logic_vector( 8-1 downto 0 );
    w_addr : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout1 : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_s2;
architecture structural of rfsoc_byp_s2 is 
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 512-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 1-1 downto 0 );
  signal db0_0_douta_net : std_logic_vector( 512-1 downto 0 );
  signal w_addr_x0 : std_logic_vector( 8-1 downto 0 );
  signal d0 : std_logic_vector( 512-1 downto 0 );
  signal we : std_logic_vector( 1-1 downto 0 );
  signal r_addr_x0 : std_logic_vector( 8-1 downto 0 );
begin
  dout1 <= d1_out;
  register1_q_net <= we0;
  register1_q_net_x1 <= p0s0p1s0;
  register0_q_net <= r_addr;
  register1_q_net_x0 <= w_addr;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  constant1 : entity xil_defaultlib.sysgen_constant_5c925725ec 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_41a2f15474 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  db0_0 : entity xil_defaultlib.rfsoc_byp_xltdpram 
  generic map (
    addr_width_b => 8,
    clocking_mode => "common_clock",
    data_width_b => 512,
    latency => 2,
    mem_init_file => "xpm_eb0cf1_vivado.mem",
    mem_size => 131072,
    mem_type => "block",
    read_reset_a => "0",
    read_reset_b => "0",
    width => 512,
    width_addr => 8,
    write_mode_a => "read_first",
    write_mode_b => "read_first"
  )
  port map (
    ena => "1",
    enb => "1",
    rsta => "0",
    rstb => "0",
    addra => w_addr_x0,
    dina => d0,
    wea => we,
    addrb => r_addr_x0,
    dinb => constant1_op_net,
    web => constant3_op_net,
    a_clk => a_clk_net,
    a_ce => a_ce_net,
    b_clk => a_clk_net,
    b_ce => a_ce_net,
    douta => db0_0_douta_net,
    doutb => d1_out
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => w_addr_x0
  );
  register11 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => r_addr_x0
  );
  register24 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => we
  );
  register25 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => d0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/S3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_s3 is
  port (
    we0 : in std_logic_vector( 1-1 downto 0 );
    p0s0p1s0 : in std_logic_vector( 512-1 downto 0 );
    r_addr : in std_logic_vector( 8-1 downto 0 );
    w_addr : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    dout1 : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_s3;
architecture structural of rfsoc_byp_s3 is 
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal constant1_op_net : std_logic_vector( 512-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 1-1 downto 0 );
  signal db0_0_douta_net : std_logic_vector( 512-1 downto 0 );
  signal w_addr_x0 : std_logic_vector( 8-1 downto 0 );
  signal d0 : std_logic_vector( 512-1 downto 0 );
  signal we : std_logic_vector( 1-1 downto 0 );
  signal r_addr_x0 : std_logic_vector( 8-1 downto 0 );
begin
  dout1 <= d1_out;
  register1_q_net <= we0;
  register1_q_net_x1 <= p0s0p1s0;
  register0_q_net <= r_addr;
  register1_q_net_x0 <= w_addr;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  constant1 : entity xil_defaultlib.sysgen_constant_5c925725ec 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_41a2f15474 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  db0_0 : entity xil_defaultlib.rfsoc_byp_xltdpram 
  generic map (
    addr_width_b => 8,
    clocking_mode => "common_clock",
    data_width_b => 512,
    latency => 2,
    mem_init_file => "xpm_eb0cf1_vivado.mem",
    mem_size => 131072,
    mem_type => "block",
    read_reset_a => "0",
    read_reset_b => "0",
    width => 512,
    width_addr => 8,
    write_mode_a => "read_first",
    write_mode_b => "read_first"
  )
  port map (
    ena => "1",
    enb => "1",
    rsta => "0",
    rstb => "0",
    addra => w_addr_x0,
    dina => d0,
    wea => we,
    addrb => r_addr_x0,
    dinb => constant1_op_net,
    web => constant3_op_net,
    a_clk => a_clk_net,
    a_ce => a_ce_net,
    b_clk => a_clk_net,
    b_ce => a_ce_net,
    douta => db0_0_douta_net,
    doutb => d1_out
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => w_addr_x0
  );
  register11 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => r_addr_x0
  );
  register24 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => we
  );
  register25 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => d0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/delay_srl1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl1_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl1_x0;
architecture structural of rfsoc_byp_delay_srl1_x0 is 
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
begin
  out_x0 <= delay_sr_q_net_x0;
  delay_sr_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/delay_srl2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl2_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl2_x0;
architecture structural of rfsoc_byp_delay_srl2_x0 is 
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net_x0;
  delay_sr_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_502d31f8d1 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/delay_srl3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl3_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl3_x0;
architecture structural of rfsoc_byp_delay_srl3_x0 is 
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net_x0;
  delay_sr_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_502d31f8d1 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/delay_srl4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl4_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl4_x0;
architecture structural of rfsoc_byp_delay_srl4_x0 is 
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net_x0;
  delay_sr_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_502d31f8d1 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/delay_srl64
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl64 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl64;
architecture structural of rfsoc_byp_delay_srl64 is 
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net_x0;
  delay_sr_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_502d31f8d1 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/edge_detect1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect1_x1 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect1_x1;
architecture structural of rfsoc_byp_edge_detect1_x1 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  register0_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_ac59be6314 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge1/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x11 is
  port (
    in1 : in std_logic_vector( 128-1 downto 0 );
    in2 : in std_logic_vector( 128-1 downto 0 );
    in3 : in std_logic_vector( 128-1 downto 0 );
    in4 : in std_logic_vector( 128-1 downto 0 );
    bus_out : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_join_x11;
architecture structural of rfsoc_byp_join_x11 is 
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net <= in1;
  reinterpret2_output_port_net <= in2;
  reinterpret3_output_port_net <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  concatenate : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net_x0,
    in1 => reinterpret2_output_port_net_x0,
    in2 => reinterpret3_output_port_net_x0,
    in3 => reinterpret4_output_port_net,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net,
    output_port => reinterpret1_output_port_net_x0
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net,
    output_port => reinterpret2_output_port_net_x0
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net,
    output_port => reinterpret3_output_port_net_x0
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge1/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x10 is
  port (
    bus_in : in std_logic_vector( 512-1 downto 0 );
    msb_out4 : out std_logic_vector( 128-1 downto 0 );
    out3 : out std_logic_vector( 128-1 downto 0 );
    out2 : out std_logic_vector( 128-1 downto 0 );
    lsb_out1 : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_split_x10;
architecture structural of rfsoc_byp_split_x10 is 
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 128-1 downto 0 );
begin
  msb_out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 384,
    new_msb => 511,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 127,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 256,
    new_msb => 383,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 128,
    new_msb => 255,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge1_x0 is
  port (
    din : in std_logic_vector( 512-1 downto 0 );
    dout : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_munge1_x0;
architecture structural of rfsoc_byp_munge1_x0 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  register1_q_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x11 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x10 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => register1_q_net,
    output_port => reinterpret_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge2/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x10 is
  port (
    in1 : in std_logic_vector( 128-1 downto 0 );
    in2 : in std_logic_vector( 128-1 downto 0 );
    in3 : in std_logic_vector( 128-1 downto 0 );
    in4 : in std_logic_vector( 128-1 downto 0 );
    bus_out : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_join_x10;
architecture structural of rfsoc_byp_join_x10 is 
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net <= in1;
  reinterpret2_output_port_net <= in2;
  reinterpret3_output_port_net <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  concatenate : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net_x0,
    in1 => reinterpret2_output_port_net_x0,
    in2 => reinterpret3_output_port_net_x0,
    in3 => reinterpret4_output_port_net,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net,
    output_port => reinterpret1_output_port_net_x0
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net,
    output_port => reinterpret2_output_port_net_x0
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net,
    output_port => reinterpret3_output_port_net_x0
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge2/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x11 is
  port (
    bus_in : in std_logic_vector( 512-1 downto 0 );
    msb_out4 : out std_logic_vector( 128-1 downto 0 );
    out3 : out std_logic_vector( 128-1 downto 0 );
    out2 : out std_logic_vector( 128-1 downto 0 );
    lsb_out1 : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_split_x11;
architecture structural of rfsoc_byp_split_x11 is 
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 128-1 downto 0 );
begin
  msb_out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 127,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 128,
    new_msb => 255,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 256,
    new_msb => 383,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 384,
    new_msb => 511,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge2_x0 is
  port (
    din : in std_logic_vector( 512-1 downto 0 );
    dout : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_munge2_x0;
architecture structural of rfsoc_byp_munge2_x0 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  register1_q_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x10 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x11 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => register1_q_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge3/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x0 is
  port (
    in1 : in std_logic_vector( 128-1 downto 0 );
    in2 : in std_logic_vector( 128-1 downto 0 );
    in3 : in std_logic_vector( 128-1 downto 0 );
    in4 : in std_logic_vector( 128-1 downto 0 );
    bus_out : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_join_x0;
architecture structural of rfsoc_byp_join_x0 is 
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net <= in1;
  reinterpret2_output_port_net <= in2;
  reinterpret3_output_port_net <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  concatenate : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net_x0,
    in1 => reinterpret2_output_port_net_x0,
    in2 => reinterpret3_output_port_net_x0,
    in3 => reinterpret4_output_port_net,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net,
    output_port => reinterpret1_output_port_net_x0
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net,
    output_port => reinterpret2_output_port_net_x0
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net,
    output_port => reinterpret3_output_port_net_x0
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge3/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x0 is
  port (
    bus_in : in std_logic_vector( 512-1 downto 0 );
    msb_out4 : out std_logic_vector( 128-1 downto 0 );
    out3 : out std_logic_vector( 128-1 downto 0 );
    out2 : out std_logic_vector( 128-1 downto 0 );
    lsb_out1 : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_split_x0;
architecture structural of rfsoc_byp_split_x0 is 
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 128-1 downto 0 );
begin
  msb_out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 127,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 128,
    new_msb => 255,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 256,
    new_msb => 383,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 384,
    new_msb => 511,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge3 is
  port (
    din : in std_logic_vector( 512-1 downto 0 );
    dout : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_munge3;
architecture structural of rfsoc_byp_munge3 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  register1_q_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x0 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x0 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => register1_q_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge4/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x1 is
  port (
    in1 : in std_logic_vector( 128-1 downto 0 );
    in2 : in std_logic_vector( 128-1 downto 0 );
    in3 : in std_logic_vector( 128-1 downto 0 );
    in4 : in std_logic_vector( 128-1 downto 0 );
    bus_out : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_join_x1;
architecture structural of rfsoc_byp_join_x1 is 
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net <= in1;
  reinterpret2_output_port_net <= in2;
  reinterpret3_output_port_net <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  concatenate : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net_x0,
    in1 => reinterpret2_output_port_net_x0,
    in2 => reinterpret3_output_port_net_x0,
    in3 => reinterpret4_output_port_net,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net,
    output_port => reinterpret1_output_port_net_x0
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net,
    output_port => reinterpret2_output_port_net_x0
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net,
    output_port => reinterpret3_output_port_net_x0
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge4/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x1 is
  port (
    bus_in : in std_logic_vector( 512-1 downto 0 );
    msb_out4 : out std_logic_vector( 128-1 downto 0 );
    out3 : out std_logic_vector( 128-1 downto 0 );
    out2 : out std_logic_vector( 128-1 downto 0 );
    lsb_out1 : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_split_x1;
architecture structural of rfsoc_byp_split_x1 is 
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 128-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 128-1 downto 0 );
begin
  msb_out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_e6c26adb08 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 127,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 128,
    new_msb => 255,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 256,
    new_msb => 383,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 384,
    new_msb => 511,
    x_width => 512,
    y_width => 128
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/munge4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge4 is
  port (
    din : in std_logic_vector( 512-1 downto 0 );
    dout : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_munge4;
architecture structural of rfsoc_byp_munge4 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 128-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  register1_q_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x1 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x1 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => register1_q_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline0
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline0;
architecture structural of rfsoc_byp_pipeline0 is 
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal bram_delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
begin
  q <= register0_q_net;
  bram_delay_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => bram_delay_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline1_x1 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline1_x1;
architecture structural of rfsoc_byp_pipeline1_x1 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline10
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline10_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline10_x0;
architecture structural of rfsoc_byp_pipeline10_x0 is 
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register2_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline11
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline11_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline11_x0;
architecture structural of rfsoc_byp_pipeline11_x0 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline12
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline12_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline12_x0;
architecture structural of rfsoc_byp_pipeline12_x0 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  register1_q_net_x0 <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline13
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline13 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline13;
architecture structural of rfsoc_byp_pipeline13 is 
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register2_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline14
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline14_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline14_x0;
architecture structural of rfsoc_byp_pipeline14_x0 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline15
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline15_x1 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline15_x1;
architecture structural of rfsoc_byp_pipeline15_x1 is 
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
begin
  q <= register2_q_net;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline158
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline158 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline158;
architecture structural of rfsoc_byp_pipeline158 is 
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal concat_y_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register2_q_net;
  concat_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => concat_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline16
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline16_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline16_x0;
architecture structural of rfsoc_byp_pipeline16_x0 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register2_q_net;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline17
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline17_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline17_x0;
architecture structural of rfsoc_byp_pipeline17_x0 is 
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
begin
  q <= register2_q_net;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline174
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline174_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline174_x0;
architecture structural of rfsoc_byp_pipeline174_x0 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  d1_out <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => d1_out,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline18
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline18 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline18;
architecture structural of rfsoc_byp_pipeline18 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical2_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline19
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline19_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline19_x0;
architecture structural of rfsoc_byp_pipeline19_x0 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline2_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline2_x0;
architecture structural of rfsoc_byp_pipeline2_x0 is 
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register2_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline20
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline20_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline20_x0;
architecture structural of rfsoc_byp_pipeline20_x0 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline21
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline21 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline21;
architecture structural of rfsoc_byp_pipeline21 is 
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline22
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline22 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline22;
architecture structural of rfsoc_byp_pipeline22 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  d1_out <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => d1_out,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline23
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline23 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline23;
architecture structural of rfsoc_byp_pipeline23 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline24
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline24 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline24;
architecture structural of rfsoc_byp_pipeline24 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline25
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline25 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline25;
architecture structural of rfsoc_byp_pipeline25 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical3_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical3_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline26
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline26 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline26;
architecture structural of rfsoc_byp_pipeline26 is 
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline27
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline27 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline27;
architecture structural of rfsoc_byp_pipeline27 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  d1_out <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => d1_out,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline28
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline28 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline28;
architecture structural of rfsoc_byp_pipeline28 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline29
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline29_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline29_x0;
architecture structural of rfsoc_byp_pipeline29_x0 is 
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
begin
  q <= register0_q_net_x0;
  register0_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline3_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline3_x0;
architecture structural of rfsoc_byp_pipeline3_x0 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline30
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline30 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline30;
architecture structural of rfsoc_byp_pipeline30 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline31
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline31 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline31;
architecture structural of rfsoc_byp_pipeline31 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical4_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical4_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical4_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline32
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline32 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline32;
architecture structural of rfsoc_byp_pipeline32 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  register1_q_net_x0 <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline33
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline33 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline33;
architecture structural of rfsoc_byp_pipeline33 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  d1_out <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => d1_out,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline34
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline34 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline34;
architecture structural of rfsoc_byp_pipeline34 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline35
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline35 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline35;
architecture structural of rfsoc_byp_pipeline35 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline37
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline37 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline37;
architecture structural of rfsoc_byp_pipeline37 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline38
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline38_x0 is
  port (
    d : in std_logic_vector( 2-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 2-1 downto 0 )
  );
end rfsoc_byp_pipeline38_x0;
architecture structural of rfsoc_byp_pipeline38_x0 is 
  signal register1_q_net : std_logic_vector( 2-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 2-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 2-1 downto 0 );
begin
  q <= register1_q_net;
  slice1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 2,
    init_value => b"00"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 2,
    init_value => b"00"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline39
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline39_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline39_x0;
architecture structural of rfsoc_byp_pipeline39_x0 is 
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
begin
  q <= register2_q_net;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline4_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline4_x0;
architecture structural of rfsoc_byp_pipeline4_x0 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline5
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline5_x1 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline5_x1;
architecture structural of rfsoc_byp_pipeline5_x1 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline6
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline6 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline6;
architecture structural of rfsoc_byp_pipeline6 is 
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register2_q_net;
  delay_sr_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline7
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline7 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline7;
architecture structural of rfsoc_byp_pipeline7 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline8
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline8_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline8_x0;
architecture structural of rfsoc_byp_pipeline8_x0 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline81
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline81 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline81;
architecture structural of rfsoc_byp_pipeline81 is 
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register2_q_net;
  register0_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net_x0
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB/pipeline9
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline9_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline9_x0;
architecture structural of rfsoc_byp_pipeline9_x0 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  register2_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_mb is
  port (
    w_addr : in std_logic_vector( 8-1 downto 0 );
    r_addr : in std_logic_vector( 8-1 downto 0 );
    we : in std_logic_vector( 1-1 downto 0 );
    p0sx_p1sx0 : in std_logic_vector( 512-1 downto 0 );
    d_run : in std_logic_vector( 1-1 downto 0 );
    valid_read : in std_logic_vector( 1-1 downto 0 );
    w_addr1 : in std_logic_vector( 8-1 downto 0 );
    we1 : in std_logic_vector( 1-1 downto 0 );
    w_addr2 : in std_logic_vector( 8-1 downto 0 );
    we2 : in std_logic_vector( 1-1 downto 0 );
    w_addr3 : in std_logic_vector( 8-1 downto 0 );
    we3 : in std_logic_vector( 1-1 downto 0 );
    p0sx_p1sx1 : in std_logic_vector( 512-1 downto 0 );
    p0sx_p1sx2 : in std_logic_vector( 512-1 downto 0 );
    p0sx_p1sx3 : in std_logic_vector( 512-1 downto 0 );
    s_rd_eof : in std_logic_vector( 1-1 downto 0 );
    d_reset : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    sync_out : out std_logic_vector( 1-1 downto 0 );
    s0_pci0 : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_mb;
architecture structural of rfsoc_byp_mb is 
  signal register0_q_net_x3 : std_logic_vector( 1-1 downto 0 );
  signal mux0_y_net : std_logic_vector( 512-1 downto 0 );
  signal delay_sr_q_net_x6 : std_logic_vector( 8-1 downto 0 );
  signal register0_q_net_x2 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x5 : std_logic_vector( 1-1 downto 0 );
  signal concat_y_net : std_logic_vector( 512-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x3 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x1 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x8 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x7 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x15 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x17 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x16 : std_logic_vector( 512-1 downto 0 );
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x18 : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal d1_out_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x31 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x26 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x30 : std_logic_vector( 8-1 downto 0 );
  signal d1_out : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x27 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x22 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x24 : std_logic_vector( 8-1 downto 0 );
  signal d1_out_x2 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x19 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x12 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x14 : std_logic_vector( 8-1 downto 0 );
  signal d1_out_x1 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x9 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x8 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x14 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x12 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x9 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x11 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x10 : std_logic_vector( 1-1 downto 0 );
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal reinterpret_out_output_port_net_x2 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x28 : std_logic_vector( 512-1 downto 0 );
  signal reinterpret_out_output_port_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x23 : std_logic_vector( 512-1 downto 0 );
  signal reinterpret_out_output_port_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x13 : std_logic_vector( 512-1 downto 0 );
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x7 : std_logic_vector( 512-1 downto 0 );
  signal bram_delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x32 : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net_x3 : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net_x8 : std_logic_vector( 8-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x7 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x29 : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net_x6 : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net_x5 : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net_x4 : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net_x2 : std_logic_vector( 512-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x11 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x25 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x3 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x2 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x21 : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x10 : std_logic_vector( 1-1 downto 0 );
  signal logical4_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x20 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x6 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x5 : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x4 : std_logic_vector( 2-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 2-1 downto 0 );
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal bram_delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal bram_delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal bram_delay2_q_net : std_logic_vector( 1-1 downto 0 );
  signal bram_delay3_q_net : std_logic_vector( 1-1 downto 0 );
  signal x4_bit_cntr_op_net : std_logic_vector( 32-1 downto 0 );
  signal register18_q_net : std_logic_vector( 1-1 downto 0 );
begin
  sync_out <= register0_q_net_x3;
  s0_pci0 <= mux0_y_net;
  delay_sr_q_net_x6 <= w_addr;
  register0_q_net_x2 <= r_addr;
  delay_sr_q_net_x5 <= we;
  concat_y_net <= p0sx_p1sx0;
  delay_sr_q_net <= d_run;
  register0_q_net_x1 <= valid_read;
  delay_sr_q_net_x3 <= w_addr1;
  delay_sr_q_net_x2 <= we1;
  delay_sr_q_net_x1 <= w_addr2;
  delay_sr_q_net_x0 <= we2;
  delay_sr_q_net_x8 <= w_addr3;
  delay_sr_q_net_x7 <= we3;
  register1_q_net_x15 <= p0sx_p1sx1;
  register1_q_net_x17 <= p0sx_p1sx2;
  register1_q_net_x16 <= p0sx_p1sx3;
  register0_q_net_x0 <= s_rd_eof;
  register1_q_net_x18 <= d_reset;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  s0 : entity xil_defaultlib.rfsoc_byp_s0 
  port map (
    we0 => register1_q_net_x31,
    p0s0p1s0 => register1_q_net_x26,
    r_addr => register0_q_net_x2,
    w_addr => register1_q_net_x30,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    dout1 => d1_out_x0
  );
  s1 : entity xil_defaultlib.rfsoc_byp_s1 
  port map (
    we0 => register1_q_net_x27,
    p0s0p1s0 => register1_q_net_x22,
    r_addr => register0_q_net_x2,
    w_addr => register1_q_net_x24,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    dout1 => d1_out
  );
  s2 : entity xil_defaultlib.rfsoc_byp_s2 
  port map (
    we0 => register1_q_net_x19,
    p0s0p1s0 => register1_q_net_x12,
    r_addr => register0_q_net_x2,
    w_addr => register1_q_net_x14,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    dout1 => d1_out_x2
  );
  s3 : entity xil_defaultlib.rfsoc_byp_s3 
  port map (
    we0 => register1_q_net_x9,
    p0s0p1s0 => register1_q_net,
    r_addr => register0_q_net_x2,
    w_addr => register1_q_net_x8,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    dout1 => d1_out_x1
  );
  delay_srl1 : entity xil_defaultlib.rfsoc_byp_delay_srl1_x0 
  port map (
    in_x0 => delay_sr_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x14
  );
  delay_srl2 : entity xil_defaultlib.rfsoc_byp_delay_srl2_x0 
  port map (
    in_x0 => delay_sr_q_net_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x12
  );
  delay_srl3 : entity xil_defaultlib.rfsoc_byp_delay_srl3_x0 
  port map (
    in_x0 => delay_sr_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x9
  );
  delay_srl4 : entity xil_defaultlib.rfsoc_byp_delay_srl4_x0 
  port map (
    in_x0 => delay_sr_q_net_x7,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x11
  );
  delay_srl64 : entity xil_defaultlib.rfsoc_byp_delay_srl64 
  port map (
    in_x0 => delay_sr_q_net_x5,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x10
  );
  edge_detect1_x0 : entity xil_defaultlib.rfsoc_byp_edge_detect1_x1 
  port map (
    in_x0 => register0_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net
  );
  munge1 : entity xil_defaultlib.rfsoc_byp_munge1_x0 
  port map (
    din => register1_q_net_x28,
    dout => reinterpret_out_output_port_net_x2
  );
  munge2 : entity xil_defaultlib.rfsoc_byp_munge2_x0 
  port map (
    din => register1_q_net_x23,
    dout => reinterpret_out_output_port_net_x1
  );
  munge3 : entity xil_defaultlib.rfsoc_byp_munge3 
  port map (
    din => register1_q_net_x13,
    dout => reinterpret_out_output_port_net_x0
  );
  munge4 : entity xil_defaultlib.rfsoc_byp_munge4 
  port map (
    din => register1_q_net_x7,
    dout => reinterpret_out_output_port_net
  );
  pipeline0 : entity xil_defaultlib.rfsoc_byp_pipeline0 
  port map (
    d => bram_delay_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register0_q_net_x3
  );
  pipeline1_x0 : entity xil_defaultlib.rfsoc_byp_pipeline1_x1 
  port map (
    d => register2_q_net_x3,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x32
  );
  pipeline10 : entity xil_defaultlib.rfsoc_byp_pipeline10_x0 
  port map (
    d => delay_sr_q_net_x1,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x8
  );
  pipeline11 : entity xil_defaultlib.rfsoc_byp_pipeline11_x0 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x31
  );
  pipeline12 : entity xil_defaultlib.rfsoc_byp_pipeline12_x0 
  port map (
    d => register1_q_net_x32,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x30
  );
  pipeline13 : entity xil_defaultlib.rfsoc_byp_pipeline13 
  port map (
    d => delay_sr_q_net_x8,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x7
  );
  pipeline14 : entity xil_defaultlib.rfsoc_byp_pipeline14_x0 
  port map (
    d => register2_q_net_x7,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x29
  );
  pipeline15_x0 : entity xil_defaultlib.rfsoc_byp_pipeline15_x1 
  port map (
    d => register1_q_net_x15,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x6
  );
  pipeline158 : entity xil_defaultlib.rfsoc_byp_pipeline158 
  port map (
    d => concat_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x5
  );
  pipeline16_x0 : entity xil_defaultlib.rfsoc_byp_pipeline16_x0 
  port map (
    d => register1_q_net_x17,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x4
  );
  pipeline17 : entity xil_defaultlib.rfsoc_byp_pipeline17_x0 
  port map (
    d => register1_q_net_x16,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x2
  );
  pipeline174 : entity xil_defaultlib.rfsoc_byp_pipeline174_x0 
  port map (
    d => d1_out_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x28
  );
  pipeline18 : entity xil_defaultlib.rfsoc_byp_pipeline18 
  port map (
    d => logical2_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x27
  );
  pipeline19 : entity xil_defaultlib.rfsoc_byp_pipeline19_x0 
  port map (
    d => register1_q_net_x11,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x26
  );
  pipeline2_x0 : entity xil_defaultlib.rfsoc_byp_pipeline2_x0 
  port map (
    d => delay_sr_q_net_x6,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x3
  );
  pipeline20 : entity xil_defaultlib.rfsoc_byp_pipeline20_x0 
  port map (
    d => delay_sr_q_net_x14,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x25
  );
  pipeline21 : entity xil_defaultlib.rfsoc_byp_pipeline21 
  port map (
    d => register1_q_net_x3,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x24
  );
  pipeline22 : entity xil_defaultlib.rfsoc_byp_pipeline22 
  port map (
    d => d1_out,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x23
  );
  pipeline23 : entity xil_defaultlib.rfsoc_byp_pipeline23 
  port map (
    d => register1_q_net_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x22
  );
  pipeline24 : entity xil_defaultlib.rfsoc_byp_pipeline24 
  port map (
    d => delay_sr_q_net_x14,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x21
  );
  pipeline25 : entity xil_defaultlib.rfsoc_byp_pipeline25 
  port map (
    d => logical3_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x19
  );
  pipeline26 : entity xil_defaultlib.rfsoc_byp_pipeline26 
  port map (
    d => register1_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x14
  );
  pipeline27 : entity xil_defaultlib.rfsoc_byp_pipeline27 
  port map (
    d => d1_out_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x13
  );
  pipeline28 : entity xil_defaultlib.rfsoc_byp_pipeline28 
  port map (
    d => register1_q_net_x1,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x12
  );
  pipeline29 : entity xil_defaultlib.rfsoc_byp_pipeline29_x0 
  port map (
    d => register0_q_net_x1,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register0_q_net
  );
  pipeline3_x0 : entity xil_defaultlib.rfsoc_byp_pipeline3_x0 
  port map (
    d => register2_q_net_x5,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x11
  );
  pipeline30 : entity xil_defaultlib.rfsoc_byp_pipeline30 
  port map (
    d => delay_sr_q_net_x14,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x10
  );
  pipeline31 : entity xil_defaultlib.rfsoc_byp_pipeline31 
  port map (
    d => logical4_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x9
  );
  pipeline32 : entity xil_defaultlib.rfsoc_byp_pipeline32 
  port map (
    d => register1_q_net_x29,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x8
  );
  pipeline33 : entity xil_defaultlib.rfsoc_byp_pipeline33 
  port map (
    d => d1_out_x1,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x7
  );
  pipeline34 : entity xil_defaultlib.rfsoc_byp_pipeline34 
  port map (
    d => register1_q_net_x20,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net
  );
  pipeline35 : entity xil_defaultlib.rfsoc_byp_pipeline35 
  port map (
    d => delay_sr_q_net_x14,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x6
  );
  pipeline37 : entity xil_defaultlib.rfsoc_byp_pipeline37 
  port map (
    d => register2_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x5
  );
  pipeline38_x0 : entity xil_defaultlib.rfsoc_byp_pipeline38_x0 
  port map (
    d => slice1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x4
  );
  pipeline39_x0 : entity xil_defaultlib.rfsoc_byp_pipeline39_x0 
  port map (
    d => register1_q_net_x18,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x0
  );
  pipeline4 : entity xil_defaultlib.rfsoc_byp_pipeline4_x0 
  port map (
    d => register2_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x3
  );
  pipeline5 : entity xil_defaultlib.rfsoc_byp_pipeline5_x1 
  port map (
    d => register2_q_net_x6,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x2
  );
  pipeline6 : entity xil_defaultlib.rfsoc_byp_pipeline6 
  port map (
    d => delay_sr_q_net_x3,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net
  );
  pipeline7 : entity xil_defaultlib.rfsoc_byp_pipeline7 
  port map (
    d => register2_q_net_x4,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x1
  );
  pipeline8 : entity xil_defaultlib.rfsoc_byp_pipeline8_x0 
  port map (
    d => register2_q_net_x8,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x0
  );
  pipeline81 : entity xil_defaultlib.rfsoc_byp_pipeline81 
  port map (
    d => register0_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x1
  );
  pipeline9 : entity xil_defaultlib.rfsoc_byp_pipeline9_x0 
  port map (
    d => register2_q_net_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x20
  );
  logical1 : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => bram_delay4_q_net,
    d1 => register1_q_net_x25,
    y => logical1_y_net
  );
  bram_delay : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => edge_op_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => bram_delay_q_net
  );
  bram_delay4 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_sr_q_net_x10,
    clk => a_clk_net,
    ce => a_ce_net,
    q => bram_delay4_q_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => bram_delay1_q_net,
    d1 => register1_q_net_x21,
    y => logical2_y_net
  );
  bram_delay1 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_sr_q_net_x12,
    clk => a_clk_net,
    ce => a_ce_net,
    q => bram_delay1_q_net
  );
  logical3 : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => bram_delay2_q_net,
    d1 => register1_q_net_x10,
    y => logical3_y_net
  );
  bram_delay2 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_sr_q_net_x9,
    clk => a_clk_net,
    ce => a_ce_net,
    q => bram_delay2_q_net
  );
  logical4 : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => bram_delay3_q_net,
    d1 => register1_q_net_x6,
    y => logical4_y_net
  );
  bram_delay3 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_sr_q_net_x11,
    clk => a_clk_net,
    ce => a_ce_net,
    q => bram_delay3_q_net
  );
  mux0 : entity xil_defaultlib.sysgen_mux_cf8e02c586 
  port map (
    clr => '0',
    sel => register1_q_net_x4,
    d0 => reinterpret_out_output_port_net_x2,
    d1 => reinterpret_out_output_port_net_x1,
    d2 => reinterpret_out_output_port_net_x0,
    d3 => reinterpret_out_output_port_net,
    clk => a_clk_net,
    ce => a_ce_net,
    y => mux0_y_net
  );
  x4_bit_cntr : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => register18_q_net,
    en => register2_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    op => x4_bit_cntr_op_net
  );
  register18 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x5,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register18_q_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 1,
    x_width => 32,
    y_width => 2
  )
  port map (
    x => x4_bit_cntr_op_net,
    y => slice1_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_RD/edge_detect2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect2 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect2;
architecture structural of rfsoc_byp_edge_detect2 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  relational1_op_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_ac59be6314 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_RD/pipeline
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline;
architecture structural of rfsoc_byp_pipeline is 
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
begin
  q <= register0_q_net;
  slice3_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice3_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_RD/pipeline1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline1;
architecture structural of rfsoc_byp_pipeline1 is 
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal valid : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
begin
  q <= register0_q_net;
  valid <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => valid,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_RD/pipeline8
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline8 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline8;
architecture structural of rfsoc_byp_pipeline8 is 
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
begin
  q <= register0_q_net;
  edge_op_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => edge_op_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_RD
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_mb_rd is
  port (
    rd_start : in std_logic_vector( 1-1 downto 0 );
    rd_start1 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    d_valid : out std_logic_vector( 1-1 downto 0 );
    r_addr : out std_logic_vector( 8-1 downto 0 );
    eof : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_mb_rd;
architecture structural of rfsoc_byp_mb_rd is 
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net_x1 : std_logic_vector( 8-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal valid : std_logic_vector( 1-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 7-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 7-1 downto 0 );
  signal counter1_op_net : std_logic_vector( 9-1 downto 0 );
  signal constant25_op_net : std_logic_vector( 9-1 downto 0 );
  signal constant5_op_net : std_logic_vector( 9-1 downto 0 );
  signal relational2_op_net : std_logic_vector( 1-1 downto 0 );
  signal logical5_y_net : std_logic_vector( 1-1 downto 0 );
  signal register3_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal slice_y_net : std_logic_vector( 8-1 downto 0 );
begin
  d_valid <= register0_q_net_x0;
  r_addr <= register0_q_net_x1;
  eof <= register0_q_net;
  delay_sr_q_net_x0 <= rd_start;
  delay_sr_q_net <= rd_start1;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  edge_detect2 : entity xil_defaultlib.rfsoc_byp_edge_detect2 
  port map (
    in_x0 => relational1_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net
  );
  pipeline : entity xil_defaultlib.rfsoc_byp_pipeline 
  port map (
    d => slice3_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register0_q_net_x1
  );
  pipeline1_x0 : entity xil_defaultlib.rfsoc_byp_pipeline1 
  port map (
    d => valid,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register0_q_net_x0
  );
  pipeline8 : entity xil_defaultlib.rfsoc_byp_pipeline8 
  port map (
    d => edge_op_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register0_q_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_43b81a9623 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => slice1_y_net,
    b => constant1_op_net,
    op => relational1_op_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 6,
    x_width => 9,
    y_width => 7
  )
  port map (
    x => counter1_op_net,
    y => slice1_y_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_fb8e46cc72 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  constant25 : entity xil_defaultlib.sysgen_constant_31d205c874 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant25_op_net
  );
  constant5 : entity xil_defaultlib.sysgen_constant_e1d8f6fd6b 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant5_op_net
  );
  counter1 : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i1",
    op_arith => xlUnsigned,
    op_width => 9
  )
  port map (
    clr => '0',
    load => delay_sr_q_net,
    din => constant5_op_net,
    rst => relational2_op_net,
    en => logical5_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter1_op_net
  );
  logical3 : entity xil_defaultlib.sysgen_logical_9c764d75e1 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => delay_sr_q_net_x0,
    d1 => register3_q_net,
    d2 => register1_q_net,
    y => valid
  );
  logical5 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => delay_sr_q_net,
    d1 => valid,
    y => logical5_y_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    d => valid,
    rst => relational2_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register3 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_sr_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register3_q_net
  );
  relational2 : entity xil_defaultlib.sysgen_relational_051d398f75 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => slice_y_net,
    b => constant25_op_net,
    op => relational2_op_net
  );
  slice : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 9,
    y_width => 8
  )
  port map (
    x => counter1_op_net,
    y => slice_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 9,
    y_width => 8
  )
  port map (
    x => counter1_op_net,
    y => slice3_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_WR0/edge_detect1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect1_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect1_x0;
architecture structural of rfsoc_byp_edge_detect1_x0 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational4_op_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  relational4_op_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational4_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => relational4_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_ac59be6314 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_WR0/edge_detect3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect3_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect3_x0;
architecture structural of rfsoc_byp_edge_detect3_x0 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational2_op_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  relational2_op_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational2_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => relational2_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_ac59be6314 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_WR0
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_mb_wr0 is
  port (
    msync : in std_logic_vector( 1-1 downto 0 );
    d_val : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    w_addr : out std_logic_vector( 8-1 downto 0 );
    rd_start : out std_logic_vector( 1-1 downto 0 );
    we : out std_logic_vector( 1-1 downto 0 );
    run : out std_logic_vector( 1-1 downto 0 );
    rd_start1 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_mb_wr0;
architecture structural of rfsoc_byp_mb_wr0 is 
  signal counter_op_net : std_logic_vector( 32-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal slice101_y_net : std_logic_vector( 8-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay6_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay5_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay8_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal edge_op_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal relational4_op_net : std_logic_vector( 1-1 downto 0 );
  signal edge_op_y_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal relational2_op_net : std_logic_vector( 1-1 downto 0 );
  signal counter1_op_net : std_logic_vector( 32-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal wr_pkt1_op_net : std_logic_vector( 2-1 downto 0 );
  signal wr_pkt2_op_net : std_logic_vector( 32-1 downto 0 );
  signal wr_pkt3_op_net : std_logic_vector( 32-1 downto 0 );
begin
  w_addr <= delay3_q_net;
  rd_start <= delay6_q_net;
  we <= delay4_q_net;
  run <= delay5_q_net;
  rd_start1 <= delay8_q_net;
  register1_q_net <= msync;
  delay1_q_net <= d_val;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  edge_detect1_x0 : entity xil_defaultlib.rfsoc_byp_edge_detect1_x0 
  port map (
    in_x0 => relational4_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net_x0
  );
  edge_detect3 : entity xil_defaultlib.rfsoc_byp_edge_detect3_x0 
  port map (
    in_x0 => relational2_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net_x1
  );
  counter1 : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => logical2_y_net,
    en => delay1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter1_op_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => relational1_op_net,
    d1 => register1_q_net,
    y => logical2_y_net
  );
  wr_pkt1 : entity xil_defaultlib.sysgen_constant_3500741f9a 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt1_op_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_9bd24157dd 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter1_op_net,
    b => wr_pkt1_op_net,
    op => relational1_op_net
  );
  counter : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => logical1_y_net,
    en => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter_op_net
  );
  delay3 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => slice101_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay3_q_net
  );
  delay4 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay4_q_net
  );
  delay5 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay5_q_net
  );
  delay6 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => edge_op_y_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay6_q_net
  );
  delay8 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => edge_op_y_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay8_q_net
  );
  logical1 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => register1_q_net,
    d1 => relational4_op_net,
    y => logical1_y_net
  );
  relational2 : entity xil_defaultlib.sysgen_relational_4647e28722 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter_op_net,
    b => wr_pkt2_op_net,
    op => relational2_op_net
  );
  relational4 : entity xil_defaultlib.sysgen_relational_4647e28722 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter_op_net,
    b => wr_pkt3_op_net,
    op => relational4_op_net
  );
  slice101 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 32,
    y_width => 8
  )
  port map (
    x => counter_op_net,
    y => slice101_y_net
  );
  wr_pkt2 : entity xil_defaultlib.sysgen_constant_090cfeb884 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt2_op_net
  );
  wr_pkt3 : entity xil_defaultlib.sysgen_constant_736a61b358 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt3_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_WR1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_mb_wr1 is
  port (
    msync : in std_logic_vector( 1-1 downto 0 );
    d_val : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    w_addr : out std_logic_vector( 8-1 downto 0 );
    we : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_mb_wr1;
architecture structural of rfsoc_byp_mb_wr1 is 
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay10_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal counter1_op_net : std_logic_vector( 32-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal wr_pkt1_op_net : std_logic_vector( 2-1 downto 0 );
  signal counter_op_net : std_logic_vector( 32-1 downto 0 );
  signal slice101_y_net : std_logic_vector( 8-1 downto 0 );
begin
  w_addr <= delay3_q_net;
  we <= delay4_q_net;
  register1_q_net <= msync;
  delay10_q_net <= d_val;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  counter1 : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => logical2_y_net,
    en => delay10_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter1_op_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => relational1_op_net,
    d1 => register1_q_net,
    y => logical2_y_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_9bd24157dd 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter1_op_net,
    b => wr_pkt1_op_net,
    op => relational1_op_net
  );
  wr_pkt1 : entity xil_defaultlib.sysgen_constant_3500741f9a 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt1_op_net
  );
  counter : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => register1_q_net,
    en => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter_op_net
  );
  delay3 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => slice101_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay3_q_net
  );
  delay4 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay4_q_net
  );
  slice101 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 32,
    y_width => 8
  )
  port map (
    x => counter_op_net,
    y => slice101_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_WR2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_mb_wr2 is
  port (
    msync : in std_logic_vector( 1-1 downto 0 );
    d_val : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    w_addr : out std_logic_vector( 8-1 downto 0 );
    we : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_mb_wr2;
architecture structural of rfsoc_byp_mb_wr2 is 
  signal slice101_y_net : std_logic_vector( 8-1 downto 0 );
  signal wr_pkt1_op_net : std_logic_vector( 2-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay12_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal counter1_op_net : std_logic_vector( 32-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal counter_op_net : std_logic_vector( 32-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
begin
  w_addr <= delay3_q_net;
  we <= delay4_q_net;
  register1_q_net <= msync;
  delay12_q_net <= d_val;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  counter1 : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => logical2_y_net,
    en => delay12_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter1_op_net
  );
  counter : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => register1_q_net,
    en => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter_op_net
  );
  delay3 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => slice101_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay3_q_net
  );
  delay4 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay4_q_net
  );
  slice101 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 32,
    y_width => 8
  )
  port map (
    x => counter_op_net,
    y => slice101_y_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => relational1_op_net,
    d1 => register1_q_net,
    y => logical2_y_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_9bd24157dd 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter1_op_net,
    b => wr_pkt1_op_net,
    op => relational1_op_net
  );
  wr_pkt1 : entity xil_defaultlib.sysgen_constant_3500741f9a 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt1_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/MB_WR3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_mb_wr3 is
  port (
    msync : in std_logic_vector( 1-1 downto 0 );
    d_val : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    w_addr : out std_logic_vector( 8-1 downto 0 );
    we : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_mb_wr3;
architecture structural of rfsoc_byp_mb_wr3 is 
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay13_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal counter_op_net : std_logic_vector( 32-1 downto 0 );
  signal relational1_op_net : std_logic_vector( 1-1 downto 0 );
  signal slice101_y_net : std_logic_vector( 8-1 downto 0 );
  signal counter1_op_net : std_logic_vector( 32-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal wr_pkt1_op_net : std_logic_vector( 2-1 downto 0 );
begin
  w_addr <= delay3_q_net;
  we <= delay4_q_net;
  register1_q_net <= msync;
  delay13_q_net <= d_val;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  counter : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => register1_q_net,
    en => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter_op_net
  );
  delay3 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => slice101_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay3_q_net
  );
  delay4 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 2,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay4_q_net
  );
  slice101 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 32,
    y_width => 8
  )
  port map (
    x => counter_op_net,
    y => slice101_y_net
  );
  counter1 : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => logical2_y_net,
    en => delay13_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter1_op_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => relational1_op_net,
    d1 => register1_q_net,
    y => logical2_y_net
  );
  relational1 : entity xil_defaultlib.sysgen_relational_9bd24157dd 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => counter1_op_net,
    b => wr_pkt1_op_net,
    op => relational1_op_net
  );
  wr_pkt1 : entity xil_defaultlib.sysgen_constant_3500741f9a 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt1_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl;
architecture structural of rfsoc_byp_delay_srl is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay6_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay6_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay6_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_ff90fbc3e7 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl1 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl1;
architecture structural of rfsoc_byp_delay_srl1 is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay8_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay8_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay8_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_ac68fc81da 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl10
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl10 is
  port (
    in_x0 : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_delay_srl10;
architecture structural of rfsoc_byp_delay_srl10 is 
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 8-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay3_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => delay3_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_810e49c397 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl11
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl11 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl11;
architecture structural of rfsoc_byp_delay_srl11 is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay4_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay4_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl2 is
  port (
    in_x0 : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_delay_srl2;
architecture structural of rfsoc_byp_delay_srl2 is 
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 8-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay3_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => delay3_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_810e49c397 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl3 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl3;
architecture structural of rfsoc_byp_delay_srl3 is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay4_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay4_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl4 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl4;
architecture structural of rfsoc_byp_delay_srl4 is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay5_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay5_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay5_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl6
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl6 is
  port (
    in_x0 : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_delay_srl6;
architecture structural of rfsoc_byp_delay_srl6 is 
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 8-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay3_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => delay3_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_810e49c397 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl7
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl7 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl7;
architecture structural of rfsoc_byp_delay_srl7 is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay4_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay4_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl8
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl8 is
  port (
    in_x0 : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_delay_srl8;
architecture structural of rfsoc_byp_delay_srl8 is 
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 8-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay3_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 8
  )
  port map (
    en => '1',
    rst => '0',
    d => delay3_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_810e49c397 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/delay_srl9
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_delay_srl9 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_delay_srl9;
architecture structural of rfsoc_byp_delay_srl9 is 
  signal delay_sr_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_ff_q_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= delay_sr_q_net;
  delay4_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay_ff : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay4_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_ff_q_net
  );
  delay_sr : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => delay_ff_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_sr_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/edge_detect2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect2_x0 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect2_x0;
architecture structural of rfsoc_byp_edge_detect2_x0 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal delay16_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  delay16_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => delay16_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => delay16_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_ac59be6314 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/edge_detect3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect3 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect3;
architecture structural of rfsoc_byp_edge_detect3 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  register2_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/edge_detect1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect1 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect1;
architecture structural of rfsoc_byp_edge_detect1 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  register1_q_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_ch/pipeline5
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline5 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline5;
architecture structural of rfsoc_byp_pipeline5 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_ch
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pack_ch is
  port (
    sync_in : in std_logic_vector( 1-1 downto 0 );
    eof0 : in std_logic_vector( 1-1 downto 0 );
    din : in std_logic_vector( 512-1 downto 0 );
    base_ip : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    din0 : out std_logic_vector( 512-1 downto 0 );
    x_base_chan : out std_logic_vector( 6-1 downto 0 );
    dest_ip0 : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pack_ch;
architecture structural of rfsoc_byp_pack_ch is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal nbytes_packet_size_y_net : std_logic_vector( 6-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal eof64_0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x2 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal count1_op_net : std_logic_vector( 2-1 downto 0 );
  signal count5_op_net : std_logic_vector( 2-1 downto 0 );
  signal nbytes_word_size_op_net : std_logic_vector( 4-1 downto 0 );
begin
  din0 <= register1_q_net;
  x_base_chan <= nbytes_packet_size_y_net;
  dest_ip0 <= addsub1_s_net;
  register1_q_net_x0 <= sync_in;
  eof64_0 <= eof0;
  register1_q_net_x2 <= din;
  register1_q_net_x1 <= base_ip;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  pipeline5 : entity xil_defaultlib.rfsoc_byp_pipeline5 
  port map (
    d => register1_q_net_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net
  );
  addsub1 : entity xil_defaultlib.sysgen_addsub_173080f056 
  port map (
    clr => '0',
    a => count1_op_net,
    b => register1_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    s => addsub1_s_net
  );
  count1 : entity xil_defaultlib.sysgen_counter_a93f2478fa 
  port map (
    clr => '0',
    rst => register1_q_net_x0,
    en => eof64_0,
    clk => a_clk_net,
    ce => a_ce_net,
    op => count1_op_net
  );
  count5 : entity xil_defaultlib.sysgen_counter_a93f2478fa 
  port map (
    clr => '0',
    rst => register1_q_net_x0,
    en => eof64_0,
    clk => a_clk_net,
    ce => a_ce_net,
    op => count5_op_net
  );
  nbytes_packet_size : entity xil_defaultlib.sysgen_concat_398ffc2097 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => nbytes_word_size_op_net,
    in1 => count5_op_net,
    y => nbytes_packet_size_y_net
  );
  nbytes_word_size : entity xil_defaultlib.sysgen_constant_7a7113ab75 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => nbytes_word_size_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/byte_swap
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_byte_swap is
  port (
    bus_in : in std_logic_vector( 512-1 downto 0 );
    msb_out8 : out std_logic_vector( 64-1 downto 0 );
    out7 : out std_logic_vector( 64-1 downto 0 );
    out6 : out std_logic_vector( 64-1 downto 0 );
    out5 : out std_logic_vector( 64-1 downto 0 );
    out4 : out std_logic_vector( 64-1 downto 0 );
    out3 : out std_logic_vector( 64-1 downto 0 );
    out2 : out std_logic_vector( 64-1 downto 0 );
    lsb_out1 : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_byte_swap;
architecture structural of rfsoc_byp_byte_swap is 
  signal reinterpret8_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal assert18_dout_net : std_logic_vector( 512-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 64-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  assert18_dout_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 63,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 64,
    new_msb => 127,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 128,
    new_msb => 191,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 192,
    new_msb => 255,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 256,
    new_msb => 319,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 320,
    new_msb => 383,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 384,
    new_msb => 447,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 448,
    new_msb => 511,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => assert18_dout_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge1/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join is
  port (
    in1 : in std_logic_vector( 64-1 downto 0 );
    in2 : in std_logic_vector( 64-1 downto 0 );
    in3 : in std_logic_vector( 64-1 downto 0 );
    in4 : in std_logic_vector( 64-1 downto 0 );
    in5 : in std_logic_vector( 64-1 downto 0 );
    in6 : in std_logic_vector( 64-1 downto 0 );
    in7 : in std_logic_vector( 64-1 downto 0 );
    in8 : in std_logic_vector( 64-1 downto 0 );
    bus_out : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_join;
architecture structural of rfsoc_byp_join is 
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_b19a6ef706 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge1/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split is
  port (
    bus_in : in std_logic_vector( 512-1 downto 0 );
    msb_out8 : out std_logic_vector( 64-1 downto 0 );
    out7 : out std_logic_vector( 64-1 downto 0 );
    out6 : out std_logic_vector( 64-1 downto 0 );
    out5 : out std_logic_vector( 64-1 downto 0 );
    out4 : out std_logic_vector( 64-1 downto 0 );
    out3 : out std_logic_vector( 64-1 downto 0 );
    out2 : out std_logic_vector( 64-1 downto 0 );
    lsb_out1 : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_split;
architecture structural of rfsoc_byp_split is 
  signal reinterpret8_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 64-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 64-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 63,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 64,
    new_msb => 127,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 128,
    new_msb => 191,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 192,
    new_msb => 255,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 256,
    new_msb => 319,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 320,
    new_msb => 383,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 384,
    new_msb => 447,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 448,
    new_msb => 511,
    x_width => 512,
    y_width => 64
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge1 is
  port (
    din : in std_logic_vector( 512-1 downto 0 );
    dout : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_munge1;
architecture structural of rfsoc_byp_munge1 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal concat6_y_net : std_logic_vector( 512-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 512-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  concat6_y_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concat6_y_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_0f2794a428 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge2/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x2 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x2;
architecture structural of rfsoc_byp_join_x2 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge2/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x3 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x3;
architecture structural of rfsoc_byp_split_x3 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge2 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge2;
architecture structural of rfsoc_byp_munge2 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret8_output_port_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x2 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net_x0,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x3 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net_x0,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge3/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x3 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x3;
architecture structural of rfsoc_byp_join_x3 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge3/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x4 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x4;
architecture structural of rfsoc_byp_split_x4 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge3_x0 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge3_x0;
architecture structural of rfsoc_byp_munge3_x0 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret7_output_port_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x3 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net_x0,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x4 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net_x0,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge4/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x4 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x4;
architecture structural of rfsoc_byp_join_x4 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge4/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x5 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x5;
architecture structural of rfsoc_byp_split_x5 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge4_x0 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge4_x0;
architecture structural of rfsoc_byp_munge4_x0 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret6_output_port_net <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x4 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net_x0,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x5 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net_x0,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge5/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x5 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x5;
architecture structural of rfsoc_byp_join_x5 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge5/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x6 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x6;
architecture structural of rfsoc_byp_split_x6 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge5
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge5 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge5;
architecture structural of rfsoc_byp_munge5 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret5_output_port_net_x0 <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x5 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x6 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge6/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x6 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x6;
architecture structural of rfsoc_byp_join_x6 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge6/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x2 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x2;
architecture structural of rfsoc_byp_split_x2 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge6
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge6 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge6;
architecture structural of rfsoc_byp_munge6 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret4_output_port_net_x0 <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x6 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x2 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge7/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x7 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x7;
architecture structural of rfsoc_byp_join_x7 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge7/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x7 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x7;
architecture structural of rfsoc_byp_split_x7 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge7
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge7 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge7;
architecture structural of rfsoc_byp_munge7 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret3_output_port_net_x0 <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x7 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x7 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge8/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x8 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x8;
architecture structural of rfsoc_byp_join_x8 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge8/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x8 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x8;
architecture structural of rfsoc_byp_split_x8 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge8
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge8 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge8;
architecture structural of rfsoc_byp_munge8 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret2_output_port_net_x0 <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x8 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x8 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge9/join
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_join_x9 is
  port (
    in1 : in std_logic_vector( 8-1 downto 0 );
    in2 : in std_logic_vector( 8-1 downto 0 );
    in3 : in std_logic_vector( 8-1 downto 0 );
    in4 : in std_logic_vector( 8-1 downto 0 );
    in5 : in std_logic_vector( 8-1 downto 0 );
    in6 : in std_logic_vector( 8-1 downto 0 );
    in7 : in std_logic_vector( 8-1 downto 0 );
    in8 : in std_logic_vector( 8-1 downto 0 );
    bus_out : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_join_x9;
architecture structural of rfsoc_byp_join_x9 is 
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net_x0 : std_logic_vector( 8-1 downto 0 );
begin
  bus_out <= concatenate_y_net;
  reinterpret1_output_port_net_x0 <= in1;
  reinterpret2_output_port_net_x0 <= in2;
  reinterpret3_output_port_net_x0 <= in3;
  reinterpret4_output_port_net_x0 <= in4;
  reinterpret5_output_port_net_x0 <= in5;
  reinterpret6_output_port_net_x0 <= in6;
  reinterpret7_output_port_net <= in7;
  reinterpret8_output_port_net <= in8;
  concatenate : entity xil_defaultlib.sysgen_concat_d4a95726a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret1_output_port_net,
    in1 => reinterpret2_output_port_net,
    in2 => reinterpret3_output_port_net,
    in3 => reinterpret4_output_port_net,
    in4 => reinterpret5_output_port_net,
    in5 => reinterpret6_output_port_net,
    in6 => reinterpret7_output_port_net_x0,
    in7 => reinterpret8_output_port_net_x0,
    y => concatenate_y_net
  );
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret2_output_port_net_x0,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret3_output_port_net_x0,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret4_output_port_net_x0,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret5_output_port_net_x0,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret6_output_port_net_x0,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret7_output_port_net,
    output_port => reinterpret7_output_port_net_x0
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret8_output_port_net,
    output_port => reinterpret8_output_port_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge9/split
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_split_x9 is
  port (
    bus_in : in std_logic_vector( 64-1 downto 0 );
    msb_out8 : out std_logic_vector( 8-1 downto 0 );
    out7 : out std_logic_vector( 8-1 downto 0 );
    out6 : out std_logic_vector( 8-1 downto 0 );
    out5 : out std_logic_vector( 8-1 downto 0 );
    out4 : out std_logic_vector( 8-1 downto 0 );
    out3 : out std_logic_vector( 8-1 downto 0 );
    out2 : out std_logic_vector( 8-1 downto 0 );
    lsb_out1 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_split_x9;
architecture structural of rfsoc_byp_split_x9 is 
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice4_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice5_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice6_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice7_y_net : std_logic_vector( 8-1 downto 0 );
  signal slice8_y_net : std_logic_vector( 8-1 downto 0 );
begin
  msb_out8 <= reinterpret8_output_port_net;
  out7 <= reinterpret7_output_port_net;
  out6 <= reinterpret6_output_port_net;
  out5 <= reinterpret5_output_port_net;
  out4 <= reinterpret4_output_port_net;
  out3 <= reinterpret3_output_port_net;
  out2 <= reinterpret2_output_port_net;
  lsb_out1 <= reinterpret1_output_port_net;
  reinterpret_output_port_net <= bus_in;
  reinterpret1 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice1_y_net,
    output_port => reinterpret1_output_port_net
  );
  reinterpret2 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice2_y_net,
    output_port => reinterpret2_output_port_net
  );
  reinterpret3 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice3_y_net,
    output_port => reinterpret3_output_port_net
  );
  reinterpret4 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice4_y_net,
    output_port => reinterpret4_output_port_net
  );
  reinterpret5 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice5_y_net,
    output_port => reinterpret5_output_port_net
  );
  reinterpret6 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice6_y_net,
    output_port => reinterpret6_output_port_net
  );
  reinterpret7 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice7_y_net,
    output_port => reinterpret7_output_port_net
  );
  reinterpret8 : entity xil_defaultlib.sysgen_reinterpret_3a4cd5a858 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => slice8_y_net,
    output_port => reinterpret8_output_port_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice1_y_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice2_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 23,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice3_y_net
  );
  slice4 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 24,
    new_msb => 31,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice4_y_net
  );
  slice5 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 32,
    new_msb => 39,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice5_y_net
  );
  slice6 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 40,
    new_msb => 47,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice6_y_net
  );
  slice7 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 48,
    new_msb => 55,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice7_y_net
  );
  slice8 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 56,
    new_msb => 63,
    x_width => 64,
    y_width => 8
  )
  port map (
    x => reinterpret_output_port_net,
    y => slice8_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng/munge9
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_munge9 is
  port (
    din : in std_logic_vector( 64-1 downto 0 );
    dout : out std_logic_vector( 64-1 downto 0 )
  );
end rfsoc_byp_munge9;
architecture structural of rfsoc_byp_munge9 is 
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal concatenate_y_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret8_output_port_net : std_logic_vector( 8-1 downto 0 );
  signal reinterpret_output_port_net : std_logic_vector( 64-1 downto 0 );
begin
  dout <= reinterpret_out_output_port_net;
  reinterpret1_output_port_net_x0 <= din;
  join : entity xil_defaultlib.rfsoc_byp_join_x9 
  port map (
    in1 => reinterpret1_output_port_net,
    in2 => reinterpret2_output_port_net,
    in3 => reinterpret3_output_port_net,
    in4 => reinterpret4_output_port_net,
    in5 => reinterpret5_output_port_net,
    in6 => reinterpret6_output_port_net,
    in7 => reinterpret7_output_port_net,
    in8 => reinterpret8_output_port_net,
    bus_out => concatenate_y_net
  );
  split : entity xil_defaultlib.rfsoc_byp_split_x9 
  port map (
    bus_in => reinterpret_output_port_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  reinterpret : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => reinterpret1_output_port_net_x0,
    output_port => reinterpret_output_port_net
  );
  reinterpret_out : entity xil_defaultlib.sysgen_reinterpret_38b1bd16de 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    input_port => concatenate_y_net,
    output_port => reinterpret_out_output_port_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pack_deng
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pack_deng is
  port (
    data256_ready : in std_logic_vector( 1-1 downto 0 );
    data256_valid : in std_logic_vector( 1-1 downto 0 );
    data256 : in std_logic_vector( 512-1 downto 0 );
    hdr_heap_id : in std_logic_vector( 48-1 downto 0 );
    hdr_heap_size : in std_logic_vector( 48-1 downto 0 );
    hdr_heap_offset : in std_logic_vector( 48-1 downto 0 );
    hdr_pkt_len_words : in std_logic_vector( 48-1 downto 0 );
    hdr5_0x1600_dir : in std_logic_vector( 48-1 downto 0 );
    hdr6_0x4101_dir : in std_logic_vector( 48-1 downto 0 );
    hdr7_0x4103_dir : in std_logic_vector( 48-1 downto 0 );
    hdr8_0x4300 : in std_logic_vector( 48-1 downto 0 );
    ip_in : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    data_out : out std_logic_vector( 512-1 downto 0 );
    valid_out : out std_logic_vector( 1-1 downto 0 );
    eof : out std_logic_vector( 1-1 downto 0 );
    data_overflow : out std_logic_vector( 1-1 downto 0 );
    ip_out : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pack_deng;
architecture structural of rfsoc_byp_pack_deng is 
  signal header_cat1_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat2_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat3_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat4_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat5_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat6_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat7_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat8_y_net : std_logic_vector( 64-1 downto 0 );
  signal header_cat9_y_net : std_logic_vector( 64-1 downto 0 );
  signal spid1 : std_logic_vector( 16-1 downto 0 );
  signal spid2 : std_logic_vector( 16-1 downto 0 );
  signal spid3 : std_logic_vector( 16-1 downto 0 );
  signal spid4 : std_logic_vector( 16-1 downto 0 );
  signal spid5 : std_logic_vector( 16-1 downto 0 );
  signal spid6 : std_logic_vector( 16-1 downto 0 );
  signal spid7 : std_logic_vector( 16-1 downto 0 );
  signal spid8_x0 : std_logic_vector( 16-1 downto 0 );
  signal spid8 : std_logic_vector( 16-1 downto 0 );
  signal register1_q_net_x3 : std_logic_vector( 8-1 downto 0 );
  signal done : std_logic_vector( 1-1 downto 0 );
  signal data_ctr : std_logic_vector( 11-1 downto 0 );
  signal assert_data1_dout_net : std_logic_vector( 512-1 downto 0 );
  signal assert_data2_dout_net : std_logic_vector( 512-1 downto 0 );
  signal delay_data10_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data11_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data12_q_net : std_logic_vector( 512-1 downto 0 );
  signal delay_data13_q_net : std_logic_vector( 512-1 downto 0 );
  signal delay_data14_q_net : std_logic_vector( 512-1 downto 0 );
  signal delay_data15_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data16_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data18_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data19_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data23_q_net : std_logic_vector( 512-1 downto 0 );
  signal mainmux2_y_net : std_logic_vector( 512-1 downto 0 );
  signal delay_data25_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_data8_q_net : std_logic_vector( 1-1 downto 0 );
  signal hdr_heap_id_x0 : std_logic_vector( 48-1 downto 0 );
  signal hdr_heap_size_x0 : std_logic_vector( 48-1 downto 0 );
  signal hdr_heap_offset_x0 : std_logic_vector( 48-1 downto 0 );
  signal hdr5_0x1600_dir_x0 : std_logic_vector( 48-1 downto 0 );
  signal hdr6_0x4101_dir_x0 : std_logic_vector( 48-1 downto 0 );
  signal hdr7_0x4103_dir_x0 : std_logic_vector( 48-1 downto 0 );
  signal hdr8_0x4300_x0 : std_logic_vector( 48-1 downto 0 );
  signal header_4cast4_dout_net : std_logic_vector( 42-1 downto 0 );
  signal header_4cat4_y_net : std_logic_vector( 48-1 downto 0 );
  signal header_4const4_op_net : std_logic_vector( 6-1 downto 0 );
  signal header_4const1_op_net : std_logic_vector( 48-1 downto 0 );
  signal reinterpret_out_output_port_net_x7 : std_logic_vector( 512-1 downto 0 );
  signal out_done_x0 : std_logic_vector( 1-1 downto 0 );
  signal out_done : std_logic_vector( 1-1 downto 0 );
  signal out_overflow : std_logic_vector( 1-1 downto 0 );
  signal delay_data26_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 512-1 downto 0 );
  signal heap_id : std_logic_vector( 48-1 downto 0 );
  signal heap_size : std_logic_vector( 48-1 downto 0 );
  signal heap_offset : std_logic_vector( 48-1 downto 0 );
  signal convert1_dout_net : std_logic_vector( 48-1 downto 0 );
  signal x0x1600 : std_logic_vector( 48-1 downto 0 );
  signal x0x4101 : std_logic_vector( 48-1 downto 0 );
  signal x0x4103 : std_logic_vector( 48-1 downto 0 );
  signal nwords_packet_size5_op_net : std_logic_vector( 48-1 downto 0 );
  signal ip : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal reinterpret8_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret7_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret6_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret5_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret4_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret3_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret2_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret1_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal assert18_dout_net : std_logic_vector( 512-1 downto 0 );
  signal concat6_y_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret_out_output_port_net_x6 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net_x5 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net_x0 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net_x4 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net_x3 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net_x2 : std_logic_vector( 64-1 downto 0 );
  signal reinterpret_out_output_port_net_x1 : std_logic_vector( 64-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 49-1 downto 0 );
  signal hdr_pkt_len_words_x0 : std_logic_vector( 48-1 downto 0 );
  signal only_one1_op_net : std_logic_vector( 1-1 downto 0 );
  signal assert17_dout_net : std_logic_vector( 64-1 downto 0 );
  signal spd_hdr : std_logic_vector( 64-1 downto 0 );
  signal out_data : std_logic_vector( 512-1 downto 0 );
  signal data64_rdy : std_logic_vector( 1-1 downto 0 );
  signal data64 : std_logic_vector( 512-1 downto 0 );
  signal data64_valid : std_logic_vector( 1-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 8-1 downto 0 );
  signal constant4_op_net : std_logic_vector( 8-1 downto 0 );
  signal item_id_width_op_net : std_logic_vector( 8-1 downto 0 );
  signal heap_addr_width_op_net : std_logic_vector( 8-1 downto 0 );
  signal constant7_op_net : std_logic_vector( 16-1 downto 0 );
  signal num_item_pts_op_net : std_logic_vector( 16-1 downto 0 );
  signal concat1_y_net : std_logic_vector( 512-1 downto 0 );
  signal header_assert1_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert2_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert3_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert4_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert5_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert6_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert7_dout_net : std_logic_vector( 64-1 downto 0 );
  signal concat2_y_net : std_logic_vector( 512-1 downto 0 );
  signal header_assert8_dout_net : std_logic_vector( 64-1 downto 0 );
  signal header_assert9_dout_net : std_logic_vector( 64-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
  signal register_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical_y_net : std_logic_vector( 1-1 downto 0 );
  signal overflow : std_logic_vector( 1-1 downto 0 );
  signal busy : std_logic_vector( 1-1 downto 0 );
  signal wr_hdr : std_logic_vector( 1-1 downto 0 );
  signal delay_data6_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_data7_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical7_y_net : std_logic_vector( 1-1 downto 0 );
  signal wr_data : std_logic_vector( 1-1 downto 0 );
  signal delay_data17_q_net : std_logic_vector( 1-1 downto 0 );
begin
  data_out <= reinterpret_out_output_port_net_x7;
  valid_out <= out_done_x0;
  eof <= out_done;
  data_overflow <= out_overflow;
  ip_out <= delay_data26_q_net;
  register1_q_net <= data256_ready;
  register1_q_net_x2 <= data256_valid;
  register1_q_net_x1 <= data256;
  heap_id <= hdr_heap_id;
  heap_size <= hdr_heap_size;
  heap_offset <= hdr_heap_offset;
  convert1_dout_net <= hdr_pkt_len_words;
  x0x1600 <= hdr5_0x1600_dir;
  x0x4101 <= hdr6_0x4101_dir;
  x0x4103 <= hdr7_0x4103_dir;
  nwords_packet_size5_op_net <= hdr8_0x4300;
  ip <= ip_in;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  byte_swap : entity xil_defaultlib.rfsoc_byp_byte_swap 
  port map (
    bus_in => assert18_dout_net,
    msb_out8 => reinterpret8_output_port_net,
    out7 => reinterpret7_output_port_net,
    out6 => reinterpret6_output_port_net,
    out5 => reinterpret5_output_port_net,
    out4 => reinterpret4_output_port_net,
    out3 => reinterpret3_output_port_net,
    out2 => reinterpret2_output_port_net,
    lsb_out1 => reinterpret1_output_port_net
  );
  munge1 : entity xil_defaultlib.rfsoc_byp_munge1 
  port map (
    din => concat6_y_net,
    dout => reinterpret_out_output_port_net_x7
  );
  munge2 : entity xil_defaultlib.rfsoc_byp_munge2 
  port map (
    din => reinterpret8_output_port_net,
    dout => reinterpret_out_output_port_net_x6
  );
  munge3 : entity xil_defaultlib.rfsoc_byp_munge3_x0 
  port map (
    din => reinterpret7_output_port_net,
    dout => reinterpret_out_output_port_net_x5
  );
  munge4 : entity xil_defaultlib.rfsoc_byp_munge4_x0 
  port map (
    din => reinterpret6_output_port_net,
    dout => reinterpret_out_output_port_net_x0
  );
  munge5 : entity xil_defaultlib.rfsoc_byp_munge5 
  port map (
    din => reinterpret5_output_port_net,
    dout => reinterpret_out_output_port_net
  );
  munge6 : entity xil_defaultlib.rfsoc_byp_munge6 
  port map (
    din => reinterpret4_output_port_net,
    dout => reinterpret_out_output_port_net_x4
  );
  munge7 : entity xil_defaultlib.rfsoc_byp_munge7 
  port map (
    din => reinterpret3_output_port_net,
    dout => reinterpret_out_output_port_net_x3
  );
  munge8 : entity xil_defaultlib.rfsoc_byp_munge8 
  port map (
    din => reinterpret2_output_port_net,
    dout => reinterpret_out_output_port_net_x2
  );
  munge9 : entity xil_defaultlib.rfsoc_byp_munge9 
  port map (
    din => reinterpret1_output_port_net,
    dout => reinterpret_out_output_port_net_x1
  );
  addsub1 : entity xil_defaultlib.rfsoc_byp_xladdsub 
  generic map (
    a_arith => xlUnsigned,
    a_bin_pt => 0,
    a_width => 48,
    b_arith => xlUnsigned,
    b_bin_pt => 0,
    b_width => 1,
    c_has_c_out => 0,
    c_latency => 0,
    c_output_width => 49,
    core_name0 => "rfsoc_byp_c_addsub_v12_0_i0",
    extra_registers => 0,
    full_s_arith => 2,
    full_s_width => 49,
    latency => 0,
    overflow => 1,
    quantization => 1,
    s_arith => xlSigned,
    s_bin_pt => 0,
    s_width => 49
  )
  port map (
    clr => '0',
    en => "1",
    a => hdr_pkt_len_words_x0,
    b => only_one1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    s => addsub1_s_net
  );
  assert17 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => spd_hdr,
    dout => assert17_dout_net
  );
  assert18 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 512,
    dout_width => 512
  )
  port map (
    din => out_data,
    dout => assert18_dout_net
  );
  assert5 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 1,
    dout_width => 1
  )
  port map (
    din => register1_q_net,
    dout => data64_rdy
  );
  assert6 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 512,
    dout_width => 512
  )
  port map (
    din => register1_q_net_x1,
    dout => data64
  );
  assert7 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 1,
    dout_width => 1
  )
  port map (
    din => register1_q_net_x2,
    dout => data64_valid
  );
  concat : entity xil_defaultlib.sysgen_concat_0eed05a53b 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => constant3_op_net,
    in1 => constant4_op_net,
    in2 => item_id_width_op_net,
    in3 => heap_addr_width_op_net,
    in4 => constant7_op_net,
    in5 => num_item_pts_op_net,
    y => spd_hdr
  );
  concat1 : entity xil_defaultlib.sysgen_concat_b19a6ef706 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => assert17_dout_net,
    in1 => header_assert1_dout_net,
    in2 => header_assert2_dout_net,
    in3 => header_assert3_dout_net,
    in4 => header_assert4_dout_net,
    in5 => header_assert5_dout_net,
    in6 => header_assert6_dout_net,
    in7 => header_assert7_dout_net,
    y => concat1_y_net
  );
  concat2 : entity xil_defaultlib.sysgen_concat_b19a6ef706 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => header_assert8_dout_net,
    in1 => header_assert9_dout_net,
    in2 => header_assert9_dout_net,
    in3 => header_assert9_dout_net,
    in4 => header_assert9_dout_net,
    in5 => header_assert9_dout_net,
    in6 => header_assert9_dout_net,
    in7 => header_assert9_dout_net,
    y => concat2_y_net
  );
  concat6 : entity xil_defaultlib.sysgen_concat_b19a6ef706 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => reinterpret_out_output_port_net_x6,
    in1 => reinterpret_out_output_port_net_x5,
    in2 => reinterpret_out_output_port_net_x0,
    in3 => reinterpret_out_output_port_net,
    in4 => reinterpret_out_output_port_net_x4,
    in5 => reinterpret_out_output_port_net_x3,
    in6 => reinterpret_out_output_port_net_x2,
    in7 => reinterpret_out_output_port_net_x1,
    y => concat6_y_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_89f26d8ee8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  constant4 : entity xil_defaultlib.sysgen_constant_8a0a401578 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant4_op_net
  );
  constant7 : entity xil_defaultlib.sysgen_constant_f02f49a5ed 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant7_op_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => register_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  logical : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => data64_valid,
    y => logical_y_net
  );
  logical5 : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => busy,
    d1 => data64_rdy,
    y => overflow
  );
  logical6 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => delay_data6_q_net,
    d1 => delay_data7_q_net,
    y => wr_hdr
  );
  logical7 : entity xil_defaultlib.sysgen_logical_57feb20476 
  port map (
    clr => '0',
    d0 => wr_hdr,
    d1 => wr_data,
    clk => a_clk_net,
    ce => a_ce_net,
    y => logical7_y_net
  );
  logical8 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => delay_data17_q_net,
    d1 => out_overflow,
    y => out_done
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    d => data64_valid,
    rst => data64_rdy,
    en => data64_valid,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    rst => "0",
    d => ip,
    en => logical_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x3
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    d => data64_rdy,
    rst => done,
    en => data64_rdy,
    clk => a_clk_net,
    ce => a_ce_net,
    q => busy
  );
  relational3 : entity xil_defaultlib.sysgen_relational_61b5b50af4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => data_ctr,
    b => addsub1_s_net,
    op => done
  );
  assert_data1 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 512,
    dout_width => 512
  )
  port map (
    din => concat2_y_net,
    dout => assert_data1_dout_net
  );
  assert_data2 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 512,
    dout_width => 512
  )
  port map (
    din => concat1_y_net,
    dout => assert_data2_dout_net
  );
  data_ctr1 : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i2",
    op_arith => xlUnsigned,
    op_width => 11
  )
  port map (
    clr => '0',
    rst => wr_hdr,
    en => wr_data,
    clk => a_clk_net,
    ce => a_ce_net,
    op => data_ctr
  );
  delay_data10 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => wr_hdr,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data10_q_net
  );
  delay_data11 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_data10_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data11_q_net
  );
  delay_data12 : entity xil_defaultlib.sysgen_delay_322b3c7cd0 
  port map (
    clr => '0',
    d => data64,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data12_q_net
  );
  delay_data13 : entity xil_defaultlib.sysgen_delay_f5ff6613e3 
  port map (
    clr => '0',
    d => delay_data12_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data13_q_net
  );
  delay_data14 : entity xil_defaultlib.sysgen_delay_f5ff6613e3 
  port map (
    clr => '0',
    d => delay_data13_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data14_q_net
  );
  delay_data15 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => done,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data15_q_net
  );
  delay_data16 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_data15_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data16_q_net
  );
  delay_data17 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_data16_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data17_q_net
  );
  delay_data18 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => overflow,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data18_q_net
  );
  delay_data19 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_data18_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data19_q_net
  );
  delay_data20 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_data19_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => out_overflow
  );
  delay_data23 : entity xil_defaultlib.sysgen_delay_f5ff6613e3 
  port map (
    clr => '0',
    d => mainmux2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data23_q_net
  );
  delay_data25 : entity xil_defaultlib.sysgen_delay_5c9b8add68 
  port map (
    clr => '0',
    d => register1_q_net_x3,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data25_q_net
  );
  delay_data26 : entity xil_defaultlib.sysgen_delay_5c9b8add68 
  port map (
    clr => '0',
    d => delay_data25_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data26_q_net
  );
  delay_data5 : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => data64_valid,
    clk => a_clk_net,
    ce => a_ce_net,
    q => wr_data
  );
  delay_data6 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => data64_rdy,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data6_q_net
  );
  delay_data7 : entity xil_defaultlib.sysgen_delay_815e992507 
  port map (
    clr => '0',
    d => data64_rdy,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data7_q_net
  );
  delay_data8 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => logical7_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_data8_q_net
  );
  delay_data9 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay_data8_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => out_done_x0
  );
  hdra_1 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => heap_id,
    dout => hdr_heap_id_x0
  );
  hdra_2 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => heap_size,
    dout => hdr_heap_size_x0
  );
  hdra_3 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => heap_offset,
    dout => hdr_heap_offset_x0
  );
  hdra_4 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => convert1_dout_net,
    dout => hdr_pkt_len_words_x0
  );
  hdra_5 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => x0x1600,
    dout => hdr5_0x1600_dir_x0
  );
  hdra_6 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => x0x4101,
    dout => hdr6_0x4101_dir_x0
  );
  hdra_7 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => x0x4103,
    dout => hdr7_0x4103_dir_x0
  );
  hdra_8 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => nwords_packet_size5_op_net,
    dout => hdr8_0x4300_x0
  );
  header_4cast4 : entity xil_defaultlib.rfsoc_byp_xlconvert_pipeline 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 48,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 42,
    latency => 1,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => hdr_pkt_len_words_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => header_4cast4_dout_net
  );
  header_4cat4 : entity xil_defaultlib.sysgen_concat_6327cd6613 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => header_4cast4_dout_net,
    in1 => header_4const4_op_net,
    y => header_4cat4_y_net
  );
  header_4const1 : entity xil_defaultlib.sysgen_constant_0624f47849 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => header_4const1_op_net
  );
  header_4const4 : entity xil_defaultlib.sysgen_constant_e6ce18a5c7 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => header_4const4_op_net
  );
  header_assert1 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat1_y_net,
    dout => header_assert1_dout_net
  );
  header_assert2 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat2_y_net,
    dout => header_assert2_dout_net
  );
  header_assert3 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat3_y_net,
    dout => header_assert3_dout_net
  );
  header_assert4 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat4_y_net,
    dout => header_assert4_dout_net
  );
  header_assert5 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat5_y_net,
    dout => header_assert5_dout_net
  );
  header_assert6 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat6_y_net,
    dout => header_assert6_dout_net
  );
  header_assert7 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat7_y_net,
    dout => header_assert7_dout_net
  );
  header_assert8 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat8_y_net,
    dout => header_assert8_dout_net
  );
  header_assert9 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 64,
    dout_width => 64
  )
  port map (
    din => header_cat9_y_net,
    dout => header_assert9_dout_net
  );
  header_cat1 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid1,
    in1 => hdr_heap_id_x0,
    y => header_cat1_y_net
  );
  header_cat2 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid2,
    in1 => hdr_heap_size_x0,
    y => header_cat2_y_net
  );
  header_cat3 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid3,
    in1 => hdr_heap_offset_x0,
    y => header_cat3_y_net
  );
  header_cat4 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid4,
    in1 => header_4cat4_y_net,
    y => header_cat4_y_net
  );
  header_cat5 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid5,
    in1 => hdr5_0x1600_dir_x0,
    y => header_cat5_y_net
  );
  header_cat6 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid6,
    in1 => hdr6_0x4101_dir_x0,
    y => header_cat6_y_net
  );
  header_cat7 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid7,
    in1 => hdr7_0x4103_dir_x0,
    y => header_cat7_y_net
  );
  header_cat8 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid8_x0,
    in1 => hdr8_0x4300_x0,
    y => header_cat8_y_net
  );
  header_cat9 : entity xil_defaultlib.sysgen_concat_8a83f22f67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => spid8,
    in1 => header_4const1_op_net,
    y => header_cat9_y_net
  );
  header_const1 : entity xil_defaultlib.sysgen_constant_bfa9fc6401 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid1
  );
  header_const2 : entity xil_defaultlib.sysgen_constant_4dd1f69d62 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid2
  );
  header_const3 : entity xil_defaultlib.sysgen_constant_7492875c7c 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid3
  );
  header_const4 : entity xil_defaultlib.sysgen_constant_a9ef6e14ce 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid4
  );
  header_const5 : entity xil_defaultlib.sysgen_constant_8c09a569ee 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid5
  );
  header_const6 : entity xil_defaultlib.sysgen_constant_f8bc021abd 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid6
  );
  header_const7 : entity xil_defaultlib.sysgen_constant_90e93afa11 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid7
  );
  header_const8 : entity xil_defaultlib.sysgen_constant_d90c968da2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid8_x0
  );
  header_const9 : entity xil_defaultlib.sysgen_constant_c6e59c8494 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => spid8
  );
  heap_addr_width : entity xil_defaultlib.sysgen_constant_3649785306 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => heap_addr_width_op_net
  );
  item_id_width : entity xil_defaultlib.sysgen_constant_1df6701ba3 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => item_id_width_op_net
  );
  mainmux1 : entity xil_defaultlib.sysgen_mux_9c73860df5 
  port map (
    clr => '0',
    sel => delay_data11_q_net,
    d0 => delay_data14_q_net,
    d1 => delay_data23_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    y => out_data
  );
  mainmux2 : entity xil_defaultlib.sysgen_mux_9c73860df5 
  port map (
    clr => '0',
    sel => delay_data7_q_net,
    d0 => assert_data2_dout_net,
    d1 => assert_data1_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    y => mainmux2_y_net
  );
  num_item_pts : entity xil_defaultlib.sysgen_constant_2a575d05d4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => num_item_pts_op_net
  );
  only_one1 : entity xil_defaultlib.sysgen_constant_195640b7dc 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => only_one1_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline1
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline1_x0 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline1_x0;
architecture structural of rfsoc_byp_pipeline1_x0 is 
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal assert1_dout_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  assert1_dout_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => assert1_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline19
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline19 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline19;
architecture structural of rfsoc_byp_pipeline19 is 
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline2 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline2;
architecture structural of rfsoc_byp_pipeline2 is 
  signal shift1_op_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  shift1_op_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => shift1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline21
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline21_x0 is
  port (
    d : in std_logic_vector( 48-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 48-1 downto 0 )
  );
end rfsoc_byp_pipeline21_x0;
architecture structural of rfsoc_byp_pipeline21_x0 is 
  signal register1_q_net_x0 : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 48-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline22
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline22_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline22_x0;
architecture structural of rfsoc_byp_pipeline22_x0 is 
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline24
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline24_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline24_x0;
architecture structural of rfsoc_byp_pipeline24_x0 is 
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline28
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline28_x0 is
  port (
    d : in std_logic_vector( 6-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 6-1 downto 0 )
  );
end rfsoc_byp_pipeline28_x0;
architecture structural of rfsoc_byp_pipeline28_x0 is 
  signal register1_q_net : std_logic_vector( 6-1 downto 0 );
  signal nbytes_packet_size_y_net : std_logic_vector( 6-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 6-1 downto 0 );
begin
  q <= register1_q_net;
  nbytes_packet_size_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 6,
    init_value => b"000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => nbytes_packet_size_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 6,
    init_value => b"000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline29
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline29 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline29;
architecture structural of rfsoc_byp_pipeline29 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal assert6_dout_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  assert6_dout_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => assert6_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline31
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline31_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline31_x0;
architecture structural of rfsoc_byp_pipeline31_x0 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal assert_dout_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  assert_dout_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => assert_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline32
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline32_x0 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline32_x0;
architecture structural of rfsoc_byp_pipeline32_x0 is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  addsub1_s_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => addsub1_s_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline34
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline34_x0 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline34_x0;
architecture structural of rfsoc_byp_pipeline34_x0 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal assert2_dout_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  assert2_dout_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => assert2_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline37
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline37_x0 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline37_x0;
architecture structural of rfsoc_byp_pipeline37_x0 is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal changing_bits_y_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  changing_bits_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => changing_bits_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline38
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline38 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline38;
architecture structural of rfsoc_byp_pipeline38 is 
  signal register3_q_net : std_logic_vector( 512-1 downto 0 );
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register3_q_net;
  reinterpret_out_output_port_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => reinterpret_out_output_port_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  register3 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register3_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline39
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline39 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline39;
architecture structural of rfsoc_byp_pipeline39 is 
  signal register3_q_net : std_logic_vector( 1-1 downto 0 );
  signal out_done : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register3_q_net;
  out_done <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => out_done,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  register3 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register3_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline4 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline4;
architecture structural of rfsoc_byp_pipeline4 is 
  signal register1_q_net_x0 : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline42
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline42 is
  port (
    d : in std_logic_vector( 48-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 48-1 downto 0 )
  );
end rfsoc_byp_pipeline42;
architecture structural of rfsoc_byp_pipeline42 is 
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal assert8_dout_net : std_logic_vector( 48-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 48-1 downto 0 );
begin
  q <= register1_q_net;
  assert8_dout_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => assert8_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline43
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline43 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline43;
architecture structural of rfsoc_byp_pipeline43 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal assert7_dout_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  assert7_dout_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => assert7_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline44
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline44 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline44;
architecture structural of rfsoc_byp_pipeline44 is 
  signal register3_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_data26_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register3_q_net;
  delay_data26_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_data26_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  register3 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register3_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline5
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline5_x0 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline5_x0;
architecture structural of rfsoc_byp_pipeline5_x0 is 
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0/pipeline6
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline6_x0 is
  port (
    d : in std_logic_vector( 48-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 48-1 downto 0 )
  );
end rfsoc_byp_pipeline6_x0;
architecture structural of rfsoc_byp_pipeline6_x0 is 
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 48-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 48-1 downto 0 );
begin
  q <= register1_q_net;
  constant1_op_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => constant1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pack0
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pack0 is
  port (
    sync_in : in std_logic_vector( 1-1 downto 0 );
    en : in std_logic_vector( 1-1 downto 0 );
    din : in std_logic_vector( 512-1 downto 0 );
    timestamp : in std_logic_vector( 48-1 downto 0 );
    board_id : in std_logic_vector( 8-1 downto 0 );
    base_ip : in std_logic_vector( 32-1 downto 0 );
    pkt_len_words : in std_logic_vector( 32-1 downto 0 );
    pkt_per_heap : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    data0 : out std_logic_vector( 512-1 downto 0 );
    valid0 : out std_logic_vector( 1-1 downto 0 );
    eof0 : out std_logic_vector( 1-1 downto 0 );
    destination_ip0 : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pack0;
architecture structural of rfsoc_byp_pack0 is 
  signal register3_q_net : std_logic_vector( 512-1 downto 0 );
  signal register3_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal eof64_0 : std_logic_vector( 1-1 downto 0 );
  signal register3_q_net_x1 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x15 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x19 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x20 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x22 : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net_x17 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x18 : std_logic_vector( 32-1 downto 0 );
  signal shift1_op_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x21 : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal edge_op_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x10 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x28 : std_logic_vector( 512-1 downto 0 );
  signal nbytes_packet_size_y_net : std_logic_vector( 6-1 downto 0 );
  signal addsub1_s_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x7 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x14 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x27 : std_logic_vector( 32-1 downto 0 );
  signal reinterpret_out_output_port_net : std_logic_vector( 512-1 downto 0 );
  signal out_done : std_logic_vector( 1-1 downto 0 );
  signal out_done_x0 : std_logic_vector( 1-1 downto 0 );
  signal out_overflow : std_logic_vector( 1-1 downto 0 );
  signal delay_data26_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x25 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal heap_id : std_logic_vector( 48-1 downto 0 );
  signal heap_size : std_logic_vector( 48-1 downto 0 );
  signal heap_offset : std_logic_vector( 48-1 downto 0 );
  signal convert1_dout_net : std_logic_vector( 48-1 downto 0 );
  signal x0x1600 : std_logic_vector( 48-1 downto 0 );
  signal x0x4101 : std_logic_vector( 48-1 downto 0 );
  signal x0x4103 : std_logic_vector( 48-1 downto 0 );
  signal nwords_packet_size5_op_net : std_logic_vector( 48-1 downto 0 );
  signal ip : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x13 : std_logic_vector( 32-1 downto 0 );
  signal assert1_dout_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x9 : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x8 : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x2 : std_logic_vector( 6-1 downto 0 );
  signal assert6_dout_net : std_logic_vector( 1-1 downto 0 );
  signal assert_dout_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x3 : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x4 : std_logic_vector( 8-1 downto 0 );
  signal assert2_dout_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x6 : std_logic_vector( 32-1 downto 0 );
  signal changing_bits_y_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x26 : std_logic_vector( 48-1 downto 0 );
  signal assert8_dout_net : std_logic_vector( 48-1 downto 0 );
  signal assert7_dout_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x23 : std_logic_vector( 48-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 48-1 downto 0 );
  signal assert5_dout_net : std_logic_vector( 48-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay15_q_net : std_logic_vector( 1-1 downto 0 );
  signal concat1_y_net : std_logic_vector( 46-1 downto 0 );
  signal constant_op_net : std_logic_vector( 8-1 downto 0 );
  signal mult1_p_net : std_logic_vector( 32-1 downto 0 );
  signal logical_y_net : std_logic_vector( 1-1 downto 0 );
  signal delay18_q_net : std_logic_vector( 1-1 downto 0 );
  signal eof64_pre : std_logic_vector( 1-1 downto 0 );
  signal relational_op_net : std_logic_vector( 1-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal shift_op_net : std_logic_vector( 32-1 downto 0 );
  signal fcount : std_logic_vector( 7-1 downto 0 );
begin
  data0 <= register3_q_net;
  valid0 <= register3_q_net_x0;
  eof0 <= eof64_0;
  destination_ip0 <= register3_q_net_x1;
  register1_q_net_x15 <= sync_in;
  register1_q_net_x19 <= en;
  register1_q_net_x20 <= din;
  register1_q_net_x22 <= timestamp;
  register1_q_net_x17 <= board_id;
  register1_q_net_x18 <= base_ip;
  shift1_op_net <= pkt_len_words;
  register1_q_net_x21 <= pkt_per_heap;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  edge_detect1_x0 : entity xil_defaultlib.rfsoc_byp_edge_detect1 
  port map (
    in_x0 => register1_q_net_x10,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net_x0
  );
  pack_ch : entity xil_defaultlib.rfsoc_byp_pack_ch 
  port map (
    sync_in => register1_q_net_x7,
    eof0 => eof64_0,
    din => register1_q_net_x14,
    base_ip => register1_q_net_x27,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    din0 => register1_q_net_x28,
    x_base_chan => nbytes_packet_size_y_net,
    dest_ip0 => addsub1_s_net
  );
  pack_deng : entity xil_defaultlib.rfsoc_byp_pack_deng 
  port map (
    data256_ready => register1_q_net_x25,
    data256_valid => register1_q_net_x1,
    data256 => register1_q_net,
    hdr_heap_id => heap_id,
    hdr_heap_size => heap_size,
    hdr_heap_offset => heap_offset,
    hdr_pkt_len_words => convert1_dout_net,
    hdr5_0x1600_dir => x0x1600,
    hdr6_0x4101_dir => x0x4101,
    hdr7_0x4103_dir => x0x4103,
    hdr8_0x4300 => nwords_packet_size5_op_net,
    ip_in => ip,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    data_out => reinterpret_out_output_port_net,
    valid_out => out_done,
    eof => out_done_x0,
    data_overflow => out_overflow,
    ip_out => delay_data26_q_net
  );
  pipeline1_x0 : entity xil_defaultlib.rfsoc_byp_pipeline1_x0 
  port map (
    d => assert1_dout_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x13
  );
  pipeline19 : entity xil_defaultlib.rfsoc_byp_pipeline19 
  port map (
    d => register1_q_net_x19,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x10
  );
  pipeline2_x0 : entity xil_defaultlib.rfsoc_byp_pipeline2 
  port map (
    d => shift1_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x9
  );
  pipeline21 : entity xil_defaultlib.rfsoc_byp_pipeline21_x0 
  port map (
    d => register1_q_net_x22,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x8
  );
  pipeline22 : entity xil_defaultlib.rfsoc_byp_pipeline22_x0 
  port map (
    d => register1_q_net_x15,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x7
  );
  pipeline24 : entity xil_defaultlib.rfsoc_byp_pipeline24_x0 
  port map (
    d => register1_q_net_x17,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x0
  );
  pipeline28 : entity xil_defaultlib.rfsoc_byp_pipeline28_x0 
  port map (
    d => nbytes_packet_size_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x2
  );
  pipeline29 : entity xil_defaultlib.rfsoc_byp_pipeline29 
  port map (
    d => assert6_dout_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x1
  );
  pipeline31 : entity xil_defaultlib.rfsoc_byp_pipeline31_x0 
  port map (
    d => assert_dout_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net
  );
  pipeline32 : entity xil_defaultlib.rfsoc_byp_pipeline32_x0 
  port map (
    d => addsub1_s_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x3
  );
  pipeline34 : entity xil_defaultlib.rfsoc_byp_pipeline34_x0 
  port map (
    d => assert2_dout_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x4
  );
  pipeline37 : entity xil_defaultlib.rfsoc_byp_pipeline37_x0 
  port map (
    d => changing_bits_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x6
  );
  pipeline38_x0 : entity xil_defaultlib.rfsoc_byp_pipeline38 
  port map (
    d => reinterpret_out_output_port_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register3_q_net
  );
  pipeline39_x0 : entity xil_defaultlib.rfsoc_byp_pipeline39 
  port map (
    d => out_done,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register3_q_net_x0
  );
  pipeline4 : entity xil_defaultlib.rfsoc_byp_pipeline4 
  port map (
    d => register1_q_net_x18,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x27
  );
  pipeline42 : entity xil_defaultlib.rfsoc_byp_pipeline42 
  port map (
    d => assert8_dout_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x26
  );
  pipeline43 : entity xil_defaultlib.rfsoc_byp_pipeline43 
  port map (
    d => assert7_dout_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x25
  );
  pipeline44 : entity xil_defaultlib.rfsoc_byp_pipeline44 
  port map (
    d => delay_data26_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register3_q_net_x1
  );
  pipeline5 : entity xil_defaultlib.rfsoc_byp_pipeline5_x0 
  port map (
    d => register1_q_net_x20,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x14
  );
  pipeline6 : entity xil_defaultlib.rfsoc_byp_pipeline6_x0 
  port map (
    d => constant1_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x23
  );
  assert_x0 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 512,
    dout_width => 512
  )
  port map (
    din => register1_q_net_x28,
    dout => assert_dout_net
  );
  assert1 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 32,
    dout_width => 32
  )
  port map (
    din => register1_q_net_x9,
    dout => assert1_dout_net
  );
  assert2 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 8,
    dout_width => 8
  )
  port map (
    din => register1_q_net_x0,
    dout => assert2_dout_net
  );
  assert5 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => register1_q_net_x23,
    dout => assert5_dout_net
  );
  assert6 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 1,
    dout_width => 1
  )
  port map (
    din => delay4_q_net,
    dout => assert6_dout_net
  );
  assert7 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 1,
    dout_width => 1
  )
  port map (
    din => delay15_q_net,
    dout => assert7_dout_net
  );
  assert8 : entity xil_defaultlib.xlpassthrough 
  generic map (
    din_width => 48,
    dout_width => 48
  )
  port map (
    din => register1_q_net_x8,
    dout => assert8_dout_net
  );
  concat1 : entity xil_defaultlib.sysgen_concat_a812e9db0f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => register1_q_net_x6,
    in1 => nbytes_packet_size_y_net,
    in2 => register1_q_net_x0,
    y => concat1_y_net
  );
  constant_x0 : entity xil_defaultlib.sysgen_constant_ec2d4bcc10 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant_op_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_0624f47849 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  convert1 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 32,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => register1_q_net_x13,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => convert1_dout_net
  );
  convert10 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 46,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => concat1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => heap_id
  );
  convert15 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 48,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => register1_q_net_x26,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => x0x1600
  );
  convert16 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 32,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => mult1_p_net,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => heap_size
  );
  convert17 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 32,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 8,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => register1_q_net_x3,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => ip
  );
  convert5 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 8,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => register1_q_net_x4,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => x0x4101
  );
  convert8 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 48,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => assert5_dout_net,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => heap_offset
  );
  convert9 : entity xil_defaultlib.rfsoc_byp_xlconvert 
  generic map (
    bool_conversion => 0,
    din_arith => 1,
    din_bin_pt => 0,
    din_width => 6,
    dout_arith => 1,
    dout_bin_pt => 0,
    dout_width => 48,
    latency => 0,
    overflow => xlWrap,
    quantization => xlTruncate
  )
  port map (
    clr => '0',
    en => "1",
    din => register1_q_net_x2,
    clk => a_clk_net,
    ce => a_ce_net,
    dout => x0x4103
  );
  delay15 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => logical_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay15_q_net
  );
  delay18 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => register1_q_net_x10,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay18_q_net
  );
  delay2 : entity xil_defaultlib.sysgen_delay_502d31f8d1 
  port map (
    clr => '0',
    d => eof64_pre,
    clk => a_clk_net,
    ce => a_ce_net,
    q => eof64_0
  );
  delay4 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay18_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay4_q_net
  );
  logical : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => relational_op_net,
    d1 => register1_q_net_x10,
    y => logical_y_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => register1_q_net_x7,
    d1 => edge_op_y_net_x0,
    y => logical2_y_net
  );
  logical4 : entity xil_defaultlib.sysgen_logical_a894f814d3 
  port map (
    clr => '0',
    d0 => out_done,
    d1 => out_done_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    y => eof64_pre
  );
  mult1 : entity xil_defaultlib.rfsoc_byp_xlmult 
  generic map (
    a_arith => xlUnsigned,
    a_bin_pt => 0,
    a_width => 8,
    b_arith => xlUnsigned,
    b_bin_pt => 0,
    b_width => 32,
    c_a_type => 1,
    c_a_width => 8,
    c_b_type => 1,
    c_b_width => 32,
    c_baat => 8,
    c_output_width => 40,
    c_type => 1,
    core_name0 => "rfsoc_byp_mult_gen_v12_0_i1",
    extra_registers => 0,
    multsign => 1,
    overflow => 1,
    p_arith => xlUnsigned,
    p_bin_pt => 0,
    p_width => 32,
    quantization => 1
  )
  port map (
    clr => '0',
    core_clr => '1',
    en => "1",
    rst => "0",
    a => register1_q_net_x21,
    b => shift_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    core_clk => a_clk_net,
    core_ce => a_ce_net,
    p => mult1_p_net
  );
  relational : entity xil_defaultlib.sysgen_relational_083246d012 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => fcount,
    b => constant_op_net,
    op => relational_op_net
  );
  shift : entity xil_defaultlib.sysgen_shift_7251bb924c 
  port map (
    clr => '0',
    ip => register1_q_net_x9,
    clk => a_clk_net,
    ce => a_ce_net,
    op => shift_op_net
  );
  changing_bits : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 16,
    new_msb => 47,
    x_width => 48,
    y_width => 32
  )
  port map (
    x => assert8_dout_net,
    y => changing_bits_y_net
  );
  count : entity xil_defaultlib.sysgen_counter_4f7b972113 
  port map (
    clr => '0',
    rst => logical2_y_net,
    en => register1_q_net_x10,
    clk => a_clk_net,
    ce => a_ce_net,
    op => fcount
  );
  nwords_packet_size5 : entity xil_defaultlib.sysgen_constant_0624f47849 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => nwords_packet_size5_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline10
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline10 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline10;
architecture structural of rfsoc_byp_pipeline10 is 
  signal register2_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay17_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register2_q_net;
  delay17_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay17_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline11
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline11 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline11;
architecture structural of rfsoc_byp_pipeline11 is 
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline12
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline12 is
  port (
    d : in std_logic_vector( 48-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 48-1 downto 0 )
  );
end rfsoc_byp_pipeline12;
architecture structural of rfsoc_byp_pipeline12 is 
  signal register1_q_net_x0 : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 48-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline121
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline121 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline121;
architecture structural of rfsoc_byp_pipeline121 is 
  signal register1_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net_x0;
  register1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x0
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline13
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline13_x0 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline13_x0;
architecture structural of rfsoc_byp_pipeline13_x0 is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal time_in_lo_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  time_in_lo_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => time_in_lo_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline14
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline14 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline14;
architecture structural of rfsoc_byp_pipeline14 is 
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay6_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register1_q_net;
  delay6_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay6_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline15
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline15 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline15;
architecture structural of rfsoc_byp_pipeline15 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal slice1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  slice1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline16
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline16 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline16;
architecture structural of rfsoc_byp_pipeline16 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline166
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline166 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline166;
architecture structural of rfsoc_byp_pipeline166 is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal control_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  control_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => control_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline17
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline17 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline17;
architecture structural of rfsoc_byp_pipeline17 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concat2_y_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  concat2_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => concat2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline174
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline174 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline174;
architecture structural of rfsoc_byp_pipeline174 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline175
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline175 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline175;
architecture structural of rfsoc_byp_pipeline175 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  delay3_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay3_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline176
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline176 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline176;
architecture structural of rfsoc_byp_pipeline176 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline177
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline177 is
  port (
    d : in std_logic_vector( 48-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 48-1 downto 0 )
  );
end rfsoc_byp_pipeline177;
architecture structural of rfsoc_byp_pipeline177 is 
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal counter_op_net : std_logic_vector( 48-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 48-1 downto 0 );
begin
  q <= register1_q_net;
  counter_op_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => counter_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline18
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline18_x0 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline18_x0;
architecture structural of rfsoc_byp_pipeline18_x0 is 
  signal register2_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay15_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register2_q_net;
  delay15_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay15_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline188
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline188 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline188;
architecture structural of rfsoc_byp_pipeline188 is 
  signal register2_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register2_q_net;
  register1_q_net_x0 <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline19
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline19_x1 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline19_x1;
architecture structural of rfsoc_byp_pipeline19_x1 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concat3_y_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  concat3_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => concat3_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline20
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline20 is
  port (
    d : in std_logic_vector( 16-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 16-1 downto 0 )
  );
end rfsoc_byp_pipeline20;
architecture structural of rfsoc_byp_pipeline20 is 
  signal register1_q_net : std_logic_vector( 16-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 16-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 16-1 downto 0 );
begin
  q <= register1_q_net;
  slice2_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 16,
    init_value => b"0000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 16,
    init_value => b"0000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline21
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline21_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline21_x1;
architecture structural of rfsoc_byp_pipeline21_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  slice2_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline22
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline22_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline22_x1;
architecture structural of rfsoc_byp_pipeline22_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline23
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline23_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline23_x0;
architecture structural of rfsoc_byp_pipeline23_x0 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical3_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical3_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline24
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline24_x1 is
  port (
    d : in std_logic_vector( 48-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 48-1 downto 0 )
  );
end rfsoc_byp_pipeline24_x1;
architecture structural of rfsoc_byp_pipeline24_x1 is 
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal register8_q_net : std_logic_vector( 48-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 48-1 downto 0 );
begin
  q <= register1_q_net;
  register8_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register8_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline25
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline25_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline25_x0;
architecture structural of rfsoc_byp_pipeline25_x0 is 
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register2_q_net;
  logical3_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical3_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline3
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline3 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline3;
architecture structural of rfsoc_byp_pipeline3 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal slice101_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  slice101_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice101_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline32
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline32_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline32_x1;
architecture structural of rfsoc_byp_pipeline32_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline33
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline33_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline33_x0;
architecture structural of rfsoc_byp_pipeline33_x0 is 
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter2_op_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register2_q_net;
  inverter2_op_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => inverter2_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline36
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline36 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline36;
architecture structural of rfsoc_byp_pipeline36 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  register1_q_net_x0 <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline37
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline37_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline37_x1;
architecture structural of rfsoc_byp_pipeline37_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  register1_q_net_x0 <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline4
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline4_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline4_x1;
architecture structural of rfsoc_byp_pipeline4_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal slice101_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  slice101_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice101_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline5
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline5_x2 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline5_x2;
architecture structural of rfsoc_byp_pipeline5_x2 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline6
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline6_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline6_x1;
architecture structural of rfsoc_byp_pipeline6_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline60
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline60 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline60;
architecture structural of rfsoc_byp_pipeline60 is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal iptx_base_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  iptx_base_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => iptx_base_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline62
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline62 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline62;
architecture structural of rfsoc_byp_pipeline62 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  slice3_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice3_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline67
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline67 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline67;
architecture structural of rfsoc_byp_pipeline67 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  register0_q_net_x0 <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline7
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline7_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline7_x0;
architecture structural of rfsoc_byp_pipeline7_x0 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline8
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline8_x1 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline8_x1;
architecture structural of rfsoc_byp_pipeline8_x1 is 
  signal register2_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay7_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register2_q_net;
  delay7_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay7_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline9
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline9 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline9;
architecture structural of rfsoc_byp_pipeline9 is 
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal concat1_y_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register1_q_net;
  concat1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => concat1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline91
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline91 is
  port (
    d : in std_logic_vector( 32-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 32-1 downto 0 )
  );
end rfsoc_byp_pipeline91;
architecture structural of rfsoc_byp_pipeline91 is 
  signal register1_q_net : std_logic_vector( 32-1 downto 0 );
  signal tx_pkt_bytes_net : std_logic_vector( 32-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register1_q_net;
  tx_pkt_bytes_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => tx_pkt_bytes_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 32,
    init_value => b"00000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/pipeline95
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline95 is
  port (
    d : in std_logic_vector( 8-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 8-1 downto 0 )
  );
end rfsoc_byp_pipeline95;
architecture structural of rfsoc_byp_pipeline95 is 
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal slice11_y_net : std_logic_vector( 8-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 8-1 downto 0 );
begin
  q <= register1_q_net;
  slice11_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => slice11_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/edge_detect2
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_edge_detect2_x1 is
  port (
    in_x0 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    out_x0 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_edge_detect2_x1;
architecture structural of rfsoc_byp_edge_detect2_x1 is 
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational2_op_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter_op_net : std_logic_vector( 1-1 downto 0 );
begin
  out_x0 <= edge_op_y_net;
  relational2_op_net <= in_x0;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => relational2_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  inverter : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => relational2_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter_op_net
  );
  edge_op : entity xil_defaultlib.sysgen_logical_ac59be6314 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter_op_net,
    d1 => delay_q_net,
    y => edge_op_y_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline15
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline15_x0 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline15_x0;
architecture structural of rfsoc_byp_pipeline15_x0 is 
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay6_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register1_q_net;
  delay6_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay6_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline21
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline21_x2 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline21_x2;
architecture structural of rfsoc_byp_pipeline21_x2 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline22
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline22_x2 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline22_x2;
architecture structural of rfsoc_byp_pipeline22_x2 is 
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay7_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register1_q_net;
  delay7_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay7_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline23
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline23_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline23_x1;
architecture structural of rfsoc_byp_pipeline23_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay10_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay10_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay10_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline24
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline24_x2 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline24_x2;
architecture structural of rfsoc_byp_pipeline24_x2 is 
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay17_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register1_q_net;
  delay17_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay17_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline25
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline25_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline25_x1;
architecture structural of rfsoc_byp_pipeline25_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay12_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay12_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay12_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline26
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline26_x0 is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 128-1 downto 0 )
  );
end rfsoc_byp_pipeline26_x0;
architecture structural of rfsoc_byp_pipeline26_x0 is 
  signal register1_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay15_q_net : std_logic_vector( 128-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 128-1 downto 0 );
begin
  q <= register1_q_net;
  delay15_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay15_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline27
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline27_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline27_x0;
architecture structural of rfsoc_byp_pipeline27_x0 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay13_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay13_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay13_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline28
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline28_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline28_x1;
architecture structural of rfsoc_byp_pipeline28_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay1_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline29
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline29_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline29_x1;
architecture structural of rfsoc_byp_pipeline29_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay10_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay10_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay10_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline30
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline30_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline30_x0;
architecture structural of rfsoc_byp_pipeline30_x0 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay12_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay12_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay12_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline31
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline31_x1 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline31_x1;
architecture structural of rfsoc_byp_pipeline31_x1 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay13_q_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  delay13_q_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => delay13_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline34
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline34_x1 is
  port (
    d : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 )
  );
end rfsoc_byp_pipeline34_x1;
architecture structural of rfsoc_byp_pipeline34_x1 is 
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal concat4_y_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register1_q_net : std_logic_vector( 512-1 downto 0 );
  signal register0_q_net : std_logic_vector( 512-1 downto 0 );
begin
  q <= register2_q_net;
  concat4_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => concat4_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline35
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline35_x0 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline35_x0;
architecture structural of rfsoc_byp_pipeline35_x0 is 
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register2_q_net;
  logical2_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline40
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline40 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline40;
architecture structural of rfsoc_byp_pipeline40 is 
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register1_q_net;
  logical1_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap/pipeline41
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_pipeline41 is
  port (
    d : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_pipeline41;
architecture structural of rfsoc_byp_pipeline41 is 
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
begin
  q <= register2_q_net;
  edge_op_y_net <= d;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  register0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => edge_op_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register0_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    en => "1",
    rst => "0",
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/snap
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_snap is
  port (
    d : in std_logic_vector( 128-1 downto 0 );
    d1 : in std_logic_vector( 1-1 downto 0 );
    d2 : in std_logic_vector( 128-1 downto 0 );
    d3 : in std_logic_vector( 1-1 downto 0 );
    d4 : in std_logic_vector( 128-1 downto 0 );
    d5 : in std_logic_vector( 1-1 downto 0 );
    d6 : in std_logic_vector( 128-1 downto 0 );
    d7 : in std_logic_vector( 1-1 downto 0 );
    d12 : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    q : out std_logic_vector( 512-1 downto 0 );
    q1 : out std_logic_vector( 1-1 downto 0 );
    q2 : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_snap;
architecture structural of rfsoc_byp_snap is 
  signal register2_q_net : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal delay6_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay7_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay10_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay17_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay12_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay15_q_net : std_logic_vector( 128-1 downto 0 );
  signal delay13_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal edge_op_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational2_op_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x8 : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x7 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x6 : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x9 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x10 : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x11 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x4 : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x3 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal concat4_y_net : std_logic_vector( 512-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x5 : std_logic_vector( 1-1 downto 0 );
  signal register8_q_net : std_logic_vector( 128-1 downto 0 );
  signal register9_q_net : std_logic_vector( 128-1 downto 0 );
  signal register11_q_net : std_logic_vector( 128-1 downto 0 );
  signal register13_q_net : std_logic_vector( 128-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 9-1 downto 0 );
  signal counter_op_net : std_logic_vector( 32-1 downto 0 );
  signal wr_pkt2_op_net : std_logic_vector( 32-1 downto 0 );
begin
  q <= register2_q_net;
  q1 <= register2_q_net_x0;
  q2 <= register2_q_net_x1;
  delay6_q_net <= d;
  delay1_q_net <= d1;
  delay7_q_net <= d2;
  delay10_q_net <= d3;
  delay17_q_net <= d4;
  delay12_q_net <= d5;
  delay15_q_net <= d6;
  delay13_q_net <= d7;
  logical1_y_net <= d12;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  edge_detect2 : entity xil_defaultlib.rfsoc_byp_edge_detect2_x1 
  port map (
    in_x0 => relational2_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net
  );
  pipeline15_x0 : entity xil_defaultlib.rfsoc_byp_pipeline15_x0 
  port map (
    d => delay6_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x8
  );
  pipeline21 : entity xil_defaultlib.rfsoc_byp_pipeline21_x2 
  port map (
    d => delay1_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x7
  );
  pipeline22 : entity xil_defaultlib.rfsoc_byp_pipeline22_x2 
  port map (
    d => delay7_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x6
  );
  pipeline23 : entity xil_defaultlib.rfsoc_byp_pipeline23_x1 
  port map (
    d => delay10_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x9
  );
  pipeline24 : entity xil_defaultlib.rfsoc_byp_pipeline24_x2 
  port map (
    d => delay17_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x10
  );
  pipeline25 : entity xil_defaultlib.rfsoc_byp_pipeline25_x1 
  port map (
    d => delay12_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x11
  );
  pipeline26 : entity xil_defaultlib.rfsoc_byp_pipeline26_x0 
  port map (
    d => delay15_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x4
  );
  pipeline27 : entity xil_defaultlib.rfsoc_byp_pipeline27_x0 
  port map (
    d => delay13_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net
  );
  pipeline28 : entity xil_defaultlib.rfsoc_byp_pipeline28_x1 
  port map (
    d => delay1_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x3
  );
  pipeline29 : entity xil_defaultlib.rfsoc_byp_pipeline29_x1 
  port map (
    d => delay10_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x2
  );
  pipeline30 : entity xil_defaultlib.rfsoc_byp_pipeline30_x0 
  port map (
    d => delay12_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x1
  );
  pipeline31 : entity xil_defaultlib.rfsoc_byp_pipeline31_x1 
  port map (
    d => delay13_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x0
  );
  pipeline34 : entity xil_defaultlib.rfsoc_byp_pipeline34_x1 
  port map (
    d => concat4_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net
  );
  pipeline35 : entity xil_defaultlib.rfsoc_byp_pipeline35_x0 
  port map (
    d => logical2_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x0
  );
  pipeline40_x0 : entity xil_defaultlib.rfsoc_byp_pipeline40 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x5
  );
  pipeline41_x0 : entity xil_defaultlib.rfsoc_byp_pipeline41 
  port map (
    d => edge_op_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x1
  );
  logical2 : entity xil_defaultlib.sysgen_logical_43f6507b99 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => register1_q_net_x3,
    d1 => register1_q_net_x2,
    d2 => register1_q_net_x1,
    d3 => register1_q_net_x0,
    y => logical2_y_net
  );
  register8 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => register1_q_net_x8,
    en => register1_q_net_x7,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register8_q_net
  );
  register9 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => register1_q_net_x6,
    en => register1_q_net_x9,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register9_q_net
  );
  register11 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => register1_q_net_x10,
    en => register1_q_net_x11,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register11_q_net
  );
  register13 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 128,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => register1_q_net_x4,
    en => register1_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register13_q_net
  );
  concat4 : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => register8_q_net,
    in1 => register9_q_net,
    in2 => register11_q_net,
    in3 => register13_q_net,
    y => concat4_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 8,
    x_width => 32,
    y_width => 9
  )
  port map (
    x => counter_op_net,
    y => slice3_y_net
  );
  counter : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i0",
    op_arith => xlUnsigned,
    op_width => 32
  )
  port map (
    clr => '0',
    rst => register1_q_net_x5,
    en => logical2_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter_op_net
  );
  relational2 : entity xil_defaultlib.sysgen_relational_4712faf8bf 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => slice3_y_net,
    b => wr_pkt2_op_net,
    op => relational2_op_net
  );
  wr_pkt2 : entity xil_defaultlib.sysgen_constant_13c3db9747 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => wr_pkt2_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp/time_gen
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_time_gen is
  port (
    wr_sync_in : in std_logic_vector( 1-1 downto 0 );
    wr_en : in std_logic_vector( 1-1 downto 0 );
    wr_val_in : in std_logic_vector( 48-1 downto 0 );
    rd_en : in std_logic_vector( 1-1 downto 0 );
    rd_misc_in : in std_logic_vector( 512-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    rd_sync_out : out std_logic_vector( 1-1 downto 0 );
    rd_dvalid : out std_logic_vector( 1-1 downto 0 );
    rd_val_out : out std_logic_vector( 48-1 downto 0 );
    rd_misc_out : out std_logic_vector( 512-1 downto 0 );
    heap_end : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_time_gen;
architecture structural of rfsoc_byp_time_gen is 
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal counter_op_net : std_logic_vector( 48-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 512-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 48-1 downto 0 );
  signal data_delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal mux0_y_net : std_logic_vector( 512-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal constant1_op_net : std_logic_vector( 9-1 downto 0 );
  signal inverter1_op_net : std_logic_vector( 1-1 downto 0 );
  signal register_q_net : std_logic_vector( 1-1 downto 0 );
  signal inverter2_op_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal logical_y_net : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal relational_op_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net : std_logic_vector( 48-1 downto 0 );
  signal period_op_net : std_logic_vector( 9-1 downto 0 );
begin
  rd_sync_out <= delay1_q_net;
  rd_dvalid <= delay_q_net;
  rd_val_out <= counter_op_net;
  rd_misc_out <= delay3_q_net;
  heap_end <= logical2_y_net;
  register0_q_net <= wr_sync_in;
  register0_q_net_x0 <= wr_en;
  register1_q_net_x0 <= wr_val_in;
  data_delay_q_net <= rd_en;
  mux0_y_net <= rd_misc_in;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  constant1 : entity xil_defaultlib.sysgen_constant_31d205c874 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  delay : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => data_delay_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay_q_net
  );
  delay1 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 1
  )
  port map (
    en => '1',
    rst => '0',
    d => register0_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay1_q_net
  );
  delay3 : entity xil_defaultlib.rfsoc_byp_xldelay 
  generic map (
    latency => 1,
    reg_retiming => 0,
    reset => 0,
    width => 512
  )
  port map (
    en => '1',
    rst => '0',
    d => mux0_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay3_q_net
  );
  inverter1 : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => register_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter1_op_net
  );
  inverter2 : entity xil_defaultlib.sysgen_inverter_851b644385 
  port map (
    clr => '0',
    ip => register2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter2_op_net
  );
  logical : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter1_op_net,
    d1 => register0_q_net_x0,
    y => logical_y_net
  );
  logical1 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => logical3_y_net,
    d1 => logical2_y_net,
    y => logical1_y_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_a894f814d3 
  port map (
    clr => '0',
    d0 => relational_op_net,
    d1 => data_delay_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    y => logical2_y_net
  );
  logical3 : entity xil_defaultlib.sysgen_logical_eb7712bfb2 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => inverter2_op_net,
    d1 => data_delay_q_net,
    y => logical3_y_net
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    d => register0_q_net_x0,
    rst => register0_q_net,
    en => register0_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => register1_q_net_x0,
    en => logical_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net
  );
  register2 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    d => data_delay_q_net,
    rst => register0_q_net,
    en => data_delay_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register2_q_net
  );
  relational : entity xil_defaultlib.sysgen_relational_508c11eb35 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    a => period_op_net,
    b => constant1_op_net,
    op => relational_op_net
  );
  counter : entity xil_defaultlib.sysgen_counter_d1bd53a790 
  port map (
    clr => '0',
    load => logical3_y_net,
    din => register1_q_net,
    en => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => counter_op_net
  );
  period : entity xil_defaultlib.rfsoc_byp_xlcounter_free 
  generic map (
    core_name0 => "rfsoc_byp_c_counter_binary_v12_0_i3",
    op_arith => xlUnsigned,
    op_width => 9
  )
  port map (
    clr => '0',
    rst => register0_q_net,
    en => data_delay_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => period_op_net
  );
end structural;
-- Generated from Simulink block rfsoc_byp_struct
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_struct is
  port (
    time_in_lo : in std_logic_vector( 32-1 downto 0 );
    control : in std_logic_vector( 32-1 downto 0 );
    m_axis_udp_tready : in std_logic_vector( 1-1 downto 0 );
    aresetn : in std_logic_vector( 1-1 downto 0 );
    s_axis_0_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_0_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_0_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_0_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_0_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_0_tvalid : in std_logic_vector( 1-1 downto 0 );
    irig_trig_in : in std_logic_vector( 1-1 downto 0 );
    s_axis_1_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_1_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_1_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_1_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_1_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_1_tvalid : in std_logic_vector( 1-1 downto 0 );
    s_axis_2_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_2_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_2_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_2_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_2_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_2_tvalid : in std_logic_vector( 1-1 downto 0 );
    s_axis_3_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_3_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_3_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_3_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_3_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_3_tvalid : in std_logic_vector( 1-1 downto 0 );
    irig_comp_in : in std_logic_vector( 1-1 downto 0 );
    iptx_base : in std_logic_vector( 32-1 downto 0 );
    tx_metadata : in std_logic_vector( 32-1 downto 0 );
    tx_pkt_bytes : in std_logic_vector( 32-1 downto 0 );
    tx_pkt_wait : in std_logic_vector( 32-1 downto 0 );
    x_setup : in std_logic_vector( 32-1 downto 0 );
    s_axis_snap_tdata : in std_logic_vector( 512-1 downto 0 );
    s_axis_snap_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_snap_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_snap_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_snap_tvalid : in std_logic_vector( 1-1 downto 0 );
    time_in_hi : in std_logic_vector( 32-1 downto 0 );
    m_axis_snap_tready : in std_logic_vector( 1-1 downto 0 );
    s_axis_udp_tdata : in std_logic_vector( 512-1 downto 0 );
    s_axis_udp_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_udp_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_udp_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_udp_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_udp_tvalid : in std_logic_vector( 1-1 downto 0 );
    clk_1 : in std_logic;
    ce_1 : in std_logic;
    intf_rst : out std_logic_vector( 3-1 downto 0 );
    m_axis_udp_tdata : out std_logic_vector( 512-1 downto 0 );
    m_axis_udp_tid : out std_logic_vector( 8-1 downto 0 );
    m_axis_udp_tkeep : out std_logic_vector( 64-1 downto 0 );
    m_axis_udp_tlast : out std_logic_vector( 1-1 downto 0 );
    m_axis_udp_tuser : out std_logic_vector( 32-1 downto 0 );
    m_axis_udp_tvalid : out std_logic_vector( 1-1 downto 0 );
    s_axis_0_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_1_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_2_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_3_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tready : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tdata : out std_logic_vector( 512-1 downto 0 );
    m_axis_snap_tid : out std_logic_vector( 16-1 downto 0 );
    m_axis_snap_tkeep : out std_logic_vector( 64-1 downto 0 );
    m_axis_snap_tlast : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tvalid : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tuser : out std_logic_vector( 32-1 downto 0 );
    s_axis_udp_tready : out std_logic_vector( 1-1 downto 0 )
  );
end rfsoc_byp_struct;
architecture structural of rfsoc_byp_struct is 
  signal time_in_lo_net : std_logic_vector( 32-1 downto 0 );
  signal control_net : std_logic_vector( 32-1 downto 0 );
  signal concat4_y_net : std_logic_vector( 3-1 downto 0 );
  signal register15_q_net : std_logic_vector( 512-1 downto 0 );
  signal register12_q_net : std_logic_vector( 8-1 downto 0 );
  signal concat2_y_net : std_logic_vector( 64-1 downto 0 );
  signal register3_q_net_x3 : std_logic_vector( 1-1 downto 0 );
  signal m_axis_udp_tready_net : std_logic_vector( 1-1 downto 0 );
  signal constant26_op_net : std_logic_vector( 32-1 downto 0 );
  signal register_q_net : std_logic_vector( 1-1 downto 0 );
  signal aresetn_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_0_tdata_net : std_logic_vector( 128-1 downto 0 );
  signal s_axis_0_tid_net : std_logic_vector( 8-1 downto 0 );
  signal s_axis_0_tkeep_net : std_logic_vector( 64-1 downto 0 );
  signal s_axis_0_tlast_net : std_logic_vector( 1-1 downto 0 );
  signal constant10_op_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_0_tuser_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_0_tvalid_net : std_logic_vector( 1-1 downto 0 );
  signal irig_trig_in_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_1_tdata_net : std_logic_vector( 128-1 downto 0 );
  signal s_axis_1_tid_net : std_logic_vector( 8-1 downto 0 );
  signal s_axis_1_tkeep_net : std_logic_vector( 64-1 downto 0 );
  signal s_axis_1_tlast_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_1_tuser_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_1_tvalid_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_2_tdata_net : std_logic_vector( 128-1 downto 0 );
  signal s_axis_2_tid_net : std_logic_vector( 8-1 downto 0 );
  signal s_axis_2_tkeep_net : std_logic_vector( 64-1 downto 0 );
  signal s_axis_2_tlast_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_2_tuser_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_2_tvalid_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_3_tdata_net : std_logic_vector( 128-1 downto 0 );
  signal s_axis_3_tid_net : std_logic_vector( 8-1 downto 0 );
  signal s_axis_3_tkeep_net : std_logic_vector( 64-1 downto 0 );
  signal s_axis_3_tlast_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_3_tuser_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_3_tvalid_net : std_logic_vector( 1-1 downto 0 );
  signal irig_comp_in_net : std_logic_vector( 1-1 downto 0 );
  signal iptx_base_net : std_logic_vector( 32-1 downto 0 );
  signal tx_metadata_net : std_logic_vector( 32-1 downto 0 );
  signal tx_pkt_bytes_net : std_logic_vector( 32-1 downto 0 );
  signal tx_pkt_wait_net : std_logic_vector( 32-1 downto 0 );
  signal x_setup_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_snap_tdata_net : std_logic_vector( 512-1 downto 0 );
  signal s_axis_snap_tid_net : std_logic_vector( 8-1 downto 0 );
  signal s_axis_snap_tkeep_net : std_logic_vector( 64-1 downto 0 );
  signal s_axis_snap_tlast_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_snap_tuser_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_snap_tvalid_net : std_logic_vector( 1-1 downto 0 );
  signal register6_q_net : std_logic_vector( 512-1 downto 0 );
  signal register5_q_net : std_logic_vector( 16-1 downto 0 );
  signal concat3_y_net : std_logic_vector( 64-1 downto 0 );
  signal register7_q_net : std_logic_vector( 1-1 downto 0 );
  signal register4_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant1_op_net : std_logic_vector( 32-1 downto 0 );
  signal time_in_hi_net : std_logic_vector( 32-1 downto 0 );
  signal m_axis_snap_tready_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_udp_tdata_net : std_logic_vector( 512-1 downto 0 );
  signal s_axis_udp_tid_net : std_logic_vector( 8-1 downto 0 );
  signal s_axis_udp_tkeep_net : std_logic_vector( 64-1 downto 0 );
  signal s_axis_udp_tlast_net : std_logic_vector( 1-1 downto 0 );
  signal s_axis_udp_tuser_net : std_logic_vector( 32-1 downto 0 );
  signal s_axis_udp_tvalid_net : std_logic_vector( 1-1 downto 0 );
  signal a_clk_net : std_logic;
  signal a_ce_net : std_logic;
  signal register0_q_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal mux0_y_net : std_logic_vector( 512-1 downto 0 );
  signal delay_sr_q_net_x2 : std_logic_vector( 8-1 downto 0 );
  signal register0_q_net_x1 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x3 : std_logic_vector( 1-1 downto 0 );
  signal concat_y_net : std_logic_vector( 512-1 downto 0 );
  signal delay_sr_q_net_x4 : std_logic_vector( 1-1 downto 0 );
  signal register0_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x6 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x7 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x8 : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x9 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net : std_logic_vector( 8-1 downto 0 );
  signal delay_sr_q_net_x10 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x18 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x30 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x36 : std_logic_vector( 512-1 downto 0 );
  signal register0_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x21 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal delay_sr_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay3_q_net_x3 : std_logic_vector( 8-1 downto 0 );
  signal delay6_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay4_q_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal delay5_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay8_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal delay1_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal delay3_q_net_x2 : std_logic_vector( 8-1 downto 0 );
  signal delay4_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x8 : std_logic_vector( 1-1 downto 0 );
  signal delay10_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay3_q_net_x1 : std_logic_vector( 8-1 downto 0 );
  signal delay4_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x9 : std_logic_vector( 1-1 downto 0 );
  signal delay12_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay3_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal delay4_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x17 : std_logic_vector( 1-1 downto 0 );
  signal delay13_q_net : std_logic_vector( 1-1 downto 0 );
  signal edge_op_y_net_x2 : std_logic_vector( 1-1 downto 0 );
  signal delay16_q_net : std_logic_vector( 1-1 downto 0 );
  signal edge_op_y_net_x3 : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net : std_logic_vector( 1-1 downto 0 );
  signal register3_q_net : std_logic_vector( 512-1 downto 0 );
  signal register3_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal eof64_0 : std_logic_vector( 1-1 downto 0 );
  signal register3_q_net_x2 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x23 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x4 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x3 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x24 : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x10 : std_logic_vector( 32-1 downto 0 );
  signal shift1_op_net : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x25 : std_logic_vector( 8-1 downto 0 );
  signal register2_q_net_x2 : std_logic_vector( 128-1 downto 0 );
  signal delay17_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x32 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x35 : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net_x20 : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x26 : std_logic_vector( 32-1 downto 0 );
  signal register1_q_net_x27 : std_logic_vector( 128-1 downto 0 );
  signal delay6_q_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x28 : std_logic_vector( 1-1 downto 0 );
  signal slice1_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal logical1_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x29 : std_logic_vector( 32-1 downto 0 );
  signal concat2_y_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal delay1_q_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x33 : std_logic_vector( 512-1 downto 0 );
  signal delay3_q_net : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x34 : std_logic_vector( 1-1 downto 0 );
  signal delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal counter_op_net : std_logic_vector( 48-1 downto 0 );
  signal register2_q_net_x3 : std_logic_vector( 128-1 downto 0 );
  signal delay15_q_net : std_logic_vector( 128-1 downto 0 );
  signal register2_q_net_x4 : std_logic_vector( 128-1 downto 0 );
  signal concat3_y_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x38 : std_logic_vector( 16-1 downto 0 );
  signal slice2_y_net : std_logic_vector( 16-1 downto 0 );
  signal register1_q_net_x39 : std_logic_vector( 1-1 downto 0 );
  signal slice2_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x40 : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x12 : std_logic_vector( 1-1 downto 0 );
  signal logical3_y_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x0 : std_logic_vector( 48-1 downto 0 );
  signal register8_q_net : std_logic_vector( 48-1 downto 0 );
  signal register1_q_net_x1 : std_logic_vector( 1-1 downto 0 );
  signal slice101_y_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal inverter2_op_net : std_logic_vector( 1-1 downto 0 );
  signal register1_q_net_x7 : std_logic_vector( 1-1 downto 0 );
  signal slice3_y_net : std_logic_vector( 8-1 downto 0 );
  signal register1_q_net_x16 : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x1 : std_logic_vector( 128-1 downto 0 );
  signal delay7_q_net_x0 : std_logic_vector( 128-1 downto 0 );
  signal concat1_y_net_x0 : std_logic_vector( 512-1 downto 0 );
  signal register1_q_net_x19 : std_logic_vector( 32-1 downto 0 );
  signal slice11_y_net : std_logic_vector( 8-1 downto 0 );
  signal logical2_y_net : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x6 : std_logic_vector( 512-1 downto 0 );
  signal register2_q_net_x5 : std_logic_vector( 1-1 downto 0 );
  signal register2_q_net_x7 : std_logic_vector( 1-1 downto 0 );
  signal data_delay_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant27_op_net : std_logic_vector( 16-1 downto 0 );
  signal delay18_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay2_q_net : std_logic_vector( 1-1 downto 0 );
  signal delay25_q_net : std_logic_vector( 512-1 downto 0 );
  signal delay3_q_net_x4 : std_logic_vector( 512-1 downto 0 );
  signal delay4_q_net_x3 : std_logic_vector( 1-1 downto 0 );
  signal delay8_q_net_x0 : std_logic_vector( 8-1 downto 0 );
  signal delay9_q_net : std_logic_vector( 8-1 downto 0 );
  signal data_delay1_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay2_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay3_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay9_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay8_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay7_q_net : std_logic_vector( 128-1 downto 0 );
  signal concat1_y_net : std_logic_vector( 48-1 downto 0 );
  signal register10_q_net : std_logic_vector( 16-1 downto 0 );
  signal delay14_q_net : std_logic_vector( 1-1 downto 0 );
  signal constant2_op_net : std_logic_vector( 16-1 downto 0 );
  signal logical2_y_net_x0 : std_logic_vector( 1-1 downto 0 );
  signal inverter1_op_net : std_logic_vector( 1-1 downto 0 );
  signal data_delay6_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay5_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay4_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay12_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay11_q_net : std_logic_vector( 128-1 downto 0 );
  signal data_delay10_q_net : std_logic_vector( 128-1 downto 0 );
  signal register1_q_net_x41 : std_logic_vector( 16-1 downto 0 );
  signal constant3_op_net : std_logic_vector( 16-1 downto 0 );
  signal delay5_q_net_x0 : std_logic_vector( 1-1 downto 0 );
begin
  time_in_lo_net <= time_in_lo;
  control_net <= control;
  intf_rst <= concat4_y_net;
  m_axis_udp_tdata <= register15_q_net;
  m_axis_udp_tid <= register12_q_net;
  m_axis_udp_tkeep <= concat2_y_net;
  m_axis_udp_tlast <= register3_q_net_x3;
  m_axis_udp_tready_net <= m_axis_udp_tready;
  m_axis_udp_tuser <= constant26_op_net;
  m_axis_udp_tvalid <= register_q_net;
  aresetn_net <= aresetn;
  s_axis_0_tdata_net <= s_axis_0_tdata;
  s_axis_0_tid_net <= s_axis_0_tid;
  s_axis_0_tkeep_net <= s_axis_0_tkeep;
  s_axis_0_tlast_net <= s_axis_0_tlast;
  s_axis_0_tready <= constant10_op_net;
  s_axis_0_tuser_net <= s_axis_0_tuser;
  s_axis_0_tvalid_net <= s_axis_0_tvalid;
  irig_trig_in_net <= irig_trig_in;
  s_axis_1_tdata_net <= s_axis_1_tdata;
  s_axis_1_tid_net <= s_axis_1_tid;
  s_axis_1_tkeep_net <= s_axis_1_tkeep;
  s_axis_1_tlast_net <= s_axis_1_tlast;
  s_axis_1_tready <= constant10_op_net;
  s_axis_1_tuser_net <= s_axis_1_tuser;
  s_axis_1_tvalid_net <= s_axis_1_tvalid;
  s_axis_2_tdata_net <= s_axis_2_tdata;
  s_axis_2_tid_net <= s_axis_2_tid;
  s_axis_2_tkeep_net <= s_axis_2_tkeep;
  s_axis_2_tlast_net <= s_axis_2_tlast;
  s_axis_2_tready <= constant10_op_net;
  s_axis_2_tuser_net <= s_axis_2_tuser;
  s_axis_2_tvalid_net <= s_axis_2_tvalid;
  s_axis_3_tdata_net <= s_axis_3_tdata;
  s_axis_3_tid_net <= s_axis_3_tid;
  s_axis_3_tkeep_net <= s_axis_3_tkeep;
  s_axis_3_tlast_net <= s_axis_3_tlast;
  s_axis_3_tready <= constant10_op_net;
  s_axis_3_tuser_net <= s_axis_3_tuser;
  s_axis_3_tvalid_net <= s_axis_3_tvalid;
  irig_comp_in_net <= irig_comp_in;
  iptx_base_net <= iptx_base;
  tx_metadata_net <= tx_metadata;
  tx_pkt_bytes_net <= tx_pkt_bytes;
  tx_pkt_wait_net <= tx_pkt_wait;
  x_setup_net <= x_setup;
  s_axis_snap_tdata_net <= s_axis_snap_tdata;
  s_axis_snap_tid_net <= s_axis_snap_tid;
  s_axis_snap_tkeep_net <= s_axis_snap_tkeep;
  s_axis_snap_tlast_net <= s_axis_snap_tlast;
  s_axis_snap_tready <= constant10_op_net;
  s_axis_snap_tuser_net <= s_axis_snap_tuser;
  s_axis_snap_tvalid_net <= s_axis_snap_tvalid;
  m_axis_snap_tdata <= register6_q_net;
  m_axis_snap_tid <= register5_q_net;
  m_axis_snap_tkeep <= concat3_y_net;
  m_axis_snap_tlast <= register7_q_net;
  m_axis_snap_tvalid <= register4_q_net;
  m_axis_snap_tuser <= constant1_op_net;
  time_in_hi_net <= time_in_hi;
  m_axis_snap_tready_net <= m_axis_snap_tready;
  s_axis_udp_tdata_net <= s_axis_udp_tdata;
  s_axis_udp_tid_net <= s_axis_udp_tid;
  s_axis_udp_tkeep_net <= s_axis_udp_tkeep;
  s_axis_udp_tlast_net <= s_axis_udp_tlast;
  s_axis_udp_tready <= constant10_op_net;
  s_axis_udp_tuser_net <= s_axis_udp_tuser;
  s_axis_udp_tvalid_net <= s_axis_udp_tvalid;
  a_clk_net <= clk_1;
  a_ce_net <= ce_1;
  mb : entity xil_defaultlib.rfsoc_byp_mb 
  port map (
    w_addr => delay_sr_q_net_x2,
    r_addr => register0_q_net_x1,
    we => delay_sr_q_net_x3,
    p0sx_p1sx0 => concat_y_net,
    d_run => delay_sr_q_net_x4,
    valid_read => register0_q_net_x0,
    w_addr1 => delay_sr_q_net_x6,
    we1 => delay_sr_q_net_x7,
    w_addr2 => delay_sr_q_net_x8,
    we2 => delay_sr_q_net_x9,
    w_addr3 => delay_sr_q_net,
    we3 => delay_sr_q_net_x10,
    p0sx_p1sx1 => register1_q_net_x18,
    p0sx_p1sx2 => register1_q_net_x30,
    p0sx_p1sx3 => register1_q_net_x36,
    s_rd_eof => register0_q_net,
    d_reset => register1_q_net_x21,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    sync_out => register0_q_net_x2,
    s0_pci0 => mux0_y_net
  );
  mb_rd : entity xil_defaultlib.rfsoc_byp_mb_rd 
  port map (
    rd_start => delay_sr_q_net_x1,
    rd_start1 => delay_sr_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    d_valid => register0_q_net_x0,
    r_addr => register0_q_net_x1,
    eof => register0_q_net
  );
  mb_wr0 : entity xil_defaultlib.rfsoc_byp_mb_wr0 
  port map (
    msync => register1_q_net_x2,
    d_val => delay1_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    w_addr => delay3_q_net_x3,
    rd_start => delay6_q_net,
    we => delay4_q_net_x2,
    run => delay5_q_net,
    rd_start1 => delay8_q_net
  );
  mb_wr1 : entity xil_defaultlib.rfsoc_byp_mb_wr1 
  port map (
    msync => register1_q_net_x8,
    d_val => delay10_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    w_addr => delay3_q_net_x2,
    we => delay4_q_net_x1
  );
  mb_wr2 : entity xil_defaultlib.rfsoc_byp_mb_wr2 
  port map (
    msync => register1_q_net_x9,
    d_val => delay12_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    w_addr => delay3_q_net_x1,
    we => delay4_q_net_x0
  );
  mb_wr3 : entity xil_defaultlib.rfsoc_byp_mb_wr3 
  port map (
    msync => register1_q_net_x17,
    d_val => delay13_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    w_addr => delay3_q_net_x0,
    we => delay4_q_net
  );
  delay_srl : entity xil_defaultlib.rfsoc_byp_delay_srl 
  port map (
    in_x0 => delay6_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x1
  );
  delay_srl1 : entity xil_defaultlib.rfsoc_byp_delay_srl1 
  port map (
    in_x0 => delay8_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x0
  );
  delay_srl10 : entity xil_defaultlib.rfsoc_byp_delay_srl10 
  port map (
    in_x0 => delay3_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net
  );
  delay_srl11_x0 : entity xil_defaultlib.rfsoc_byp_delay_srl11 
  port map (
    in_x0 => delay4_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x10
  );
  delay_srl2 : entity xil_defaultlib.rfsoc_byp_delay_srl2 
  port map (
    in_x0 => delay3_q_net_x3,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x2
  );
  delay_srl3 : entity xil_defaultlib.rfsoc_byp_delay_srl3 
  port map (
    in_x0 => delay4_q_net_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x3
  );
  delay_srl4 : entity xil_defaultlib.rfsoc_byp_delay_srl4 
  port map (
    in_x0 => delay5_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x4
  );
  delay_srl6 : entity xil_defaultlib.rfsoc_byp_delay_srl6 
  port map (
    in_x0 => delay3_q_net_x2,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x6
  );
  delay_srl7 : entity xil_defaultlib.rfsoc_byp_delay_srl7 
  port map (
    in_x0 => delay4_q_net_x1,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x7
  );
  delay_srl8 : entity xil_defaultlib.rfsoc_byp_delay_srl8 
  port map (
    in_x0 => delay3_q_net_x1,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x8
  );
  delay_srl9 : entity xil_defaultlib.rfsoc_byp_delay_srl9 
  port map (
    in_x0 => delay4_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => delay_sr_q_net_x9
  );
  edge_detect2 : entity xil_defaultlib.rfsoc_byp_edge_detect2_x0 
  port map (
    in_x0 => delay16_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net_x2
  );
  edge_detect3 : entity xil_defaultlib.rfsoc_byp_edge_detect3 
  port map (
    in_x0 => register2_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    out_x0 => edge_op_y_net_x3
  );
  pack0 : entity xil_defaultlib.rfsoc_byp_pack0 
  port map (
    sync_in => register1_q_net_x23,
    en => register1_q_net_x4,
    din => register1_q_net_x3,
    timestamp => register1_q_net_x24,
    board_id => register1_q_net,
    base_ip => register1_q_net_x10,
    pkt_len_words => shift1_op_net,
    pkt_per_heap => register1_q_net_x25,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    data0 => register3_q_net,
    valid0 => register3_q_net_x0,
    eof0 => eof64_0,
    destination_ip0 => register3_q_net_x2
  );
  pipeline10 : entity xil_defaultlib.rfsoc_byp_pipeline10 
  port map (
    d => delay17_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x2
  );
  pipeline11 : entity xil_defaultlib.rfsoc_byp_pipeline11 
  port map (
    d => register1_q_net_x32,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x23
  );
  pipeline12 : entity xil_defaultlib.rfsoc_byp_pipeline12 
  port map (
    d => register1_q_net_x35,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x24
  );
  pipeline121 : entity xil_defaultlib.rfsoc_byp_pipeline121 
  port map (
    d => register1_q_net_x20,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x25
  );
  pipeline13 : entity xil_defaultlib.rfsoc_byp_pipeline13_x0 
  port map (
    d => time_in_lo_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x26
  );
  pipeline14 : entity xil_defaultlib.rfsoc_byp_pipeline14 
  port map (
    d => delay6_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x27
  );
  pipeline15_x0 : entity xil_defaultlib.rfsoc_byp_pipeline15 
  port map (
    d => slice1_y_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x28
  );
  pipeline16_x0 : entity xil_defaultlib.rfsoc_byp_pipeline16 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x21
  );
  pipeline166 : entity xil_defaultlib.rfsoc_byp_pipeline166 
  port map (
    d => control_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x29
  );
  pipeline17 : entity xil_defaultlib.rfsoc_byp_pipeline17 
  port map (
    d => concat2_y_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x30
  );
  pipeline174 : entity xil_defaultlib.rfsoc_byp_pipeline174 
  port map (
    d => delay1_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x32
  );
  pipeline175 : entity xil_defaultlib.rfsoc_byp_pipeline175 
  port map (
    d => delay3_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x33
  );
  pipeline176 : entity xil_defaultlib.rfsoc_byp_pipeline176 
  port map (
    d => delay_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x34
  );
  pipeline177 : entity xil_defaultlib.rfsoc_byp_pipeline177 
  port map (
    d => counter_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x35
  );
  pipeline18 : entity xil_defaultlib.rfsoc_byp_pipeline18_x0 
  port map (
    d => delay15_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x3
  );
  pipeline188 : entity xil_defaultlib.rfsoc_byp_pipeline188 
  port map (
    d => register1_q_net_x27,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x4
  );
  pipeline19 : entity xil_defaultlib.rfsoc_byp_pipeline19_x1 
  port map (
    d => concat3_y_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x36
  );
  pipeline20 : entity xil_defaultlib.rfsoc_byp_pipeline20 
  port map (
    d => slice2_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x38
  );
  pipeline21 : entity xil_defaultlib.rfsoc_byp_pipeline21_x1 
  port map (
    d => slice2_y_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x39
  );
  pipeline22 : entity xil_defaultlib.rfsoc_byp_pipeline22_x1 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x40
  );
  pipeline23 : entity xil_defaultlib.rfsoc_byp_pipeline23_x0 
  port map (
    d => logical3_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x12
  );
  pipeline24 : entity xil_defaultlib.rfsoc_byp_pipeline24_x1 
  port map (
    d => register8_q_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x0
  );
  pipeline25 : entity xil_defaultlib.rfsoc_byp_pipeline25_x0 
  port map (
    d => logical3_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net
  );
  pipeline3_x0 : entity xil_defaultlib.rfsoc_byp_pipeline3 
  port map (
    d => slice101_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x1
  );
  pipeline32 : entity xil_defaultlib.rfsoc_byp_pipeline32_x1 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x2
  );
  pipeline33 : entity xil_defaultlib.rfsoc_byp_pipeline33_x0 
  port map (
    d => inverter2_op_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x0
  );
  pipeline36_x0 : entity xil_defaultlib.rfsoc_byp_pipeline36 
  port map (
    d => register1_q_net_x33,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x3
  );
  pipeline37 : entity xil_defaultlib.rfsoc_byp_pipeline37_x1 
  port map (
    d => register1_q_net_x34,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x4
  );
  pipeline4 : entity xil_defaultlib.rfsoc_byp_pipeline4_x1 
  port map (
    d => slice101_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x7
  );
  pipeline5 : entity xil_defaultlib.rfsoc_byp_pipeline5_x2 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x8
  );
  pipeline6 : entity xil_defaultlib.rfsoc_byp_pipeline6_x1 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x9
  );
  pipeline60 : entity xil_defaultlib.rfsoc_byp_pipeline60 
  port map (
    d => iptx_base_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x10
  );
  pipeline62 : entity xil_defaultlib.rfsoc_byp_pipeline62 
  port map (
    d => slice3_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net
  );
  pipeline67 : entity xil_defaultlib.rfsoc_byp_pipeline67 
  port map (
    d => register0_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x16
  );
  pipeline7 : entity xil_defaultlib.rfsoc_byp_pipeline7_x0 
  port map (
    d => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x17
  );
  pipeline8 : entity xil_defaultlib.rfsoc_byp_pipeline8_x1 
  port map (
    d => delay7_q_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x1
  );
  pipeline9 : entity xil_defaultlib.rfsoc_byp_pipeline9 
  port map (
    d => concat1_y_net_x0,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x18
  );
  pipeline91 : entity xil_defaultlib.rfsoc_byp_pipeline91 
  port map (
    d => tx_pkt_bytes_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x19
  );
  pipeline95 : entity xil_defaultlib.rfsoc_byp_pipeline95 
  port map (
    d => slice11_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register1_q_net_x20
  );
  snap : entity xil_defaultlib.rfsoc_byp_snap 
  port map (
    d => delay6_q_net_x0,
    d1 => delay1_q_net_x0,
    d2 => delay7_q_net_x0,
    d3 => delay10_q_net,
    d4 => delay17_q_net,
    d5 => delay12_q_net,
    d6 => delay15_q_net,
    d7 => delay13_q_net,
    d12 => logical1_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    q => register2_q_net_x6,
    q1 => register2_q_net_x5,
    q2 => register2_q_net_x7
  );
  time_gen : entity xil_defaultlib.rfsoc_byp_time_gen 
  port map (
    wr_sync_in => register0_q_net_x2,
    wr_en => register0_q_net_x0,
    wr_val_in => register1_q_net_x0,
    rd_en => data_delay_q_net,
    rd_misc_in => mux0_y_net,
    clk_1 => a_clk_net,
    ce_1 => a_ce_net,
    rd_sync_out => delay1_q_net,
    rd_dvalid => delay_q_net,
    rd_val_out => counter_op_net,
    rd_misc_out => delay3_q_net,
    heap_end => logical2_y_net
  );
  constant10 : entity xil_defaultlib.sysgen_constant_195640b7dc 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant10_op_net
  );
  constant26 : entity xil_defaultlib.sysgen_constant_38f969afab 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant26_op_net
  );
  constant27 : entity xil_defaultlib.sysgen_constant_46c4e4eb6f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant27_op_net
  );
  delay1 : entity xil_defaultlib.sysgen_delay_a6880577e7 
  port map (
    clr => '0',
    d => s_axis_0_tvalid_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay1_q_net_x0
  );
  delay18 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => register3_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay18_q_net
  );
  delay2 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => eof64_0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay2_q_net
  );
  delay25 : entity xil_defaultlib.sysgen_delay_f5ff6613e3 
  port map (
    clr => '0',
    d => delay3_q_net_x4,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay25_q_net
  );
  delay3 : entity xil_defaultlib.sysgen_delay_f5ff6613e3 
  port map (
    clr => '0',
    d => register3_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay3_q_net_x4
  );
  delay4 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay18_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay4_q_net_x3
  );
  delay8 : entity xil_defaultlib.sysgen_delay_5c9b8add68 
  port map (
    clr => '0',
    d => register3_q_net_x2,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay8_q_net_x0
  );
  delay9 : entity xil_defaultlib.sysgen_delay_5c9b8add68 
  port map (
    clr => '0',
    d => delay8_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay9_q_net
  );
  data_delay1 : entity xil_defaultlib.sysgen_delay_aff82389e2 
  port map (
    clr => '0',
    d => register2_q_net_x4,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay1_q_net
  );
  inverter2 : entity xil_defaultlib.sysgen_inverter_9e36a99262 
  port map (
    clr => '0',
    ip => aresetn_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter2_op_net
  );
  logical1 : entity xil_defaultlib.sysgen_logical_3f4db257a4 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    d0 => register2_q_net_x0,
    d1 => register1_q_net_x7,
    y => logical1_y_net
  );
  data_delay2 : entity xil_defaultlib.sysgen_delay_d32ac66525 
  port map (
    clr => '0',
    d => register2_q_net_x4,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay2_q_net
  );
  data_delay3 : entity xil_defaultlib.sysgen_delay_3d27d2aa0d 
  port map (
    clr => '0',
    d => register2_q_net_x4,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay3_q_net
  );
  delay15 : entity xil_defaultlib.sysgen_delay_799e779f92 
  port map (
    clr => '0',
    d => s_axis_3_tdata_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay15_q_net
  );
  concat2 : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => data_delay9_q_net,
    in1 => data_delay8_q_net,
    in2 => data_delay7_q_net,
    in3 => register2_q_net_x2,
    y => concat2_y_net_x0
  );
  data_delay7 : entity xil_defaultlib.sysgen_delay_aff82389e2 
  port map (
    clr => '0',
    d => register2_q_net_x2,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay7_q_net
  );
  data_delay8 : entity xil_defaultlib.sysgen_delay_d32ac66525 
  port map (
    clr => '0',
    d => register2_q_net_x2,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay8_q_net
  );
  delay17 : entity xil_defaultlib.sysgen_delay_799e779f92 
  port map (
    clr => '0',
    d => s_axis_2_tdata_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay17_q_net
  );
  data_delay9 : entity xil_defaultlib.sysgen_delay_3d27d2aa0d 
  port map (
    clr => '0',
    d => register2_q_net_x2,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay9_q_net
  );
  concat1 : entity xil_defaultlib.sysgen_concat_0d95fecbca 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => register1_q_net_x38,
    in1 => register1_q_net_x26,
    y => concat1_y_net
  );
  delay7 : entity xil_defaultlib.sysgen_delay_799e779f92 
  port map (
    clr => '0',
    d => s_axis_1_tdata_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay7_q_net_x0
  );
  delay6 : entity xil_defaultlib.sysgen_delay_799e779f92 
  port map (
    clr => '0',
    d => s_axis_0_tdata_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay6_q_net_x0
  );
  register_x0 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    rst => "0",
    d => delay4_q_net_x3,
    en => m_axis_udp_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register_q_net
  );
  register10 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 16,
    init_value => b"0000000000000000"
  )
  port map (
    rst => "0",
    d => constant27_op_net,
    en => delay4_q_net_x3,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register10_q_net
  );
  register12 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 8,
    init_value => b"00000000"
  )
  port map (
    rst => "0",
    d => delay9_q_net,
    en => m_axis_udp_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register12_q_net
  );
  register15 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => delay25_q_net,
    en => m_axis_udp_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register15_q_net
  );
  register3 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    rst => "0",
    d => delay14_q_net,
    en => m_axis_udp_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register3_q_net_x3
  );
  constant2 : entity xil_defaultlib.sysgen_constant_46c4e4eb6f 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant2_op_net
  );
  slice101 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 0,
    x_width => 32,
    y_width => 1
  )
  port map (
    x => register1_q_net_x29,
    y => slice101_y_net
  );
  concat2_x0 : entity xil_defaultlib.sysgen_concat_3052e951a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => register10_q_net,
    in1 => register10_q_net,
    in2 => register10_q_net,
    in3 => register10_q_net,
    y => concat2_y_net
  );
  data_delay : entity xil_defaultlib.sysgen_delay_8a340f589b 
  port map (
    clr => '0',
    d => register1_q_net_x16,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay_q_net
  );
  concat : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => data_delay3_q_net,
    in1 => data_delay2_q_net,
    in2 => data_delay1_q_net,
    in3 => register2_q_net_x4,
    y => concat_y_net
  );
  delay10 : entity xil_defaultlib.sysgen_delay_a6880577e7 
  port map (
    clr => '0',
    d => s_axis_1_tvalid_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay10_q_net
  );
  concat4 : entity xil_defaultlib.sysgen_concat_6fa8795e67 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => register1_q_net_x1,
    in1 => register1_q_net_x28,
    in2 => edge_op_y_net_x3,
    y => concat4_y_net
  );
  delay12 : entity xil_defaultlib.sysgen_delay_a6880577e7 
  port map (
    clr => '0',
    d => s_axis_2_tvalid_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay12_q_net
  );
  delay13 : entity xil_defaultlib.sysgen_delay_a6880577e7 
  port map (
    clr => '0',
    d => s_axis_3_tvalid_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay13_q_net
  );
  logical2 : entity xil_defaultlib.sysgen_logical_0afb187314 
  port map (
    clr => '0',
    d0 => register1_q_net_x28,
    d1 => register1_q_net_x39,
    en => inverter1_op_net,
    clk => a_clk_net,
    ce => a_ce_net,
    y => logical2_y_net_x0
  );
  concat1_x0 : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => data_delay6_q_net,
    in1 => data_delay5_q_net,
    in2 => data_delay4_q_net,
    in3 => register2_q_net_x1,
    y => concat1_y_net_x0
  );
  concat3 : entity xil_defaultlib.sysgen_concat_ed51fd5bd8 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => data_delay12_q_net,
    in1 => data_delay11_q_net,
    in2 => data_delay10_q_net,
    in3 => register2_q_net_x3,
    y => concat3_y_net_x0
  );
  data_delay10 : entity xil_defaultlib.sysgen_delay_aff82389e2 
  port map (
    clr => '0',
    d => register2_q_net_x3,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay10_q_net
  );
  data_delay11 : entity xil_defaultlib.sysgen_delay_d32ac66525 
  port map (
    clr => '0',
    d => register2_q_net_x3,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay11_q_net
  );
  data_delay12 : entity xil_defaultlib.sysgen_delay_3d27d2aa0d 
  port map (
    clr => '0',
    d => register2_q_net_x3,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay12_q_net
  );
  data_delay4 : entity xil_defaultlib.sysgen_delay_aff82389e2 
  port map (
    clr => '0',
    d => register2_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay4_q_net
  );
  data_delay5 : entity xil_defaultlib.sysgen_delay_d32ac66525 
  port map (
    clr => '0',
    d => register2_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay5_q_net
  );
  data_delay6 : entity xil_defaultlib.sysgen_delay_3d27d2aa0d 
  port map (
    clr => '0',
    d => register2_q_net_x1,
    clk => a_clk_net,
    ce => a_ce_net,
    q => data_delay6_q_net
  );
  inverter1 : entity xil_defaultlib.sysgen_inverter_9e36a99262 
  port map (
    clr => '0',
    ip => logical1_y_net,
    clk => a_clk_net,
    ce => a_ce_net,
    op => inverter1_op_net
  );
  delay14 : entity xil_defaultlib.sysgen_delay_1a89bf1c4d 
  port map (
    clr => '0',
    d => delay2_q_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay14_q_net
  );
  register1 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 16,
    init_value => b"0000000000000000"
  )
  port map (
    rst => "0",
    d => constant2_op_net,
    en => m_axis_snap_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register1_q_net_x41
  );
  shift1 : entity xil_defaultlib.sysgen_shift_46cf059324 
  port map (
    clr => '0',
    ip => register1_q_net_x19,
    clk => a_clk_net,
    ce => a_ce_net,
    op => shift1_op_net
  );
  slice11 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 0,
    new_msb => 7,
    x_width => 32,
    y_width => 8
  )
  port map (
    x => x_setup_net,
    y => slice11_y_net
  );
  slice3 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 8,
    new_msb => 15,
    x_width => 32,
    y_width => 8
  )
  port map (
    x => tx_metadata_net,
    y => slice3_y_net
  );
  constant1 : entity xil_defaultlib.sysgen_constant_38f969afab 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant1_op_net
  );
  concat3_x0 : entity xil_defaultlib.sysgen_concat_3052e951a9 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    in0 => register1_q_net_x41,
    in1 => register1_q_net_x41,
    in2 => register1_q_net_x41,
    in3 => register1_q_net_x41,
    y => concat3_y_net
  );
  register4 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    rst => "0",
    d => register2_q_net_x5,
    en => m_axis_snap_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register4_q_net
  );
  register5 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 16,
    init_value => b"0000000000000000"
  )
  port map (
    rst => "0",
    d => constant3_op_net,
    en => m_axis_snap_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register5_q_net
  );
  register6 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 512,
    init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
  )
  port map (
    rst => "0",
    d => register2_q_net_x6,
    en => m_axis_snap_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register6_q_net
  );
  register7 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 1,
    init_value => b"0"
  )
  port map (
    rst => "0",
    d => register2_q_net_x7,
    en => m_axis_snap_tready_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register7_q_net
  );
  slice2 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 1,
    new_msb => 16,
    x_width => 32,
    y_width => 16
  )
  port map (
    x => time_in_hi_net,
    y => slice2_y_net
  );
  constant3 : entity xil_defaultlib.sysgen_constant_f02f49a5ed 
  port map (
    clk => '0',
    ce => '0',
    clr => '0',
    op => constant3_op_net
  );
  delay5 : entity xil_defaultlib.sysgen_delay_8a340f589b 
  port map (
    clr => '0',
    d => irig_trig_in_net,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay5_q_net_x0
  );
  delay16 : entity xil_defaultlib.sysgen_delay_8a340f589b 
  port map (
    clr => '0',
    d => delay5_q_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    q => delay16_q_net
  );
  slice1 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 1,
    new_msb => 1,
    x_width => 32,
    y_width => 1
  )
  port map (
    x => register1_q_net_x29,
    y => slice1_y_net_x0
  );
  slice2_x0 : entity xil_defaultlib.rfsoc_byp_xlslice 
  generic map (
    new_lsb => 1,
    new_msb => 1,
    x_width => 32,
    y_width => 1
  )
  port map (
    x => register1_q_net_x29,
    y => slice2_y_net_x0
  );
  register8 : entity xil_defaultlib.rfsoc_byp_xlregister 
  generic map (
    d_width => 48,
    init_value => b"000000000000000000000000000000000000000000000000"
  )
  port map (
    d => concat1_y_net,
    rst => register1_q_net_x40,
    en => register1_q_net_x12,
    clk => a_clk_net,
    ce => a_ce_net,
    q => register8_q_net
  );
  logical3 : entity xil_defaultlib.sysgen_logical_f40a2a71f3 
  port map (
    clr => '0',
    d0 => edge_op_y_net_x2,
    d1 => logical2_y_net_x0,
    clk => a_clk_net,
    ce => a_ce_net,
    y => logical3_y_net
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp_default_clock_driver is
  port (
    rfsoc_byp_sysclk : in std_logic;
    rfsoc_byp_sysce : in std_logic;
    rfsoc_byp_sysclr : in std_logic;
    rfsoc_byp_clk1 : out std_logic;
    rfsoc_byp_ce1 : out std_logic
  );
end rfsoc_byp_default_clock_driver;
architecture structural of rfsoc_byp_default_clock_driver is 
begin
  clockdriver : entity xil_defaultlib.xlclockdriver 
  generic map (
    period => 1,
    log_2_period => 1
  )
  port map (
    sysclk => rfsoc_byp_sysclk,
    sysce => rfsoc_byp_sysce,
    sysclr => rfsoc_byp_sysclr,
    clk => rfsoc_byp_clk1,
    ce => rfsoc_byp_ce1
  );
end structural;
-- Generated from Simulink block 
library IEEE;
use IEEE.std_logic_1164.all;
library xil_defaultlib;
use xil_defaultlib.conv_pkg.all;
entity rfsoc_byp is
  port (
    m_axis_udp_tready : in std_logic_vector( 1-1 downto 0 );
    aresetn : in std_logic_vector( 1-1 downto 0 );
    s_axis_0_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_0_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_0_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_0_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_0_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_0_tvalid : in std_logic_vector( 1-1 downto 0 );
    irig_trig_in : in std_logic_vector( 1-1 downto 0 );
    s_axis_1_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_1_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_1_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_1_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_1_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_1_tvalid : in std_logic_vector( 1-1 downto 0 );
    s_axis_2_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_2_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_2_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_2_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_2_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_2_tvalid : in std_logic_vector( 1-1 downto 0 );
    s_axis_3_tdata : in std_logic_vector( 128-1 downto 0 );
    s_axis_3_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_3_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_3_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_3_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_3_tvalid : in std_logic_vector( 1-1 downto 0 );
    irig_comp_in : in std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tdata : in std_logic_vector( 512-1 downto 0 );
    s_axis_snap_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_snap_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_snap_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_snap_tvalid : in std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tready : in std_logic_vector( 1-1 downto 0 );
    s_axis_udp_tdata : in std_logic_vector( 512-1 downto 0 );
    s_axis_udp_tid : in std_logic_vector( 8-1 downto 0 );
    s_axis_udp_tkeep : in std_logic_vector( 64-1 downto 0 );
    s_axis_udp_tlast : in std_logic_vector( 1-1 downto 0 );
    s_axis_udp_tuser : in std_logic_vector( 32-1 downto 0 );
    s_axis_udp_tvalid : in std_logic_vector( 1-1 downto 0 );
    clk : in std_logic;
    rfsoc_byp_aresetn : in std_logic;
    rfsoc_byp_s_axi_awaddr : in std_logic_vector( 11-1 downto 0 );
    rfsoc_byp_s_axi_awvalid : in std_logic;
    rfsoc_byp_s_axi_wdata : in std_logic_vector( 32-1 downto 0 );
    rfsoc_byp_s_axi_wstrb : in std_logic_vector( 4-1 downto 0 );
    rfsoc_byp_s_axi_wvalid : in std_logic;
    rfsoc_byp_s_axi_bready : in std_logic;
    rfsoc_byp_s_axi_araddr : in std_logic_vector( 11-1 downto 0 );
    rfsoc_byp_s_axi_arvalid : in std_logic;
    rfsoc_byp_s_axi_rready : in std_logic;
    m_axis_udp_tdata : out std_logic_vector( 512-1 downto 0 );
    m_axis_udp_tid : out std_logic_vector( 8-1 downto 0 );
    m_axis_udp_tkeep : out std_logic_vector( 64-1 downto 0 );
    m_axis_udp_tlast : out std_logic_vector( 1-1 downto 0 );
    m_axis_udp_tuser : out std_logic_vector( 32-1 downto 0 );
    m_axis_udp_tvalid : out std_logic_vector( 1-1 downto 0 );
    s_axis_0_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_1_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_2_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_3_tready : out std_logic_vector( 1-1 downto 0 );
    s_axis_snap_tready : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tdata : out std_logic_vector( 512-1 downto 0 );
    m_axis_snap_tid : out std_logic_vector( 16-1 downto 0 );
    m_axis_snap_tkeep : out std_logic_vector( 64-1 downto 0 );
    m_axis_snap_tlast : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tvalid : out std_logic_vector( 1-1 downto 0 );
    m_axis_snap_tuser : out std_logic_vector( 32-1 downto 0 );
    s_axis_udp_tready : out std_logic_vector( 1-1 downto 0 );
    rfsoc_byp_s_axi_awready : out std_logic;
    rfsoc_byp_s_axi_wready : out std_logic;
    rfsoc_byp_s_axi_bresp : out std_logic_vector( 2-1 downto 0 );
    rfsoc_byp_s_axi_bvalid : out std_logic;
    rfsoc_byp_s_axi_arready : out std_logic;
    rfsoc_byp_s_axi_rdata : out std_logic_vector( 32-1 downto 0 );
    rfsoc_byp_s_axi_rresp : out std_logic_vector( 2-1 downto 0 );
    rfsoc_byp_s_axi_rvalid : out std_logic
  );
end rfsoc_byp;
architecture structural of rfsoc_byp is 
  attribute core_generation_info : string;
  attribute core_generation_info of structural : architecture is "rfsoc_byp,sysgen_core_2020_2,{,compilation=IP Catalog,block_icon_display=Default,family=zynquplus,part=xczu9eg,speed=-2-e,package=ffvb1156,synthesis_language=vhdl,hdl_library=xil_defaultlib,synthesis_strategy=Vivado Synthesis Defaults,implementation_strategy=Vivado Implementation Defaults,testbench=0,interface_doc=0,ce_clr=0,clock_period=5,system_simulink_period=1,waveform_viewer=0,axilite_interface=1,ip_catalog_plugin=0,hwcosim_burst_mode=0,simulation_time=60000,addsub=3,assert=32,concat=40,constant=51,convert=10,counter=20,delay=138,dpram=4,inv=19,logical=42,mult=2,mux=3,register=340,reinterpret=210,relational=14,shift=2,slice=117,}";
  signal control : std_logic_vector( 32-1 downto 0 );
  signal intf_rst : std_logic_vector( 3-1 downto 0 );
  signal iptx_base : std_logic_vector( 32-1 downto 0 );
  signal tx_metadata : std_logic_vector( 32-1 downto 0 );
  signal tx_pkt_bytes : std_logic_vector( 32-1 downto 0 );
  signal tx_pkt_wait : std_logic_vector( 32-1 downto 0 );
  signal x_setup : std_logic_vector( 32-1 downto 0 );
  signal time_in_hi : std_logic_vector( 32-1 downto 0 );
  signal clk_1_net : std_logic;
  signal ce_1_net : std_logic;
  signal clk_net : std_logic;
  signal time_in_lo : std_logic_vector( 32-1 downto 0 );
begin
  rfsoc_byp_axi_lite_interface : entity xil_defaultlib.rfsoc_byp_axi_lite_interface 
  port map (
    intf_rst => intf_rst,
    rfsoc_byp_s_axi_awaddr => rfsoc_byp_s_axi_awaddr,
    rfsoc_byp_s_axi_awvalid => rfsoc_byp_s_axi_awvalid,
    rfsoc_byp_s_axi_wdata => rfsoc_byp_s_axi_wdata,
    rfsoc_byp_s_axi_wstrb => rfsoc_byp_s_axi_wstrb,
    rfsoc_byp_s_axi_wvalid => rfsoc_byp_s_axi_wvalid,
    rfsoc_byp_s_axi_bready => rfsoc_byp_s_axi_bready,
    rfsoc_byp_s_axi_araddr => rfsoc_byp_s_axi_araddr,
    rfsoc_byp_s_axi_arvalid => rfsoc_byp_s_axi_arvalid,
    rfsoc_byp_s_axi_rready => rfsoc_byp_s_axi_rready,
    rfsoc_byp_aresetn => rfsoc_byp_aresetn,
    rfsoc_byp_aclk => clk,
    time_in_hi => time_in_hi,
    x_setup => x_setup,
    tx_pkt_wait => tx_pkt_wait,
    tx_pkt_bytes => tx_pkt_bytes,
    tx_metadata => tx_metadata,
    iptx_base => iptx_base,
    control => control,
    time_in_lo => time_in_lo,
    rfsoc_byp_s_axi_awready => rfsoc_byp_s_axi_awready,
    rfsoc_byp_s_axi_wready => rfsoc_byp_s_axi_wready,
    rfsoc_byp_s_axi_bresp => rfsoc_byp_s_axi_bresp,
    rfsoc_byp_s_axi_bvalid => rfsoc_byp_s_axi_bvalid,
    rfsoc_byp_s_axi_arready => rfsoc_byp_s_axi_arready,
    rfsoc_byp_s_axi_rdata => rfsoc_byp_s_axi_rdata,
    rfsoc_byp_s_axi_rresp => rfsoc_byp_s_axi_rresp,
    rfsoc_byp_s_axi_rvalid => rfsoc_byp_s_axi_rvalid,
    clk => clk_net
  );
  rfsoc_byp_default_clock_driver : entity xil_defaultlib.rfsoc_byp_default_clock_driver 
  port map (
    rfsoc_byp_sysclk => clk_net,
    rfsoc_byp_sysce => '1',
    rfsoc_byp_sysclr => '0',
    rfsoc_byp_clk1 => clk_1_net,
    rfsoc_byp_ce1 => ce_1_net
  );
  rfsoc_byp_struct : entity xil_defaultlib.rfsoc_byp_struct 
  port map (
    time_in_lo => time_in_lo,
    control => control,
    m_axis_udp_tready => m_axis_udp_tready,
    aresetn => aresetn,
    s_axis_0_tdata => s_axis_0_tdata,
    s_axis_0_tid => s_axis_0_tid,
    s_axis_0_tkeep => s_axis_0_tkeep,
    s_axis_0_tlast => s_axis_0_tlast,
    s_axis_0_tuser => s_axis_0_tuser,
    s_axis_0_tvalid => s_axis_0_tvalid,
    irig_trig_in => irig_trig_in,
    s_axis_1_tdata => s_axis_1_tdata,
    s_axis_1_tid => s_axis_1_tid,
    s_axis_1_tkeep => s_axis_1_tkeep,
    s_axis_1_tlast => s_axis_1_tlast,
    s_axis_1_tuser => s_axis_1_tuser,
    s_axis_1_tvalid => s_axis_1_tvalid,
    s_axis_2_tdata => s_axis_2_tdata,
    s_axis_2_tid => s_axis_2_tid,
    s_axis_2_tkeep => s_axis_2_tkeep,
    s_axis_2_tlast => s_axis_2_tlast,
    s_axis_2_tuser => s_axis_2_tuser,
    s_axis_2_tvalid => s_axis_2_tvalid,
    s_axis_3_tdata => s_axis_3_tdata,
    s_axis_3_tid => s_axis_3_tid,
    s_axis_3_tkeep => s_axis_3_tkeep,
    s_axis_3_tlast => s_axis_3_tlast,
    s_axis_3_tuser => s_axis_3_tuser,
    s_axis_3_tvalid => s_axis_3_tvalid,
    irig_comp_in => irig_comp_in,
    iptx_base => iptx_base,
    tx_metadata => tx_metadata,
    tx_pkt_bytes => tx_pkt_bytes,
    tx_pkt_wait => tx_pkt_wait,
    x_setup => x_setup,
    s_axis_snap_tdata => s_axis_snap_tdata,
    s_axis_snap_tid => s_axis_snap_tid,
    s_axis_snap_tkeep => s_axis_snap_tkeep,
    s_axis_snap_tlast => s_axis_snap_tlast,
    s_axis_snap_tuser => s_axis_snap_tuser,
    s_axis_snap_tvalid => s_axis_snap_tvalid,
    time_in_hi => time_in_hi,
    m_axis_snap_tready => m_axis_snap_tready,
    s_axis_udp_tdata => s_axis_udp_tdata,
    s_axis_udp_tid => s_axis_udp_tid,
    s_axis_udp_tkeep => s_axis_udp_tkeep,
    s_axis_udp_tlast => s_axis_udp_tlast,
    s_axis_udp_tuser => s_axis_udp_tuser,
    s_axis_udp_tvalid => s_axis_udp_tvalid,
    clk_1 => clk_1_net,
    ce_1 => ce_1_net,
    intf_rst => intf_rst,
    m_axis_udp_tdata => m_axis_udp_tdata,
    m_axis_udp_tid => m_axis_udp_tid,
    m_axis_udp_tkeep => m_axis_udp_tkeep,
    m_axis_udp_tlast => m_axis_udp_tlast,
    m_axis_udp_tuser => m_axis_udp_tuser,
    m_axis_udp_tvalid => m_axis_udp_tvalid,
    s_axis_0_tready => s_axis_0_tready,
    s_axis_1_tready => s_axis_1_tready,
    s_axis_2_tready => s_axis_2_tready,
    s_axis_3_tready => s_axis_3_tready,
    s_axis_snap_tready => s_axis_snap_tready,
    m_axis_snap_tdata => m_axis_snap_tdata,
    m_axis_snap_tid => m_axis_snap_tid,
    m_axis_snap_tkeep => m_axis_snap_tkeep,
    m_axis_snap_tlast => m_axis_snap_tlast,
    m_axis_snap_tvalid => m_axis_snap_tvalid,
    m_axis_snap_tuser => m_axis_snap_tuser,
    s_axis_udp_tready => s_axis_udp_tready
  );
end structural;
